﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class weaponCanvasManager : equipmentCanvasManager
{

	public GameObject unitSideMenu;
	public GameObject backButton;

	private bool sideMenuSetup = false;
	private bool buttonIsSetup = false;

	public void displayWeapons ()
	{

		if (!sideMenuSetup) {

			unitSideMenu.GetComponent<weaponCanvasSideMenu> ().setupCanvas (this, profileManager.getInstance ().getLocalPlayer ().getUnits ().ToArray ());
		

			sideMenuSetup = true;

		}

		setDetailToBlank ();
		unitSideMenu.SetActive (true);

	}

	public void unitSelected (unit item)
	{
		
		base.upgradeButton.gameObject.SetActive (true);
		unitSideMenu.SetActive (false);

		if (!buttonIsSetup) {
			backButton = Instantiate (backButton);

			backButton.GetComponent<Button> ().onClick.AddListener (() => {

				unitSideMenu.SetActive (true);
				base.cleanCanvas ();
				setDetailToBlank ();

			});

			buttonIsSetup = true;
		}



		base.displayWeaponsWithBackButton (item.getAvailableWeapons (), backButton, item.getUnitName ());
	
	}

	private void setDetailToBlank ()
	{

		base.itemTitle.text = "";
		base.itemDescription.text = "";
		base.itemLevel.text = "";
		base.goldCost.text = "";
		base.lumberCost.text = "";
		base.HPText.text = "";
		base.damageText.text = "";
		base.attackSpeedText.text = "";
		base.moveSpeedText.text = "";
		base.rangeText.text = "";

		base.HPImage.color = Color.white;
		base.damageImage.color = Color.white;
		base.rangeImage.color = Color.white;
		base.moveSpeedImage.color = Color.white;
		base.attackSpeedImage.color = Color.white;

		base.upgradeButton.gameObject.SetActive (false);

	}

	void onDestroy ()
	{

		backButton.GetComponent<Button> ().onClick.RemoveAllListeners ();

	}



}
