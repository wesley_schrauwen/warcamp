﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class cancelStructureCanvas : MonoBehaviour {

	public Button noButton;
	public Button yesButton;

	private int unitID;

	private matchCoordinator matchInstance;
	private matchCanvasManager sceneManager;

	void Start(){

		sceneManager = GameObject.Find ("Canvas").GetComponent<matchCanvasManager> ();
		matchInstance = GameObject.Find ("MatchCoordinator").GetComponent<matchCoordinator>();

		noButton.onClick.AddListener (() => {

			sceneManager.switchToCanvas(gameObject);

		});

		yesButton.onClick.AddListener (() => {

			matchInstance.cancelStructure(unitID);
			sceneManager.switchToCanvas(gameObject);

		});

	}

	public void cancelStructure(int unitID){

		this.unitID = unitID;

	}

	void OnDestroy(){

		noButton.onClick.RemoveAllListeners();
		yesButton.onClick.RemoveAllListeners();

	}

}
