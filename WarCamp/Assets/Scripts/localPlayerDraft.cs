﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class localPlayerDraft : MonoBehaviour
{

	public Text unitCount;
	public RawImage rank;
	public RawImage weapon;
	public RawImage armour;
	public Text lockInState;
	public RawImage card;

	public GameObject prefab;
	private Button lockInButton;
	private matchCanvasManager sceneManager;

	private bool isLockedIn;
	private int _unitCount;
	private unit _unit;

	void Awake ()
	{
		isLockedIn = false;
		_unitCount = 0;
		unitCount.text = "0";
		lockInState.text = "LOCK IN";
	}

	void Start ()
	{

		sceneManager = GameObject.Find ("Canvas").GetComponent<matchCanvasManager> ();

		card.texture = prefab.GetComponent<unit> ().getCard ();

		lockInButton = gameObject.GetComponent<Button> ();

		lockInButton.onClick.AddListener (() => {

			sceneManager.draftUnit (this.prefab.GetComponent<unit> ().type);

		});

		gameObject.SetActive (profileManager.getInstance ().getLocalPlayerRT ().unitIsAvailable (this.prefab.GetComponent<unit> ().type));

	}

	public void increaseCount ()
	{
		
//		if (_unitCount == 0) {
//		
//			gameObject.SetActive (true);
//
//		}

		unitCount.text = (++_unitCount).ToString ();

	}

	public void decreaseCount ()
	{
		unitCount.text = (--_unitCount).ToString ();

//		if (_unitCount == 0) {
//			gameObject.SetActive (false);
//		}

	}

	public void reset ()
	{
		isLockedIn = false;
		_unitCount = 0;
		unitCount.text = "0";
		lockInState.text = "LOCK IN";
//		gameObject.SetActive (false);
	}

	void OnDestroy ()
	{
		lockInButton.onClick.RemoveAllListeners ();
	}

}
