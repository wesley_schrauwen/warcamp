﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class shopSelectorButton : MonoBehaviour
{

	[Header ("Is Selected")]
	public bool selected = false;

	[Header ("Selector Type")]
	public bool goldSelector = false;
	public bool lumberSelector = false;
	public bool gemSelector = false;

	private Text displayAmount;
	private RawImage frame;
	private Button selector;
	private shopManager shopCanvas;

	void Start ()
	{

		displayAmount = gameObject.GetComponentInChildren<Text> ();
		frame = gameObject.GetComponent<RawImage> ();
		shopCanvas = GameObject.Find ("shopCanvas").GetComponent<shopManager> ();

		selector = gameObject.GetComponent<Button> ();

		selector.onClick.AddListener (selectorClicked);

		if (selected) {
			selectorClicked ();
		} else {
			isNotSelected ();
		}

	}

	public void selectorClicked ()
	{
		
		if (goldSelector) {
			shopCanvas.setGoldSelector (this);
		} else if (lumberSelector) {
			shopCanvas.setLumberSelector (this);
		} else if (gemSelector) {
			shopCanvas.setGemSelector (this);
		}

	}

	public void isSelected ()
	{
		frame.enabled = true;
		displayAmount.color = standardColors.selected;
	}

	public void isNotSelected ()
	{
		frame.enabled = false;
		displayAmount.color = standardColors.text;
	}

	void OnDestroy ()
	{
		isNotSelected ();
	}

	public string getDisplayAmount ()
	{

		return displayAmount.text;

	}

}
