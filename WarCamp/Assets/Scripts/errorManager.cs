﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class errorManager : MonoBehaviour
{

	public Text errorMessage;
	public float displayDuration = 2.5f;

	[Tooltip ("The percentile at which point the message will begin to fade")]
	[Range (0, 1)]
	public float fadeFrom = 0.6f;

	private float durationRemaining;
	private float _fadeFrom;

	void Start ()
	{
		_fadeFrom = displayDuration * (1 - fadeFrom);
	}

	void Update ()
	{

		if (durationRemaining > 0) {

			if (durationRemaining < _fadeFrom) {

				gameObject.GetComponent<RawImage> ().color = standardColors.setAlpha (gameObject.GetComponent<RawImage> ().color, durationRemaining / _fadeFrom);
				errorMessage.color = standardColors.setAlpha (errorMessage.color, durationRemaining / _fadeFrom);
						

			}

			durationRemaining -= Time.deltaTime;

			if (durationRemaining <= 0) {
				gameObject.SetActive (false);
			}

		}

	}

	public void displayError (string error)
	{

		gameObject.SetActive (true);
		durationRemaining = displayDuration;
		errorMessage.text = error;
		gameObject.GetComponent<RawImage> ().color = standardColors.setAlpha (gameObject.GetComponent<RawImage> ().color, 1);
		errorMessage.color = standardColors.setAlpha (errorMessage.color, 1);

	}

}
