﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class constructionCanvasManager : MonoBehaviour
{

	private int buildSiteReference;

	public Text gold;
	public Text lumber;

	public GameObject prefab;
	public GameObject scrollView;

	private bool canvasSetup = false;

	private matchCoordinator matchInstance;

	void Start ()
	{
		matchInstance = GameObject.Find ("MatchCoordinator").GetComponent<matchCoordinator> ();
	}

	public void setupCanvas (int buildSiteReference)
	{

		this.buildSiteReference = buildSiteReference;

		if (!canvasSetup) {
			
			var buildings = profileManager.getInstance ().getLocalPlayerRT ().getStructures ();

			var offset = 0;

			for (int i = 0; i < buildings.Count; i++) {

				if (buildings [i].GetComponent<unit> ().level == 0) {
					return;
				}

				offset++;
				GameObject item = Instantiate (prefab);

				item.GetComponent<structurePrefab> ().setupPrefab (buildings [i].GetComponent<unit> (), this);

				item.GetComponent<RectTransform> ().localPosition = new Vector3 (i * (prefab.GetComponent<RectTransform> ().sizeDelta.x + 24), 0, 0);

				item.transform.SetParent (scrollView.transform, false);


			}


			resizeScrollView (offset);

			canvasSetup = true;

		}

	}

	private void resizeScrollView (int offset)
	{

		float requiredWidth = offset * (prefab.GetComponent<RectTransform> ().sizeDelta.x + 24);

		if (scrollView.GetComponent<RectTransform> ().sizeDelta.x < requiredWidth) {

			scrollView.GetComponent<RectTransform> ().sizeDelta = new Vector2 (requiredWidth, scrollView.GetComponent<RectTransform> ().sizeDelta.y);

		}

	}

	public void requestBuildStructure (unit _unit)
	{
	
		print ("requestBuildStructure called");
		matchInstance.requestBuildStructure (buildSiteReference, _unit);

	}

}
