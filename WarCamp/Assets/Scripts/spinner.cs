﻿using UnityEngine;
using System.Collections;

public class spinner : MonoBehaviour
{

	private bool isSpinning = false;

	public void shouldSpin ()
	{
		isSpinning = true;
	}

	public void stopSpin ()
	{
		isSpinning = false;
	}

	void Update ()
	{
	
		if (isSpinning) {

			gameObject.GetComponent<RectTransform> ().Rotate (0, 0, 150 * Time.deltaTime);

		}

	}
}
