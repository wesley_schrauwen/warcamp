﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class unitLockedInNotification : MonoBehaviour
{

	[Tooltip ("Time to display in seconds")]
	public float displayDuration = 1.5f;

	private Text lockInText;
	private RawImage background;

	private float displayFor = 0;

	void Start ()
	{

		gameObject.SetActive (false);
		background = gameObject.GetComponent<RawImage> ();
		lockInText = gameObject.GetComponentInChildren<Text> ();
		lockInText.text = "DRAFT LOCKED";
	}

	void Update ()
	{
		if (displayFor > 0) {

			displayFor -= Time.deltaTime;

			lockInText.color = new Color (1, 1, 1, 1 * (float)(displayFor / displayDuration));
			background.color = new Color (0, 0, 0, 0.5f * (float)(displayFor / displayDuration));

			if (displayFor <= 0) {
				gameObject.SetActive (false);
			}

		}

	}

	public void displayLockIn ()
	{

		displayFor = displayDuration;
		gameObject.SetActive (true);
		lockInText.color = new Color (1, 1, 1, 1);
		background.color = new Color (0, 0, 0, 0.5f);

	}



}
