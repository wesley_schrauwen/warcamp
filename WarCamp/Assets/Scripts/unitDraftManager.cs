﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class unitDraftManager : MonoBehaviour
{

	public GameObject prefab;

	public GameObject scrollView;
	private GameObject[] scrollViewList;

	private matchCoordinator MatchCoordinator;

	public void setupCanvas ()
	{
		MatchCoordinator = GameObject.Find ("MatchCoordinator").GetComponent<matchCoordinator> ();

		List<GameObject> _clientUnits = new List<GameObject> ();

		foreach (GameObject unit in profileManager.getInstance ().getLocalPlayerRT().getUnits ()) {

			if (unit.GetComponent<unit> ().getUnitLevel () != 0) {
				_clientUnits.Add (unit);
			}

		}

		var clientUnits = _clientUnits.ToArray ();

		scrollViewList = new GameObject[clientUnits.Length];

		var sizeDelta = scrollView.GetComponent<RectTransform> ().sizeDelta;

		var newSizeDelta = new Vector2 (clientUnits.Length * (prefab.GetComponent<RectTransform> ().sizeDelta.x + 24), sizeDelta.y);

		if (newSizeDelta.x > sizeDelta.x) {
			scrollView.GetComponent<RectTransform> ().sizeDelta = newSizeDelta;
		}

		for (int i = 0; i < clientUnits.Length; i++) {

			GameObject item = (GameObject)Instantiate (prefab);

			item.GetComponent<unitDraftPrefab> ().setupPrefab (clientUnits [i].GetComponent<unit> ());

			item.GetComponent<RectTransform> ().position = new Vector3 (i * (item.GetComponent<RectTransform> ().sizeDelta.x + 24), item.GetComponent<RectTransform> ().position.y, 0);

			item.transform.SetParent (scrollView.transform, false);

			scrollViewList [i] = item;

		}
	}

	public void draftUnit (unit unitCode)
	{
		MatchCoordinator.requestUnitDraft (unitCode);
	}

	public void removeUnitDraft (unit unitCode)
	{
		MatchCoordinator.requestRemoveUnitDraft (unitCode);
	}

	public void increaseUnitCount (unit _unit)
	{

		foreach (GameObject item in scrollViewList) {
		
			if (item.GetComponent<unitDraftPrefab> ().getUnitType () == _unit.type) {
			
				item.GetComponent<unitDraftPrefab> ().increaseCount ();
				break;

			}

		}

	}

	public void decreaseUnitCount (unit _unit)
	{

		foreach (GameObject item in scrollViewList) {

			if (item.GetComponent<unitDraftPrefab> ().getUnitType () == _unit.type) {

				item.GetComponent<unitDraftPrefab> ().decreaseCount ();
				break;

			}

		}

	}

	public void resetUnitCount (string unitCode)
	{
		
	}

	public void resetUnitCount ()
	{
		foreach (GameObject item in scrollViewList) {

			item.GetComponent<unitDraftPrefab> ().resetCount ();

		}
	}

	public void reloadUnit (unit _unit)
	{
		foreach (GameObject item in scrollViewList) {
			if (item.GetComponent<unitDraftPrefab> ().getCreepType () == _unit.type) {

				item.GetComponent<unitDraftPrefab> ().reloadPrefab (_unit);
				break;

			}
		}
	}

}
