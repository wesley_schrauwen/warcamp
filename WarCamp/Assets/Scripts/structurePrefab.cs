﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class structurePrefab : MonoBehaviour
{

	public RawImage rank;
	public Text structureName;
	public RawImage card;
	public Text goldCost;
	public Text lumberCost;
	public Text timeCost;
	public Button buildButton;
	public Button containerButton;

	private constructionCanvasManager constructionManager;
	private unit _unit;

	private matchCanvasManager sceneManager = null;

	public void setupPrefab (unit unitObj, constructionCanvasManager reference)
	{

		if (sceneManager == null) {
			sceneManager = GameObject.Find ("Canvas").GetComponent<matchCanvasManager> ();
		}

		this._unit = unitObj;
		constructionManager = reference;

		structureName.text = (this._unit.getUnitName () + "(" + this._unit.getUnitLevel () + ")").ToString ();
		goldCost.text = (this._unit.getGoldCost ()).ToString ();
		lumberCost.text = this._unit.getLumberCost ().ToString ();

		buildButton.onClick.AddListener (() => {

			sceneManager.switchToEquipmentManager (this._unit);


		});

		containerButton.onClick.AddListener (() => {

			constructionManager.requestBuildStructure (this._unit);

		});

	}

	void OnDestroy ()
	{

		buildButton.onClick.RemoveAllListeners ();
		containerButton.onClick.RemoveAllListeners ();

	}
}
