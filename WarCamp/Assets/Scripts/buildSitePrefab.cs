﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class buildSitePrefab : MonoBehaviour
{

	public int siteNumber;
	private bool hasStructure = false;

	private int ownerPeerID = 0;

	private matchCanvasManager sceneManager;

	void Start ()
	{
		sceneManager = GameObject.Find ("Canvas").GetComponent<matchCanvasManager> ();
	}

	void OnMouseUpAsButton ()
	{
		if (!hasStructure && GameManager.getInstance ().getClientPeerID () == ownerPeerID)
			sceneManager.switchToConstructionCanvas (siteNumber);
	}

	public void assignOwner (int ownerID)
	{
		this.ownerPeerID = ownerID;
	}

	public void setHasStructure (bool hasStructure)
	{
		this.hasStructure = hasStructure;

		gameObject.SetActive (!hasStructure);

	}

}
