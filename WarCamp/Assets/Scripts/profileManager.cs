﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class playerProfile
{
	private int gold;
	private int lumber;
	private int gems;
	private int peerID;

	/// <summary>
	/// The subgroup of all units and equipment that are available to this user.
	/// Items of level 0 will also be included in these lists.
	/// </summary>
	private List<GameObject> availableWeapons = new List<GameObject> ();
	private List<GameObject> availableArmour = new List<GameObject> ();
	private List<GameObject> availableStructures = new List<GameObject> ();
	private List<GameObject> availableUnits = new List<GameObject> ();

	#region class setup

	private void setupPlayer (GameSparks.Core.GSData profile)
	{
		setupPlayerProfile (GameManager.getInstance ().getTempProfile ());
	}

	public void setupPlayerProfile (GameSparks.Core.GSData profile)
	{

		var inventory = profile.GetGSData ("inventory");
		var GSArmour = inventory.GetGSData ("armour");
		var GSUnits = inventory.GetGSData ("units");
		var GSWeapons = inventory.GetGSData ("weapons");
		var GSDefenses = inventory.GetGSData ("defenses");

		foreach (GameObject armour in profileManager.getInstance().getAllArmour()) {
			var armourRAW = GSArmour.GetGSData (armour.GetComponent<equipment> ().equipmentName);

			if (armourRAW != null) {				
				setupEquipment (armour, armourRAW);	
				addArmour (armour);
			}
		}

		foreach (GameObject weapon in profileManager.getInstance().getAllWeapons()) {
			var weaponRAW = GSWeapons.GetGSData (weapon.GetComponent<equipment> ().equipmentName);

			if (weaponRAW != null) {				
				setupEquipment (weapon, weaponRAW);
				addWeapon (weapon);
			}
		}

		foreach (GameObject structure in profileManager.getInstance().getAllDefenses()) {
			var structureRAW = GSDefenses.GetGSData (structure.GetComponent<unit> ().unitName);

			if (structureRAW != null) {				
				setupUnit (structure, structureRAW);
				addStructure (structure);
			}
		}

		foreach (GameObject unit in profileManager.getInstance().getAllUnits()) {
			var unitRAW = GSUnits.GetGSData (unit.GetComponent<unit> ().unitName);

			if (unitRAW != null) {				
				setupUnit (unit, unitRAW);
				addUnit (unit);
			}
		}
	}



	public void setupPeerPlayer (GameSparks.Core.GSData profile, int peerID)
	{
		this.peerID = peerID;
		setupPlayer (profile);
	}

	public void setupLocalPlayer (GameSparks.Core.GSData profile)
	{
		gold = (int)profile.GetInt ("gold");
		lumber = (int)profile.GetInt ("lumber");
		gems = (int)profile.GetInt ("gems");

		setupPlayer (profile);
	}

	#endregion

	#region getters and setters

	public List<GameObject> getWeapons ()
	{
		return availableWeapons;
	}

	public List<GameObject> getArmour ()
	{
		return availableArmour;
	}

	public List<GameObject> getStructures ()
	{
		return availableStructures;
	}

	public List<GameObject> getUnits ()
	{
		return availableUnits;
	}

	public void addArmour (GameObject armour)
	{
		availableArmour.Add (armour);
	}

	public void addWeapon (GameObject weapon)
	{
		availableWeapons.Add (weapon);
	}

	public void addStructure (GameObject structure)
	{
		availableStructures.Add (structure);
	}

	public void addUnit (GameObject unit)
	{
		availableUnits.Add (unit);
	}

	public int getGold ()
	{
		return gold;
	}

	public int getLumber ()
	{
		return lumber;
	}

	public int getGems ()
	{
		return gems;
	}

	public void setGold (int gold)
	{

		this.gold = gold > 2500 ? 2500 : gold;
	}

	public void setLumber (int lumber)
	{
		this.lumber = lumber > 2500 ? 2500 : lumber;
	}

	public void setGems (int gems)
	{

		this.gems = gems > 2500 ? 2500 : gems;

	}

	public void decreaseGold (int amount)
	{
		this.gold -= amount;

		if (this.gold < 0) {
			this.gold = 0;
		}
	}

	public void decreaseLumber (int amount)
	{
		this.lumber -= amount;

		if (this.lumber < 0) {
			this.lumber = 0;
		}

	}

	public void decreaseGems (int amount)
	{
		this.gems -= amount;

		if (this.gems < 0) {
			this.gems = 0;
		}

	}

	public void increaseGold (int amount)
	{

		this.gold = this.gold + amount > 2500 ? 2500 : this.gold + amount;

	}

	public void increaseLumber (int amount)
	{
		this.lumber += this.lumber + amount > 2500 ? 2500 : this.lumber + amount;
	}

	public void increaseGems (int amount)
	{
		this.gems += this.gems + amount > 2500 ? 2500 : this.gems + amount;
	}

	public int getPeerID ()
	{
		return peerID;
	}

	public void setUnitWeapon (string unitCode, string weapon)
	{

		GameObject unit = getUnitOfType (unitCode);

		unit.GetComponent<unit> ().setEquipment (true, weapon);

	}

	public void setUnitArmour (string unitCode, string armour)
	{

		GameObject unit = getUnitOfType (unitCode);

		unit.GetComponent<unit> ().setEquipment (false, armour);

	}

	public unit updateUnitEquipment (string unitCode, string weapon, string armour)
	{

		GameObject unit = getUnitOfType (unitCode);

		unit.GetComponent<unit> ().setEquipment (true, weapon);
		unit.GetComponent<unit> ().setEquipment (false, armour);

		return unit.GetComponent<unit> ();
	}

	public void setUnitEquipment (string unitCode, string weapon, string armour)
	{

		GameObject unit = getUnitOfType (unitCode);

		unit.GetComponent<unit> ().setEquipment (true, weapon);
		unit.GetComponent<unit> ().setEquipment (false, armour);

	}

	#endregion

	public GameObject getUnitOfType (creepType type)
	{

		foreach (GameObject unit in availableUnits) {
			if (unit.GetComponent<unit> ().type == type) {
				return unit;
			}
		}

		foreach (GameObject building in availableStructures) {
			if (building.GetComponent<unit> ().type == type) {
				return building;
			}
		}

		if (type == creepType.CONSTRUCTION_SITE) {

			return profileManager.getInstance ().constructionSite;

		}

		return new GameObject ();

	}

	public bool unitIsAvailable (creepType type)
	{
		foreach (GameObject unit in availableUnits) {
			if (unit.GetComponent<unit> ().getUnitType () == type && unit.GetComponent<unit> ().level > 0) {
				return true;
			}
		}

		foreach (GameObject building in availableStructures) {
			if (building.GetComponent<unit> ().getUnitType () == type && building.GetComponent<unit> ().level > 0) {
				return true;
			}
		}

		if (type == creepType.CONSTRUCTION_SITE) {

			return true;

		}

		return false;
	}

	public GameObject getUnitOfType (string type)
	{

		foreach (GameObject unit in availableUnits) {
			if (unit.GetComponent<unit> ().getUnitName ().Equals (type)) {
				return unit;
			}
		}

		foreach (GameObject building in availableStructures) {
			if (building.GetComponent<unit> ().getUnitName ().Equals (type)) {
				return building;
			}
		}

		if (type.Equals ("construction_site")) {
			
			return profileManager.getInstance ().constructionSite;

		}

		return new GameObject ();

	}

	public GameObject getEquipmentOfType (string type)
	{

		foreach (GameObject armour in availableArmour) {

			if (armour.GetComponent<equipment> ().equipmentName.Equals (type)) {
				return armour;
			}

		}

		foreach (GameObject weapon in availableWeapons) {

			if (weapon.GetComponent<equipment> ().equipmentName.Equals (type)) {

				return weapon;

			}

		}

		return new GameObject ();

	}

	/// <summary>
	/// Returns a unit or equipment gameobject given the group and name.
	/// If no matching gameobject was found will return a null gameobject.
	/// </summary>
	/// <param name="group"></param>
	/// <param name="itemName"></param>
	/// <returns></returns>
	public GameObject getItemOfTypeInGroup (string group, string itemName)
	{

		switch (group) {
		case "armour":

			return getEquipmentOfType (itemName);

		case "weapons":

			return getEquipmentOfType (itemName);

		case "units":

			return getUnitOfType (itemName);

		case "defenses":

			return getUnitOfType (itemName);

		}

		return new GameObject ();

	}

	#region convenience functions

	private void setupEquipment (GameObject obj, GameSparks.Core.GSData rawData)
	{

		obj.GetComponent<equipment> ().setupEquipment (
			(int)rawData.GetInt ("level"),
			(int)rawData.GetInt ("max_level"),
			(bool)rawData.GetBoolean ("is_upgrading"),
			(ulong)rawData.GetLong ("upgrade_complete"), 
			(ulong)rawData.GetLong ("upgrade_begin"));

	}

	private void setupUnit (GameObject obj, GameSparks.Core.GSData rawData)
	{

		obj.GetComponent<unit> ().setupUnit (
			(int)rawData.GetInt ("level"),
			(int)rawData.GetInt ("max_level"), 
			(bool)rawData.GetBoolean ("is_upgrading"),
			(ulong)rawData.GetLong ("upgrade_complete"),
			(ulong)rawData.GetLong ("upgrade_begin"),
			(string)rawData.GetString ("weapon"),
			(string)rawData.GetString ("armour"),
			rawData.GetStringList ("available_weapons"),
			rawData.GetStringList ("available_armour"));

	}

	#endregion
}

public class profileManager : MonoBehaviour
{
	public GameObject[] allWeapons;
	public GameObject[] allArmour;
	public GameObject[] allDefenses;
	public GameObject[] allUnits;
	public GameObject constructionSite;

	private playerProfile localPlayer = new playerProfile ();
	private List<playerProfile> peers = new List<playerProfile> ();

	private static profileManager instance = null;

	public void Awake ()
	{
		if (instance == null) {
			instance = this;
			DontDestroyOnLoad (this.gameObject);
		} else {
			Destroy (gameObject);
		}

		localPlayer.setupLocalPlayer (GameManager.getInstance ().getTempProfile ());
	}

	#region getters and setters

	/// <summary>
	/// Gets the local player object used in the local session.
	/// </summary>
	/// <returns>The local player.</returns>
	public playerProfile getLocalPlayer ()
	{
		return localPlayer;
	}

	public List<playerProfile> getPeers ()
	{
		return peers;
	}

	/// <summary>
	/// Gets the peer with the passed in peerID.
	/// If it cannot find a matching peer will return a null object.
	/// </summary>
	/// <returns>The peer.</returns>
	/// <param name="peerID">Peer ID.</param>
	public playerProfile getPeerProfile (int peerID)
	{
		foreach (playerProfile peer in peers) {
			if (peer.getPeerID () == peerID) {
				return peer;
			}
		}

		return new playerProfile ();

	}

	public void addPeer (GameSparks.Api.Messages.MatchFoundMessage._Participant player)
	{
		playerProfile peerProfile = new playerProfile ();
		peerProfile.setupPeerPlayer (player.ScriptData, (int)player.PeerId);
		peers.Add (peerProfile);
	}

	public void emptyPeerList ()
	{
		peers.Clear ();
	}

	public GameObject[] getAllArmour ()
	{
		return allArmour;
	}

	public GameObject[] getAllWeapons ()
	{
		return allWeapons;
	}

	public GameObject[] getAllDefenses ()
	{
		return allDefenses;
	}

	public GameObject[] getAllUnits ()
	{
		return allUnits;
	}

	public List<GameObject> getArmourForUnit (List<string> requiredArmour, string selectedArmour)
	{
		List<GameObject> package = new List<GameObject> ();

		foreach (string required in requiredArmour) {
			foreach (GameObject available in localPlayer.getArmour()) {
				string equipmentName = available.GetComponent<equipment> ().equipmentName;
				if (equipmentName.Equals (required)) {

					if (equipmentName.Equals (selectedArmour)) {
						package.Insert (0, available);
					} else {
						package.Add (available);
					}
					break;
				}
			}
		}
		return package;
	}

	public GameObject getArmourForUnit (string selectedArmour)
	{
		
		foreach (GameObject available in localPlayer.getArmour()) {
		
			string equipmentName = available.GetComponent<equipment> ().equipmentName;

			if (equipmentName.Equals (selectedArmour)) {

				return available;
				break;

			}

		}

		return localPlayer.getArmour () [0];

	}

	public List<GameObject> getWeaponForUnit (List<string> requiredWeapons, string selectedWeapon)
	{
		List<GameObject> package = new List<GameObject> ();

		foreach (string required in requiredWeapons) {
			foreach (GameObject available in localPlayer.getWeapons()) {
				string equipmentName = available.GetComponent<equipment> ().equipmentName;
				if (equipmentName.Equals (required)) {

					if (equipmentName.Equals (selectedWeapon)) {
						package.Insert (0, available);
					} else {
						package.Add (available);
					}						
					break;
				}
			}
		}
		return package;
	}

	public GameObject getWeaponForUnit (string selectedWeapon)
	{

		foreach (GameObject available in localPlayer.getWeapons()) {

			string equipmentName = available.GetComponent<equipment> ().equipmentName;

			if (equipmentName.Equals (selectedWeapon)) {

				return available;
				break;

			}

		}

		return localPlayer.getWeapons () [0];

	}

	public static profileManager getInstance ()
	{
		return instance;
	}

	/// <summary>
	/// Gets the local player object used in the RT Session.
	/// </summary>
	/// <returns>The local player's RT session.</returns>
	public playerProfile getLocalPlayerRT ()
	{
		return peers [GameManager.getInstance ().getClientPeerID () - 1]; // list starts at 0; clientPeerID starts at 1
	}

	public void setUnitEquipment ()
	{
		
	}

	#endregion
}
