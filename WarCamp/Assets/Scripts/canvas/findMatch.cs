﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class findMatch : MonoBehaviour
{

	private capitalCanvasManager sceneManager;

	void Start ()
	{

		sceneManager = GameObject.Find ("Canvas").GetComponent<capitalCanvasManager> ();

		GetComponent<Button> ().onClick.AddListener (() => {
			sceneManager.displayMatchMaking ();
			registerForMatchMaking ();
		});
	}

	private void registerForMatchMaking ()
	{
		GameManager.getInstance ().beginMatchMaking (sceneManager);
	}
}
