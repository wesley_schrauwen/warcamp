﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class detailCanvasNavigator : MonoBehaviour {

    public Button backButton;
    public Button upgradeButton;
    public Button changeSkinButton;
    public GameObject informationPanel;
    public GameObject upgradeCanvas;

    private capitalCanvasManager capitalCanvasRef;

    void Start() {
        capitalCanvasRef = GameObject.Find("capitalCanvas").GetComponent<capitalCanvasManager>();

		backButton.GetComponent<Button> ();
        backButton.onClick.AddListener(() => {
            capitalCanvasRef.popCanvas();
        });

        upgradeButton.onClick.AddListener(()=> {
            capitalCanvasRef.pushCanvas(upgradeCanvas);
        });

        changeSkinButton.onClick.AddListener(()=> {
            // TODO: at some point we will use this button to offer unique skins.
        });
    }


}
