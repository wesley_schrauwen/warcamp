﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class capitalCanvasManager : MonoBehaviour
{

	[Header ("Canvas")]
	public GameObject matchFoundCanvas;
	public GameObject armourCanvas;
	public GameObject weaponCanvas;
	public GameObject unitSelectorCanvas;
	public GameObject unitEditorCanvas;
	public GameObject selectItemCanvas;
	public GameObject upgradeCanvas;
	public GameObject shopCanvas;
	public GameObject errorCanvas;
	public GameObject loadingBar;

	public Text goldText;
	public Text lumberText;
	public Text gemText;

	[Header ("Rates")]
	public float goldGainPM = 24f;
	public float lumberGainPM = 24f;

	private float gold;
	private float lumber;

	private float goldGained = 0f;
	private float lumberGained = 0f;
	private float tick = 0f;

	private Stack canvasStack = new Stack ();
	private GameObject currentCanvas;

	void Start ()
	{

		currentCanvas = gameObject;

		gold = profileManager.getInstance ().getLocalPlayer ().getGold ();
		lumber = profileManager.getInstance ().getLocalPlayer ().getLumber ();

		updateResources ();

	}

	void Update ()
	{

		tick += Time.deltaTime;

		if (tick > 1f) {

			tick = 0f;

			goldGained += 0.4f; // 0.4 = 24 (gold gain rate) / 60
			lumberGained += 0.4f;

			profileManager.getInstance ().getLocalPlayer ().setGold ((int)(gold + goldGained));
			profileManager.getInstance ().getLocalPlayer ().setLumber ((int)(gold + goldGained));

			updateResources ();

		}


	}

	public void updateResources ()
	{

		goldText.text = profileManager.getInstance ().getLocalPlayer ().getGold ().ToString ();
		lumberText.text = profileManager.getInstance ().getLocalPlayer ().getLumber ().ToString ();
		gemText.text = profileManager.getInstance ().getLocalPlayer ().getGems ().ToString ();

	}

	#region canvasNavigation

	public void switchToCanvas (GameObject canvas)
	{

		if (currentCanvas != null &&
		    currentCanvas.name.Equals (canvas.name) &&
		    canvasStack.Count > 0) {

			popCanvas ();

		} else {

			pushCanvas (canvas);

		}

	}

	public void pushCanvas (GameObject canvas)
	{

		if (currentCanvas != null && currentCanvas.name.Equals (canvas.name)) { // prevent the same canvas from being pushed onto itself
			return;
		}

		canvasStack.Push (currentCanvas);
		currentCanvas = canvas;
		canvas.SetActive (true);

	}

	/// <summary>
	/// Pops the current canvas and displays the previous one in the stack.
	/// </summary>
	public void popCanvas ()
	{

		currentCanvas.SetActive (false);

		if (canvasStack.Count != 0) {
			currentCanvas = (GameObject)canvasStack.Pop ();
			currentCanvas.SetActive (true);
		} else {
			currentCanvas = gameObject;
		}			

	}

	/// <summary>
	/// Pops the canvas iff the current canvas equals the parameter.
	/// </summary>
	/// <param name="canvas">The canvas to pop.</param>
	public void popCanvas (string canvasName)
	{

		if (currentCanvas.name.Equals (canvasName)) {

			currentCanvas.SetActive (false);
			currentCanvas = (GameObject)canvasStack.Pop ();
			currentCanvas.SetActive (true);

			if (canvasStack.Count == 0) {
				currentCanvas = null;
			}

		}

	}

	/// <summary>
	/// Displays the insufficient resources error.
	/// </summary>
	public void displayInsufficientResources ()
	{

		errorCanvas.GetComponent<errorManager> ().displayError ("Insufficient Resources");

	}

	public void displayUpgradeRequestFailed ()
	{
		errorCanvas.GetComponent<errorManager> ().displayError ("Upgrade Request Failed");
	}

	public void reloadUpgradeUI ()
	{
		upgradeCanvas.GetComponent<upgradeManager> ().reloadUI ();
	}

	#endregion

	#region matchmaking

	public void matchFoundAlert (GameSparks.Api.Messages.MatchFoundMessage message)
	{

		matchFoundCanvas.GetComponent<matchFoundCanvas> ().setupCanvas ("John", "Jane");
		pushCanvas (matchFoundCanvas);
		loadingBar.GetComponent<spinner> ().stopSpin ();

	}

	public void matchNotFoundAlert (GameSparks.Api.Messages.MatchNotFoundMessage message)
	{
		
	}

	public void cancelMatchFound ()
	{
		matchFoundCanvas.SetActive (false);
		// TODO: delete the local stored session in game manager
	}

	public void acceptMatchFound ()
	{
		GameManager.getInstance ().connectToMatch ();
	}

	#endregion

	#region requests

	public void requestUpgrade (string subgroup, string item)
	{

		GameManager.getInstance ().upgradeItemRequest (subgroup, item);

	}

	#endregion

	#region custom navigation

	public void displayArmour ()
	{
		switchToCanvas (armourCanvas);
		armourCanvas.GetComponent<equipmentCanvasManager> ().displayArmour ();
	}

	public void displayWeapons ()
	{
		switchToCanvas (weaponCanvas);
		weaponCanvas.GetComponent<weaponCanvasManager> ().displayWeapons ();
	}

	public void displayBuildings ()
	{
		switchToCanvas (unitSelectorCanvas);
		unitSelectorCanvas.GetComponent<unitSelectorManager> ().displayBuildings ();
	}

	public void displayUnits ()
	{
		switchToCanvas (unitSelectorCanvas);
		unitSelectorCanvas.GetComponent<unitSelectorManager> ().displayUnits ();
	}

	public void displayUnitEditor (unit _unit)
	{
		unitEditorCanvas.GetComponent<unitEditorManager> ().setupUnitEditor (_unit);
		switchToCanvas (unitEditorCanvas);
	}

	public void displayItemSelector (unit _unit, string itemType)
	{
	
		selectItemCanvas.GetComponent<selectItemManager> ().setupCanvas (_unit, itemType);
		pushCanvas (selectItemCanvas);

	}

	public void displayUpgradeCanvas (equipment item)
	{
		upgradeCanvas.GetComponent<upgradeManager> ().displayUpgrade (item);
		pushCanvas (upgradeCanvas);
	}

	public void displayUpgradeCanvas (unit item)
	{
		upgradeCanvas.GetComponent<upgradeManager> ().displayUpgrade (item);
		pushCanvas (upgradeCanvas);
	}

	public void updateUnitEditor (unit _unit)
	{
	
		popCanvas (selectItemCanvas.name);
		unitSelectorCanvas.GetComponent<unitSelectorManager> ().updateItems ();
		unitEditorCanvas.GetComponent<unitEditorManager> ().updateStats (_unit);

	}

	public void displayMatchMaking ()
	{

		loadingBar.SetActive (true);
		loadingBar.GetComponent<spinner> ().shouldSpin ();
		popCanvas ();

	}

	#endregion

	#region server responses

	public void upgradeResponse (bool success, int gold, int lumber, long upgradeComplete, long upgradeBegin, string subgroup, string itemName)
	{

		if (success) {

			if (subgroup.Equals ("armour") || subgroup.Equals ("weapons")) {
				
				equipment item = profileManager.getInstance ().getLocalPlayer ().getEquipmentOfType (itemName).GetComponent<equipment> ();

				item.upgrade.isUpgrading = true;
				item.upgrade.upgradeComplete = (ulong)upgradeComplete;
				item.upgrade.upgradeBegin = (ulong)upgradeBegin;

				armourCanvas.GetComponent<equipmentCanvasManager> ().reloadUIWithDetail (itemName);


			} else {
				
				unit item = profileManager.getInstance ().getLocalPlayer ().getUnitOfType (itemName).GetComponent<unit> ();
				item.upgrade.isUpgrading = true;
				item.upgrade.upgradeComplete = (ulong)upgradeComplete;
				item.upgrade.upgradeBegin = (ulong)upgradeBegin;

				unitEditorCanvas.GetComponent<unitEditorManager> ();

			}


			profileManager.getInstance ().getLocalPlayer ().decreaseGold (gold);
			profileManager.getInstance ().getLocalPlayer ().decreaseLumber (lumber);
			updateResources ();

			upgradeCanvas.GetComponent<upgradeManager> ().reloadUI ();


            
		} else {



		}

	}



	#endregion

}
