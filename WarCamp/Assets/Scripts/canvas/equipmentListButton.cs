﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class equipmentListButton : MonoBehaviour
{

	public Text buttonText;

	private equipment equipmentItem;
	private unit unitItem;

	private MonoBehaviour parent;

	/// <summary>
	/// Use this function to setup the list element for the equipmentCanvas in the capital Scene.
	/// </summary>
	/// <param name="parent">Parent Script.</param>
	/// <param name="item">Equipment Script.</param>
	public void setupObject (equipmentCanvasManager parent, equipment item)
	{
	
		initObject (parent, item);

		gameObject.GetComponent<Button> ().onClick.AddListener (() => {

			((equipmentCanvasManager)this.parent).displayItem (this.equipmentItem, this);

		});
			
	}

	public void setupObject (weaponCanvasSideMenu parent, unit unitItem)
	{

		initObject (parent, unitItem);

		gameObject.GetComponent<Button> ().onClick.AddListener (() => {

			((weaponCanvasSideMenu)this.parent).unitSelected (this.unitItem);

		});

	}

	public void setupObject (selectItemManager parent, equipment item)
	{
		initObject (parent, item);

		gameObject.GetComponent<Button> ().onClick.AddListener (() => {

			((selectItemManager)this.parent).displayItem (this.equipmentItem, this);

		});
	}

	public void initObject (MonoBehaviour parent, equipment item)
	{
		this.equipmentItem = item;
		initObject (parent, item.equipmentName);
	}

	public void initObject (MonoBehaviour parent, unit item)
	{

		this.unitItem = item;
		initObject (parent, item.getUnitName ());

	}

	private void initObject (MonoBehaviour parent, string _buttonText)
	{
		this.parent = parent;
		buttonText.text = _buttonText.ToUpper ();
	}


	public void isSelected ()
	{
		buttonText.color = standardColors.selected;
	}

	public void isNotSelected ()
	{
		buttonText.color = standardColors.unselected;
	}

	void OnDestroy ()
	{
		gameObject.GetComponent<Button> ().onClick.RemoveAllListeners ();
	}

}
