﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class equipmentCanvasManager : MonoBehaviour
{

	public Text pageTitle;
	public Text itemTitle;
	public Text itemDescription;
	public Text itemLevel;
	public Text goldCost;
	public Text lumberCost;
	public Text HPText;
	public RawImage HPImage;
	public Text damageText;
	public RawImage damageImage;
	public Text moveSpeedText;
	public RawImage moveSpeedImage;
	public Text attackSpeedText;
	public RawImage attackSpeedImage;
	public Text rangeText;
	public RawImage rangeImage;
	public Button upgradeButton;

	public GameObject scrollView;
	public GameObject scrollPrefab;

	private GameObject[] scrollViewItems;
	private equipmentListButton currentlySelected;
	private capitalCanvasManager sceneManager;
	private equipment currentlyDisplayed;

	void Start ()
	{
		sceneManager = GameObject.Find ("Canvas").GetComponent<capitalCanvasManager> ();
	}

	/// <summary>
	/// Prepares the canvas for displaying a particular item.
	/// </summary>
	public void displayItem (equipment item, equipmentListButton scriptReference)
	{
		itemTitle.text = item.equipmentName.ToUpper ();
		itemDescription.text = item.itemDescription;

		currentlySelected.isNotSelected ();
		currentlySelected = scriptReference;
		currentlySelected.isSelected ();

		GameObject[] allItems;

		if (pageTitle.text.Equals ("ARMOUR")) {
			allItems = profileManager.getInstance ().getAllArmour ();
		} else {
			allItems = profileManager.getInstance ().getAllWeapons ();
		}

		setupDetail (item, allItems);
	}

	/// <summary>
	/// Prepares the canvas for displaying armour.
	/// </summary>
	public void displayArmour ()
	{
		var allArmour = profileManager.getInstance ().getAllArmour ();
		var allClientArmour = profileManager.getInstance ().getLocalPlayer ().getArmour ().ToArray ();

		pageTitle.text = "ARMOUR";

		setupCanvas (allArmour, allClientArmour);
			
	}

	/// <summary>
	/// Prepares the canvas for displaying all weapons.
	/// </summary>
	public void displayWeaponsWithBackButton ()
	{

		var allWeapons = profileManager.getInstance ().getAllWeapons ();
		var allClientWeapons = profileManager.getInstance ().getAllWeapons ();
		pageTitle.text = "WEAPONS";

		setupCanvas (allWeapons, allClientWeapons);

	}

	/// <summary>
	/// Prepares the canvas for displaying a subset of weapons and inserts a back button
	/// </summary>
	/// <param name="allWeapons">All weapons.</param>
	/// <param name="allClientWeapons">All client weapons.</param>
	public void displayWeaponsWithBackButton (GameObject[] unitWeapons, GameObject backButton, string title)
	{

		pageTitle.text = title.ToUpper ();

		setupCanvas (unitWeapons, unitWeapons);

		backButton.transform.SetParent (scrollView.transform, false);

		backButton.GetComponent<RectTransform> ().localPosition = new Vector2 (0, 
			-scrollView.GetComponent<RectTransform> ().sizeDelta.y);
		
		scrollView.GetComponent<RectTransform> ().sizeDelta = new Vector2 (scrollView.GetComponent<RectTransform> ().sizeDelta.x, 
			scrollView.GetComponent<RectTransform> ().sizeDelta.y + backButton.GetComponent<RectTransform> ().sizeDelta.y);



	}

	/// <summary>
	/// Sets up the canvas. (Scroll view)
	/// </summary>
	private void setupCanvas (GameObject[] localItems, GameObject[] clientItems)
	{
		scrollViewItems = new GameObject[localItems.Length];

		resizeScrollView (localItems.Length);

		for (int i = 0; i < localItems.Length; i++) {

			GameObject item = Instantiate (scrollPrefab);

			item.GetComponent<RectTransform> ().position = new Vector3 (
				0, 
				-scrollPrefab.GetComponent<RectTransform> ().sizeDelta.y * i, 
				0);

			item.transform.SetParent (scrollView.transform, false);	
			item.GetComponent<equipmentListButton> ().setupObject (this, localItems [i].GetComponent<equipment> ());

			if (i == 0) {
				item.GetComponent<equipmentListButton> ().isSelected ();
				currentlySelected = item.GetComponent<equipmentListButton> ();
				itemTitle.text = localItems [i].GetComponent<equipment> ().equipmentName.ToUpper ();
				itemDescription.text = localItems [i].GetComponent<equipment> ().itemDescription;
			} else {
				item.GetComponent<equipmentListButton> ().isNotSelected ();
			}

			scrollViewItems [i] = item;
		}

		setupDetail (localItems [0].GetComponent<equipment> (), clientItems);
	}

	/// <summary>
	/// Sets up the detail. (The stats and blurb)
	/// </summary>
	private void setupDetail (equipment selectedItem, GameObject[] allClientItems)
	{
		upgradeButton.onClick.RemoveAllListeners ();

		foreach (GameObject clientArmour in allClientItems) {

			var equipmentReference = clientArmour.GetComponent<equipment> ();
			this.currentlyDisplayed = equipmentReference;

			if (selectedItem.equipmentName.Equals (equipmentReference.equipmentName)) {

				itemLevel.text = "Level " + equipmentReference.level;

				setupLabel (goldCost, equipmentReference.stats.goldCostCH);
				setupLabel (lumberCost, equipmentReference.stats.lumberCostCH);

				setupLabel (HPText, HPImage, equipmentReference.stats.hpCH);
				setupLabel (damageText, damageImage, equipmentReference.stats.damageCH);
				setupLabel (moveSpeedText, moveSpeedImage, equipmentReference.stats.moveSpeedCH);
				setupLabel (attackSpeedText, attackSpeedImage, equipmentReference.stats.attackSpeedCH);
				setupLabel (rangeText, rangeImage, equipmentReference.stats.rangeCH);

				upgradeButton.onClick.AddListener (() => {

					sceneManager.displayUpgradeCanvas (this.currentlyDisplayed);

				});

				break;

			}

		}
	}

	/// <summary>
	/// Reloads the UI.
	/// Used when a data set has been modified.
	/// </summary>
	public void reloadUI ()
	{

		if (pageTitle.text.Equals ("ARMOUR"))
			displayArmour ();
		else
			displayWeaponsWithBackButton ();

	}

	/// <summary>
	/// Reloads the user interface to display a specified detail.
	/// Used when upgrading to not change the users current detail view.
	/// Will only reload the detail if the user has not navigated away.
	/// </summary>
	/// <param name="detail">Detail.</param>
	public void reloadUIWithDetail (string itemName)
	{
		if (currentlyDisplayed.equipmentName.Equals (itemName)) {
			displayItem (currentlyDisplayed, currentlySelected);
		}
	}

	private void setupLabel (Text textCanvas, int stat)
	{
		if (stat > 0) {
			textCanvas.text = "+" + stat;
		} else if (stat < 0) {
			textCanvas.text = "" + stat;
		} else {
			textCanvas.text = "0";
		}
	}

	private void setupLabel (Text textCanvas, RawImage texture, int stat)
	{

		if (stat > 0) {
			textCanvas.text = "+" + stat;
			texture.color = standardColors.green;
		} else if (stat < 0) {
			textCanvas.text = "" + stat;
			texture.color = standardColors.red;
		} else {
			textCanvas.text = "No Change";
			texture.color = standardColors.yellow;
		}
	
	}

	private void resizeScrollView (int yCount)
	{		

		Vector2 coords = new Vector2 (scrollView.GetComponent<RectTransform> ().sizeDelta.x, yCount * scrollPrefab.GetComponent<RectTransform> ().sizeDelta.y);
		scrollView.GetComponent<RectTransform> ().sizeDelta = coords;

	}

	protected void cleanCanvas ()
	{
		foreach (GameObject item in scrollViewItems) {
			Destroy (item);
		}

		scrollViewItems = null;
		upgradeButton.onClick.RemoveAllListeners ();
	}

	void OnDisable ()
	{
		cleanCanvas ();
	}
}
