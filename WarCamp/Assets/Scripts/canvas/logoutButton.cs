﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class logoutButton : MonoBehaviour
{

	// Use this for initialization
	void Start ()
	{
		gameObject.GetComponent<Button> ().onClick.AddListener (() => {

			GameManager.getInstance ().logout ();

		});
	}

	void OnDestroy ()
	{
		gameObject.GetComponent<Button> ().onClick.RemoveAllListeners ();
	}

}
