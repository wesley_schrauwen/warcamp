﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class switchToCanvas : MonoBehaviour
{

	public GameObject canvas;

	public bool onCapitalScene = false;
	public bool onMatchScene = false;
	//	public bool onLoginScene = false;

	private capitalCanvasManager capitalSceneReference;
	private matchCanvasManager matchSceneReference;

	void Start ()
	{
	
		if (onCapitalScene) {
			capitalSceneReference = GameObject.Find ("Canvas").GetComponent<capitalCanvasManager> ();
		} else if (onMatchScene) {
			matchSceneReference = GameObject.Find ("Canvas").GetComponent<matchCanvasManager> ();
		}


		gameObject.GetComponent<Button> ().onClick.AddListener (() => {

			if (onCapitalScene) {
				capitalSceneReference.switchToCanvas (canvas);
			} else if (onMatchScene) {
				matchSceneReference.switchToCanvas (canvas);
			}
				
		});

	}


}
