﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class selectorButton : MonoBehaviour
{
	[Header ("This script should only be used on the match and capital scenes.")]

	public bool onCapitalScene = false;
	[Header ("Only select one scene")]
	public bool displayArmour = false;
	public bool displayBuildings = false;
	public bool displayUnits = false;
	public bool displayWeapons = false;

	private capitalCanvasManager canvasManager;

	void Start ()
	{

		canvasManager = GameObject.Find ("Canvas").GetComponent<capitalCanvasManager> ();
	
		gameObject.GetComponent<Button> ().onClick.AddListener (() => {

			if (displayArmour) {

				canvasManager.displayArmour ();

			} else if (displayBuildings) {

				canvasManager.displayBuildings ();

			} else if (displayUnits) {

				canvasManager.displayUnits ();
				
			} else if (displayWeapons) {

				canvasManager.displayWeapons ();

			}

		});

	}

	void OnDestroy ()
	{
		gameObject.GetComponent<Button> ().onClick.RemoveAllListeners ();
	}
}
