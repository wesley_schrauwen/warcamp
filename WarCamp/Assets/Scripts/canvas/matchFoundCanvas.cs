﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class matchFoundCanvas : MonoBehaviour
{

	public Button cancelButton;
	public Button acceptButton;
	public Text player1Text;
	public Text player2Text;
	private capitalCanvasManager capitalCanvasRef;
	public Image player1ReadyImage;
	public Image player2ReadyImage;

	// Use this for initialization
	void Start ()
	{	
		capitalCanvasRef = GameObject.Find ("Canvas").GetComponent<capitalCanvasManager> ();
		cancelButton.onClick.AddListener (capitalCanvasRef.cancelMatchFound);
		acceptButton.onClick.AddListener (() => {


			capitalCanvasRef.acceptMatchFound ();

		});
	}

	public void setupCanvas (string player1DisplayName, string player2DisplayName)
	{
		player1Text.text = player1DisplayName;
		player2Text.text = player2DisplayName;
	}

	public void playerTickedReady ()
	{
		
	}
}
