﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class mainCanvasNavigator : MonoBehaviour {

	public Button units;
	public Button weapons;
	public Button armor;
	public Button buildings;
	public Button team;

    private capitalCanvasManager capitalCanvasRef;

	public GameObject unitsPlabe;
	public GameObject weaponsPlabe;
	public GameObject armorPlabe;
	public GameObject buildingsPlane;
	public GameObject teamPlane;

	void Start () {
        capitalCanvasRef = GameObject.Find("capitalCanvas").GetComponent<capitalCanvasManager>();

        units.onClick.AddListener(() => {
			showAvailableUnits();
			capitalCanvasRef.pushCanvas(unitsPlabe);
        });
		weapons.onClick.AddListener(() => {
			showAvailableWeapons();
			capitalCanvasRef.pushCanvas(weaponsPlabe);
		});
		armor.onClick.AddListener(() => {
			showAvailableArmor();
			capitalCanvasRef.pushCanvas(armorPlabe);
		});
        buildings.onClick.AddListener(() => {
			showAvailableBuildings();
			capitalCanvasRef.pushCanvas(buildingsPlane);
        });
		team.onClick.AddListener(() => {
			capitalCanvasRef.pushCanvas(teamPlane);
		});
    }

	public void showAvailableUnits () 
	{
		availableUnits.AssetPath = "/resources/cards/units/Images";
		availableUnits.cardType = "Unit";
	}

	public void showAvailableWeapons () 
	{
		availableUnits.AssetPath = "/resources/cards/equipment";
		availableUnits.cardType = "Weapon";
	}

	public void showAvailableArmor () 
	{
		availableUnits.AssetPath = "/resources/cards/armor";
		availableUnits.cardType = "Armor";
	}

	public void showAvailableBuildings () 
	{
		availableUnits.AssetPath = "/resources/cards/defenses";
		availableUnits.cardType = "Building";
	}
}
