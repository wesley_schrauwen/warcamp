﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class informationCanvasNavigator : MonoBehaviour {

    public Button equipmentButton;
    public Button statsButton;
    public Button bioButton;

    public GameObject equipmentPanel;
    public GameObject statsPanel;
    public GameObject bioPanel;

    private GameObject currentCanvas;

    void Start() {
        equipmentButton.onClick.AddListener(() => {
            displayCanvas(equipmentPanel);
        });
        statsButton.onClick.AddListener(() => {
            displayCanvas(statsPanel);
        });
        bioButton.onClick.AddListener(()=> {
            displayCanvas(bioPanel);
        });
    }

    private void displayCanvas(GameObject newCanvas) {
        currentCanvas.SetActive(false);
        currentCanvas = newCanvas;
        currentCanvas.SetActive(true);
    }

    public void hideEquipmentElements(bool shouldHide) {
        equipmentButton.gameObject.SetActive(shouldHide);
        equipmentPanel.SetActive(shouldHide);
    }

}
