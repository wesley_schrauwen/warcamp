﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class upgradeTickerPanel : MonoBehaviour
{

	public Text upgradeTimeText;
	public GameObject upgradeDial;

	private ulong upgradeBegin;
	private ulong upgradeComplete;

	private float timeToUpgrade;

	private bool shouldSpinDial = false;

	private capitalCanvasManager sceneManager;
	private int hoursInMS = 60 * 60 * 1000;

	void Start ()
	{
		sceneManager = GameObject.Find ("Canvas").GetComponent<capitalCanvasManager> ();
	}

	void Update ()
	{

		if (!shouldSpinDial) {
			return;
		}

		upgradeDial.GetComponent<RectTransform> ().Rotate (0, 0, 100 * Time.deltaTime);

		timeToUpgrade -= (Time.deltaTime * 1000);

		upgradeTimeText.text = readableTimeToUpgrade (timeToUpgrade);

	}

	public void startTicking (ulong begin, ulong complete)
	{
		upgradeBegin = begin;
		upgradeComplete = complete;
		shouldSpinDial = true;
		timeToUpgrade = complete - (ulong)Date.now;
		upgradeTimeText.text = readableTimeToUpgrade (timeToUpgrade);
	}

	public void stopTicking ()
	{
		shouldSpinDial = false;
	}

	private string readableTimeToUpgrade (float time)
	{

		if (time <= 0) {

			sceneManager.reloadUpgradeUI ();
			return "Upgrade Complete";

		}

		string response = "";

		int hours = Mathf.FloorToInt (time / (hoursInMS));
		int minutes = Mathf.FloorToInt ((time - (hours * hoursInMS)) / (60 * 1000));

		if (hours > 24) {

			response += (int)(hours / 24) + " Days";
			
		} else {

			if (hours != 0) {
				response += hours + " Hours ";
			}

			if (minutes != 0) {
				response += minutes + " Minutes";
			}

			if (hours == 0) {
				response += Mathf.FloorToInt (time * 0.001f) + " Seconds";
			}
		}
			
		print (response);

		return response;
	}

	void OnDisable ()
	{
		stopTicking ();
	}

}
