﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class unitSelectorManager : MonoBehaviour
{

	public GameObject prefab;

	public GameObject scrollView;
	public Text containerTitle;

	private GameObject[] scrollViewList;

	private bool isDisplayingUnits = false;

	public void updateItems ()
	{
		if (isDisplayingUnits) {
			cleanUp ();
			displayUnits ();
		} else {
			cleanUp ();
			displayBuildings ();
		}
	}

	public void displayUnits ()
	{
	
		containerTitle.text = "UNITS";
		var allUnits = profileManager.getInstance ().getAllUnits ();
		var allClientUnits = profileManager.getInstance ().getLocalPlayer ().getUnits ();

		isDisplayingUnits = true;

		displayItems (allUnits, allClientUnits);
	}

	private void displayItems (GameObject[] allItems, List<GameObject> allClientItems)
	{
		scrollViewList = new GameObject[allItems.Length];

		resizeScrollView (allItems.Length, 24);

		for (int i = 0; i < allClientItems.Count; i++) {

			var item = (GameObject)Instantiate (prefab);

			item.GetComponent<unitSelectorPrefab> ().setupPrefab (allClientItems [i].GetComponent<unit> ());

			item.transform.position = new Vector3 ((prefab.GetComponent<RectTransform> ().sizeDelta.x + 24) * i, 0, 0);

			item.transform.SetParent (scrollView.transform, false);

			scrollViewList [i] = item;

		}
	}

	public void displayBuildings ()
	{

		containerTitle.text = "DEFENSES";

		var allBuildings = profileManager.getInstance ().getAllDefenses ();
		var allClientBuildings = profileManager.getInstance ().getLocalPlayer ().getStructures ();

		isDisplayingUnits = false;

		displayItems (allBuildings, allClientBuildings);
	}

	private void resizeScrollView (int itemCount, int offset)
	{

		scrollView.GetComponent<RectTransform> ().sizeDelta = new Vector2 (
			prefab.GetComponent<RectTransform> ().sizeDelta.x * itemCount + (offset * itemCount), 
			scrollView.GetComponent<RectTransform> ().sizeDelta.y);

	}

	void OnDisable ()
	{
	

		cleanUp ();
	}

	private void cleanUp ()
	{
		foreach (GameObject item in scrollViewList) {
			Destroy (item);
		}


		scrollViewList = null;
	}
}
