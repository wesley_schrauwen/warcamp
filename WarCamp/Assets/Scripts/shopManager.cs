﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class shopManager : MonoBehaviour
{

	public Text goldText;
	public Text lumberText;
	public Text gemText;
	public Text totalText;

	public Button checkout;

	private shopSelectorButton currentGoldSelector = null;
	private shopSelectorButton currentLumberSelector = null;
	private shopSelectorButton currentGemSelector = null;

	public void updateReceipt ()
	{
	
		goldText.text = currentGoldSelector.getDisplayAmount ();
		lumberText.text = currentLumberSelector.getDisplayAmount ();
		gemText.text = currentGemSelector.getDisplayAmount ();

		totalText.text = "$ " + (int.Parse (goldText.text) + int.Parse (lumberText.text) + int.Parse (gemText.text)).ToString ();

	}

	public void setGoldSelector (shopSelectorButton reference)
	{
	
		if (currentGoldSelector != null) {
			currentGoldSelector.isNotSelected ();
		}

		currentGoldSelector = reference;
		currentGoldSelector.isSelected ();
		updateReceipt ();
	}

	public void setLumberSelector (shopSelectorButton reference)
	{
		if (currentLumberSelector != null) {
			currentLumberSelector.isNotSelected ();
		}

		currentLumberSelector = reference;
		currentLumberSelector.isSelected ();
		updateReceipt ();
	}

	public void setGemSelector (shopSelectorButton reference)
	{
		if (currentGemSelector != null) {
			currentGemSelector.isNotSelected ();
		}

		currentGemSelector = reference;
		currentGemSelector.isSelected ();
		updateReceipt ();
	}
}
