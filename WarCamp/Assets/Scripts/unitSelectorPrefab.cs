﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class unitSelectorPrefab : MonoBehaviour
{
	public RawImage rank;
	public Text unitName;

	public Text gold;
	public Text lumber;

	public RawImage weapon;
	public RawImage armour;

	public RawImage card;

	private unit _unit;
	private GameObject rootSceneManager;

	void Start ()
	{
		rootSceneManager = GameObject.Find ("Canvas");
	}

	public void setupPrefab (unit _unit)
	{
		

		unitName.text = _unit.getUnitName () + " (" + _unit.getUnitLevel () + ")";
		gold.text = _unit.getGoldCost ().ToString ();
		lumber.text = _unit.getLumberCost ().ToString ();
		card.texture = _unit.card;

		this._unit = _unit;

		gameObject.GetComponent<Button> ().onClick.AddListener (() => {

			rootSceneManager.GetComponent<capitalCanvasManager> ().displayUnitEditor (this._unit);
		
		});
	}

	void OnDestroy ()
	{
		gameObject.GetComponent<Button> ().onClick.RemoveAllListeners ();
	}
}
