﻿using UnityEngine;
using UnityEngine.UI;
using System.IO;
using System.Collections;
using System.Collections.Generic;

public class availableUnits : MonoBehaviour
{
	public static string AssetPath = "";
	public static string cardType = "";

    public GameObject parentPanel;

	public RawImage prefabImage;
	public Text prefabText;
	public Text prefabTypeText;

    public Texture2D[] imageList;
    public string[] files;
   
	public int dummy;

	private int pathLength;

    void OnEnable ()
	{
		print (files.Length);
		// This is to set the path for a selected type of cards
		string path = (Application.dataPath +""+ AssetPath);
		pathLength = path.Length + 1;

		if (files.Length == 0 ) {
			int dummy = 0;
			// This is to get the files and start the load prosess
			files = Directory.GetFiles(path, "*.jpg");
			StartCoroutine(LoadImages());
		}
		print (cardType);
    }

    private IEnumerator LoadImages()
    {
        imageList = new Texture2D[files.Length];
        
		// This gets a textures for every file found at that path
        foreach (string file in files)
        {
			string pathTemp = @"file://" + file;
			//This loads the textures into physical memory to manipulate "use"
            WWW www = new WWW(pathTemp);
			//This allows the textures to load before being manipulated to prevent null errors
            yield return www;
			//Changes texture size and format and loads textures to array
            Texture2D texTmp = new Texture2D(1024, 1024, TextureFormat.DXT1, false);
            www.LoadImageIntoTexture(texTmp);
			//Indexes array of textures
			imageList[dummy] = texTmp;

			prefabText.text = file.Substring(pathLength,(file.Length - (pathLength + 4)));
			prefabTypeText.text = cardType;

			//Creates Images to add the textures to
			var ImageClone = Instantiate(prefabImage);
			//Says where the image should be created
            ImageClone.transform.SetParent(parentPanel.transform);
			ImageClone.transform.localScale = new Vector3(1F, 1F, 1F);
			//Adding the indevidual textures to the individually created images
		
			ImageClone.texture = imageList[dummy];
			
            dummy++;
        }
    }
}
