﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class sliderSelected : MonoBehaviour {

	public Slider slider;
	public Button sliderButton;

	public bool showButtons;
	public GameObject buttonsPanel;

	private bool showSlider;

	// Use this for initialization
	void Start () 
	{
		slider.value = 0;
		sliderButton.onClick.AddListener(()=> { StartSlider(); });
	}

	// this is used to toggel the slider load motions
	void StartSlider () {
		if (!showSlider) {
			showSlider = true;
		} else {
			showSlider = false;
		}
	}

	// this is used to load over time to make a fluent motion
	void Update ()
	{
		if (showSlider) {
			slider.value += (Time.deltaTime * 1f);
		}
		if (!showSlider) {
			slider.value -= (Time.deltaTime * 1f);
		}
		if (showButtons) {
			buttonsPanel.SetActive (true);
		} 
		if (!showButtons){
			buttonsPanel.SetActive (false);
		}
		if (slider.value == 1) {
			showButtons = true;
		}
		if (slider.value != 1) {
			showButtons = false;
		}
	}
}
