﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class internalPanelNavigation : MonoBehaviour {

		public Button weapons;
		public Button armor;
		public Button skins;
		public Button bio;

		private capitalCanvasManager capitalCanvasRefrence;

		public GameObject weaponsPlabe;
		public GameObject armorPlabe;
		public GameObject skinsPlabe;
		public GameObject bioPlane;

		void Start () 
		{
			capitalCanvasRefrence = GameObject.Find("capitalCanvas").GetComponent<capitalCanvasManager>();

			weapons.onClick.AddListener(() => {
				closeAll();
				capitalCanvasRefrence.pushCanvas(weaponsPlabe);
			});
			armor.onClick.AddListener(() => {
				closeAll();
				capitalCanvasRefrence.pushCanvas(armorPlabe);
			});
			skins.onClick.AddListener(() => {
				closeAll();
				capitalCanvasRefrence.pushCanvas(skinsPlabe);
			});
			bio.onClick.AddListener(() => {
				closeAll();
				capitalCanvasRefrence.pushCanvas(bioPlane);
			});
		}

	public void closeAll()
	{
		weaponsPlabe.SetActive (false);
		armorPlabe.SetActive (false);
		skinsPlabe.SetActive (false);
		bioPlane.SetActive (false);
	}
}
