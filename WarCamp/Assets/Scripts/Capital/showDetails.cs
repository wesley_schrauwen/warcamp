﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class showDetails : MonoBehaviour {

	public RawImage cardImage;
	public Text cardName;
	public Text cardType;

	private GameObject unitPanel;
	private GameObject weaponPanel;
	private GameObject armorPanel;
	private GameObject buildingPanel;
	private GameObject middelPanel;

	void Awake()
	{
		middelPanel = GameObject.Find ("MiddelCanvas");
		unitPanel = middelPanel.transform.Find("unitPanel/unitDetailsPanel").gameObject;
		weaponPanel = middelPanel.transform.Find("weaponsPanel/weaponDetailsPanel").gameObject;
		armorPanel = middelPanel.transform.Find("armorPanel/armorDetailsPanel").gameObject;
		buildingPanel = middelPanel.transform.Find("buildingsPanel/buildingDetailsPanel").gameObject;
	}

	public void OnClick ()
	{
		if (cardType.text == "Unit") {
			print (cardType.text);
			unitPanel.GetComponent<unitDetails> ().unitImage.texture = cardImage.texture;
			unitPanel.GetComponent<unitDetails> ().unitName.text = cardName.text;
			unitPanel.SetActive (true);
		}
		if (cardType.text == "Weapon") {
			print (cardName.text);
			weaponPanel.GetComponent<weaponDetails> ().weaponImage.texture = cardImage.texture;
			weaponPanel.GetComponent<weaponDetails> ().weaponName.text = cardName.text;
			weaponPanel.SetActive (true);
		}
		if (cardType.text == "Armor") {
			armorPanel.GetComponent<armorDetails> ().armorImage.texture = cardImage.texture;
			armorPanel.GetComponent<armorDetails> ().armorName.text = cardName.text;
			armorPanel.SetActive (true);
		}
		if (cardType.text == "Building") {
			buildingPanel.GetComponent<buildingDetails> ().cardImage.texture = cardImage.texture;
			buildingPanel.GetComponent<buildingDetails> ().cardName.text = cardName.text;
			buildingPanel.SetActive (true);
		}
	}
}
