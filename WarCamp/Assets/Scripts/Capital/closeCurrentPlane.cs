﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class closeCurrentPlane : MonoBehaviour {

	public Button closeButton;
	//public GameObject CurrentPanel;

	// Use this for initialization
	void OnEnable () 
	{
		closeButton.onClick.AddListener(()=> { closePlane(); });
	}

	void closePlane ()
	{
		this.gameObject.SetActive (false);
	}
}
