﻿using UnityEngine;
using UnityEngine.UI;
using System.IO;
using System.Collections;
using System.Collections.Generic;

public class unitDetails : MonoBehaviour {

	public RawImage unitImage;
	public Text unitName;
	public string cardType;

	public Text damage;
	public Text health;
	public Text movementSpeed;
	public Text attackSpeed;
	public Text range;
	public Text hitLimit;
	public Text areaOfAffectRadius;

	public Text goldCost;
	public Text lumberCost;

	public int amountOfMonsters;
	private GameObject monster;

	private string previousPrefab;

	public GameObject parentPanel;

	void OnEnable ()
	{
		if (previousPrefab == "") 
		{
			createPrefab ();
		}
		else 
		{
			destroyPreviousPrefab ();
			print (previousPrefab);
			createPrefab ();
		}
	}

	public void createPrefab() 
	{
		monster = (GameObject)Instantiate (Resources.Load ("units/" + unitName.text + ""));
		monster.transform.SetParent (parentPanel.transform);
		monster.transform.position = parentPanel.transform.position;

		previousPrefab = unitName.text;

		damage.text = monster.GetComponent<unit> ().creepStats.damage.ToString();
		health.text = monster.GetComponent<unit> ().creepStats.hp.ToString();
		movementSpeed.text = monster.GetComponent<unit> ().creepStats.moveSpeed.ToString();
		attackSpeed.text = monster.GetComponent<unit> ().creepStats.attackSpeed.ToString();
		range.text = monster.GetComponent<unit> ().creepStats.range.ToString();
		//hitLimit.text = monster.GetComponent<unit> ().creepStats.hitLimit.ToString();
		//areaOfAffectRadius.text = monster.GetComponent<unit> ().creepStats.areaOfAttackRadius.ToString();

		goldCost.text = monster.GetComponent<unit> ().creepStats.goldCost.ToString();
		lumberCost.text = monster.GetComponent<unit> ().creepStats.lumberCost.ToString();

		print (damage.text);
	}

	public void destroyPreviousPrefab()
	{
		monster = GameObject.Find (previousPrefab + "(Clone)");
		Destroy (monster);

//		damage;
//		health;
//		movementSpeed;
//		attackSpeed;
//		range;
//		hitLimit;
//		areaOfAffectRadius;
//
//		goldCost;
//		lumberCost;
	}
}



