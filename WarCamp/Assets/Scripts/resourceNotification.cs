﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class resourceNotification : MonoBehaviour
{

	public Text resourceGained;

	private float timeToExpire = 2.5f;

	public void setGain (int resource)
	{
		resourceGained.text = (resource <= 0 ? "" : "+ " + resource).ToString ();
	}

	public float getTimeToExpire ()
	{
		return timeToExpire;
	}

	public void decreaseTimeToExpire (float t)
	{
		timeToExpire -= t;
	}

	public void setTimeToExpire (float t)
	{
		timeToExpire = t;
	}
}
