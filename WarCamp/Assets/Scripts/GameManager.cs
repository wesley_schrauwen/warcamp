﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using GameSparks.RT;
using System.Collections.Generic;
using GameSparks.Core;

public class GameManager : MonoBehaviour
{
	private static GameManager instance = null;
	private capitalCanvasManager capitalCanvasRef = null;
	// For SilentLogin
	public static string deviceDisplayName = "superman";

	// The first response received from match making
	private GameSparks.Api.Messages.MatchFoundMessage RTResponse;
	// The current match session in use
	private GameSparksRTUnity RTSession = null;
	private GameSparks.Core.GSData tempPlayerProfile = null;
	// The local players ID. Retrieved on authentication.
	private string localPlayerID;
	private int clientPeerID;
	private matchCoordinator matchInstance = null;

	private int estimatedPing;

	private long timeStampReceived;
	private long timeStampReturned;

	private enum sceneNames
	{
		capitalScene,
		loginScene,
		matchScene
	}

	// Use this for initialization
	void Start ()
	{

		if (instance == null) {
			instance = this;
			DontDestroyOnLoad (this.gameObject);
		} else {
			Destroy (gameObject);
		}
	}


	// TODO: convert this function to a closure / callback type function so that the response can be handled
	// in the calling function
	public void attemptAuth (string password, string emailAddress)
	{
		new GameSparks.Api.Requests.AuthenticationRequest ()
			.SetPassword (password)
			.SetUserName (emailAddress)
			.Send ((response) => {

			if (response.HasErrors) {

				// TODO: send back some error message to the user
				print (response.Errors.JSON.ToString ());

			} else {
          
				//deviceDisplayName = response.DisplayName;
				localPlayerID = response.UserId;
				downloadPlayerProfile ();
			}
		});
	}

	public void createAccount (string displayName, string email, string password)
	{
		new GameSparks.Api.Requests.RegistrationRequest ()
            .SetUserName (email)
            .SetDisplayName (displayName)
            .SetPassword (password)
            .Send ((response) => {
			 
			if (response.HasErrors) {

				GameObject.Find ("LoginMainSceneCanvas").GetComponent<loginSceneManager> ().returnToLoginPanel ();
				GameObject.Find ("successPanel").GetComponent<successPanelManager> ().unknownError ();

			} else {

				loadScene (sceneNames.capitalScene);

			}

		});
	}

	#region match making

	public void beginMatchMaking (capitalCanvasManager canvasReference)
	{
		if (capitalCanvasRef == null) {
			capitalCanvasRef = canvasReference;
		}


		new GameSparks.Api.Requests.LogEventRequest_getMatch ().Send ((response) => {

			if (response.HasErrors) {
				print (response.Errors.JSON.ToString ());
			}

		});

//		new GameSparks.Api.Requests.MatchmakingRequest ().SetMatchShortCode ("oneVone").SetSkill (0).Send ((response) => {
//
//			if (response.HasErrors) {
//				print (response.Errors.JSON.ToString ());
//			}
//
//		});
			
		GameSparks.Api.Messages.MatchNotFoundMessage.Listener += matchNotFound;
		GameSparks.Api.Messages.MatchFoundMessage.Listener += matchFound;
	}

	/// <summary>
	/// Performs any background tasks before sending message onto the alert
	/// </summary>
	/// <param name="message">The session that has been found.</param>
	public void matchFound (GameSparks.Api.Messages.MatchFoundMessage message)
	{
		// prevents the user from connecting to an old session
		if (RTSession != null) {
			RTSession = null;
		}

		setRTInitResponse (message);
		RTSession = this.gameObject.AddComponent<GameSparksRTUnity> ();
		RTSession.Configure (getRTInitResponse (), onPlayerConnect, onPlayerDisconnect, onReady, onPacketReceived);
		RTSession.Connect ();



		foreach (GameSparks.Api.Messages.MatchFoundMessage._Participant player in message.Participants) {

			profileManager.getInstance ().addPeer (player);

			if (player.Id.Equals (localPlayerID)) { // check if this is the local player
				clientPeerID = (int)player.PeerId;
			}

		}

		capitalCanvasRef.matchFoundAlert (message);

	}

	/// <summary>
	/// Performs any background tasks on the event that a match could not be found
	/// </summary>
	/// <param name="message">Details related to the failed session.</param>
	public void matchNotFound (GameSparks.Api.Messages.MatchNotFoundMessage message)
	{
		capitalCanvasRef.matchNotFoundAlert (message);
	}

	/// <summary>
	/// Sends a 101 packet to the server informing it that the player is ready.
	/// </summary>
	public void connectToMatch ()
	{
		using (RTData data = RTData.Get ()) {

			data.SetLong (1, Date.now);
			RTSession.SendData (101, GameSparksRT.DeliveryIntent.RELIABLE, data);


		}
			
	}

	public void disconnectFromMatch ()
	{

		if (RTSession == null || RTResponse == null) {
			return;
		}
			
		new GameSparks.Api.Requests.LogEventRequest_matchDisconnect ().Set_matchID (RTResponse.MatchId).Send ((response) => {

			if (response.HasErrors) {
				
			}

		});

		resetMatchVariables ();

		loadScene (sceneNames.capitalScene);


	}

	public void finishMatch ()
	{
		resetMatchVariables ();
		loadScene (sceneNames.capitalScene);
	}

	private void resetMatchVariables ()
	{
		RTSession.Disconnect ();
		RTSession = null;
		RTResponse = null;
		matchInstance = null;
	}

	#endregion

	#region real-time session

	#region match setup

	public GameSparks.Api.Messages.MatchFoundMessage getRTInitResponse ()
	{
		return RTResponse;
	}

	public void setRTInitResponse (GameSparks.Api.Messages.MatchFoundMessage newResponse)
	{
		RTResponse = newResponse;
	}

	private void onPlayerConnect (int peerID)
	{
		
	}

	private void onPlayerDisconnect (int peerID)
	{
		
	}

	private void onReady (bool isReady)
	{
		
	}

	#endregion

	private void updateLatency ()
	{
	
		using (RTData emptyPacket = RTData.Get ()) {

			RTSession.SendData (100, GameSparksRT.DeliveryIntent.UNRELIABLE, emptyPacket);

		}
		;

	}

	public void draftUnit (string unitCode)
	{

		using (RTData unitData = RTData.Get ()) {

			print ("drafting a: " + unitCode);
			unitData.SetString (1, unitCode);
			RTSession.SendData (102, GameSparksRT.DeliveryIntent.RELIABLE, unitData);

		}
		;
	}

	public void removeUnitDraft (string unitCode)
	{

		using (RTData unitData = RTData.Get ()) {
		
			unitData.SetString (1, unitCode);
			RTSession.SendData (103, GameSparksRT.DeliveryIntent.RELIABLE, unitData);

		}
		;

	}

	public void buildStructure (int buildSite, string structureCode)
	{

		using (RTData structureData = RTData.Get ()) {

			print ("packet sent");
			structureData.SetString (1, structureCode);
			structureData.SetInt (2, buildSite);

			RTSession.SendData (106, GameSparksRT.DeliveryIntent.RELIABLE, structureData);
		}
		;
	}

	/// <summary>
	/// USED IN MATCH ONLY
	/// Call this function to change the equipment for a given unit
	/// </summary>
	public void changeUnitEquipmentRT (string unitCode, string weaponCode, string armourCode)
	{

		using (RTData requestData = RTData.Get ()) {

			requestData.SetString (1, unitCode);
			requestData.SetString (2, weaponCode);
			requestData.SetString (3, armourCode);

			Debug.Log ("sending packet 108");
			Debug.Log ("unitCode: " + unitCode);
			Debug.Log ("weaponCode: " + weaponCode);
			Debug.Log ("armourCode: " + armourCode);
			RTSession.SendData (108, GameSparksRT.DeliveryIntent.RELIABLE, requestData);

		}
		;

	}

	/// <summary>
	/// Locks in the players current draft.
	/// </summary>
	public void lockInDraft ()
	{
		using (RTData lockInData = RTData.Get ()) {

			RTSession.SendData (104, GameSparksRT.DeliveryIntent.RELIABLE, lockInData);

		}
		;
	}

	public void cancelStructure(int buildSite){

		using(RTData packetData = RTData.Get()){

			packetData.SetInt (1, buildSite);

			RTSession.SendData (107, GameSparksRT.DeliveryIntent.RELIABLE, packetData);

		};

	}

	/// <summary>
	/// Called at the start of a match. Sets the instance for packet routing.
	/// </summary>
	/// <param name="instance">Match Coordinator Reference</param>
	public void setMatchCoordinator (matchCoordinator instance)
	{
		this.matchInstance = instance;
	}

	public float getEstimatedPing ()
	{
		return estimatedPing;
	}

	private void onPacketReceived (RTPacket packet)
	{

		switch (packet.OpCode) {
		case 10:

			matchInstance.gameOver ((int)packet.Data.GetInt (1), (int)packet.Data.GetInt (2), (int)packet.Data.GetInt (3));
			break;

		case 90:
			
			loadScene (sceneNames.matchScene);

			break;
		case 91:

//			print ("packet 91 received");
			matchInstance.startSpawnTimer ((int)packet.Data.GetInt (1) - estimatedPing);


			break;

		case 92:

//			print ("packet 92 received");
			estimatedPing = (int)(Date.now - (long)packet.Data.GetLong (1));
//			print ("estimated ping: " + (Date.now - (long)packet.Data.GetLong (1)));
			break;

		case 99:

			updateLatency ();
			timeStampReceived = (long)packet.Data.GetLong (1);
			break;

		case 100:

			timeStampReturned = (long)packet.Data.GetLong (1);
			estimatedPing = (int)(timeStampReturned - timeStampReceived);

			break;

		case 101:         
			
			print (packet.Sender + " has ticked ready");

			break;
		case 200:
			

			break;
		case 201:
			

			for (uint i = 1; i <= 10; i++) {

				if (packet.Data.GetString (i) != null) {

					matchInstance.manageUnitPacket (packet.Data.GetString (i).Split (':'));

				} else {
					break;
				}
					
			}
				
			break;

		case 202: 
			
			matchInstance.increaseLocalDraft (packet.Data.GetString (1));

			break;
		case 203:
            
			matchInstance.updateUnknownDraft ((int)packet.Data.GetInt (2));

			break;
		case 204:
                
			matchInstance.displayLockIn ((int)packet.Data.GetInt (1), (int)packet.Data.GetInt (2));

			break;

		case 206:

			matchInstance.removeUnit ((int)packet.Data.GetInt (2));
			matchInstance.setHasStructure ((int)packet.Data.GetInt (3), false);
			matchInstance.removeSiteIcon ((int)packet.Data.GetInt(3));

			break;

		case 207:

			int[] unitIDS = new int[(int)packet.Data.GetInt (1)];

			for (int i = 0; i < (int)packet.Data.GetInt (1); i++) {

				unitIDS [i] = (int)packet.Data.GetInt ((uint)i + 2);

			}

			matchInstance.removeUnits (unitIDS);

			break;

		case 208:

			matchInstance.resetDrafts ();

			break;

		case 209:

			matchInstance.assignBuildSite ((int)packet.Data.GetInt (1), (int)packet.Data.GetInt (2));

			break;

		case 210: 

			matchInstance.setResources ((int)packet.Data.GetInt (1), (int)packet.Data.GetInt (2));

			break;

		case 211:

			matchInstance.updatePeerDraft (packet);

			break;

		case 212:

			matchInstance.structureComplete ((int)packet.Data.GetInt (1), (int)packet.Data.GetInt (2), packet.Data.GetString (3));

			break;

		case 303: 
			
			matchInstance.decreaseLocalDraft (packet.Data.GetString (1));

			break;
		case 308:
			
			matchInstance.responseUnitEquipmentChange (packet.Data.GetString (1), 
				packet.Data.GetString (2),
				packet.Data.GetString (3),
				(int)packet.Data.GetInt (4), 
				(int)packet.Data.GetInt (5));
			
			break;

		default:

			Debug.Log ("unknown packet received " +
			"\nopCode: " + packet.OpCode +
			"\ncontent" + packet.Data.ToString ());
			break;
		}
	}

	#endregion

	#region sceneManagement

	private void loadScene (sceneNames scene)
	{
		string sceneName = "LoginScene";

		switch (scene) {
		case sceneNames.capitalScene:
			sceneName = "CapitalScene";
			break;
		case sceneNames.loginScene:
			sceneName = "LoginScene";
			break;
		case sceneNames.matchScene:
			sceneName = "MatchScene";
			break;
		}
		SceneManager.LoadScene (sceneName);
	}

	#endregion

	public int getClientPeerID ()
	{
		return clientPeerID;
	}

	public void downloadPlayerProfile ()
	{
		new GameSparks.Api.Requests.LogEventRequest_getProfile ().Send ((profileIN) => {

			tempPlayerProfile = profileIN.ScriptData;
			loadScene (sceneNames.capitalScene);

		});

	}

	public GameSparks.Core.GSData getTempProfile ()
	{
		return tempPlayerProfile;
	}

	/// <summary>
	/// Returns the Game Manager singleton.
	/// </summary>
	/// <returns>The instance.</returns>
	public static GameManager getInstance ()
	{
		return instance;
	}

	public void upgradeItemRequest (string group, string name)
	{

		new GameSparks.Api.Requests.LogEventRequest_upgrade ()
            .Set_type (group)
            .Set_item (name)
            .Send ((response) => {

			if (response.HasErrors) {
				print (response.Errors);
			} else {
				GameObject.Find ("Canvas").GetComponent<capitalCanvasManager> ().upgradeResponse (
					(bool)response.ScriptData.GetBoolean ("success"),
					(int)response.ScriptData.GetInt ("gold"),
					(int)response.ScriptData.GetInt ("lumber"),
					(long)response.ScriptData.GetLong ("upgrade_complete"),
					(long)response.ScriptData.GetLong ("upgrade_begin"),
					response.ScriptData.GetString ("subgroup"),
					response.ScriptData.GetString ("item"));
			}

		});

	}

	public void changeUnitEquipmentRequest (string type, string item, unit _unit)
	{

		new GameSparks.Api.Requests.LogEventRequest_changeEquipment ()
			.Set_item (item)
			.Set_type (type)
			.Set_unit (_unit.getUnitName ())
			.Send ((response) => {
						
			if (response.HasErrors) {
					
				Debug.Log (response.Errors);

			} else {

				_unit.setEquipment (type, item);
				GameObject.Find ("Canvas").GetComponent<capitalCanvasManager> ().updateUnitEditor (_unit);

			}			

		});

	}

	public void resetPassword (string email)
	{

		// this should be called when successfully reset password.
		GameObject loginSceneManagerReference = GameObject.Find ("LoginMainSceneCanvas");
		loginSceneManagerReference.GetComponent<loginSceneManager> ().returnToLoginPanel ();
		loginSceneManagerReference.GetComponent<loginSceneManager> ().displaySuccessPanel ();

		GameObject.Find ("successPanel").GetComponent<successPanelManager> ().passwordResetText ();

	}

	public void logout ()
	{
		loadScene (sceneNames.loginScene);
		tempPlayerProfile = null;
	}
}