﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class unitEditorManager : MonoBehaviour
{
	[Header ("Headline Info")]
	public Text unitNameText;
	public Text unitLevelText;
	public Text unitDescriptionText;
	public RawImage rank;
	public RawImage card;

	[Header ("Detailed Info")]
	public Text goldText;
	public Text lumberText;
	public Text hpText;
	public Text damageText;
	public Text moveSpeedText;
	public Text attackSpeedText;
	public Text rangeText;

	[Header ("Loadout")]
	public Text selectedWeaponText;
	public Text selectedArmourText;
	public RawImage selectedWeaponRawImage;
	public RawImage selectedArmourRawImage;

	[Header ("Buttons")]
	public Button upgradeButton;
	public Button selectWeaponButton;
	public Button selectArmourButton;

	private capitalCanvasManager sceneManagerReference;
	private unit _unit;

	void Start ()
	{
		sceneManagerReference = GameObject.Find ("Canvas").GetComponent<capitalCanvasManager> ();
	}

	public void setupUnitEditor (unit _unit)
	{
		updateStats (_unit);

		upgradeButton.onClick.AddListener (() => {

			sceneManagerReference.displayUpgradeCanvas (this._unit);

		});

		selectWeaponButton.onClick.AddListener (() => {

			sceneManagerReference.displayItemSelector (this._unit, "weapon");

		});

		selectArmourButton.onClick.AddListener (() => {

			sceneManagerReference.displayItemSelector (this._unit, "armour");

		});

	}

	public void updateStats (unit _unit)
	{

		unitNameText.text = _unit.getUnitName ();
		unitLevelText.text = "LEVEL " + _unit.getUnitLevel ().ToString ();
		unitDescriptionText.text = _unit.getUnitDescription ();
		card.texture = _unit.getCard ();

		goldText.text = _unit.creepStats.goldCost.ToString ();
		lumberText.text = _unit.creepStats.lumberCost.ToString ();
		hpText.text = _unit.creepStats.hp.ToString ();
		damageText.text = _unit.creepStats.damage.ToString ();
		moveSpeedText.text = _unit.creepStats.moveSpeed.ToString ();
		attackSpeedText.text = _unit.creepStats.attackSpeed.ToString ();
		rangeText.text = _unit.creepStats.range.ToString ();

		var weapon = _unit.getSelectedWeapon ();
		var armour = _unit.getSelectedArmour ();

		if (weapon != null) {
			selectedWeaponText.text = weapon.GetComponent<equipment> ().equipmentName;
			selectedWeaponRawImage.texture = weapon.GetComponent<equipment> ().card;
		} else {
			selectWeaponButton.gameObject.SetActive (false);
		}

		if (armour != null) {
			selectedArmourText.text = armour.GetComponent<equipment> ().equipmentName;
			selectedArmourRawImage.texture = armour.GetComponent<equipment> ().card;
		} else {
			selectArmourButton.gameObject.SetActive (false);
		}


		this._unit = _unit;
	}

	public void reloadUI (string unitName)
	{

		if (_unit.getUnitName ().Equals (unitName)) {
			setupUnitEditor (_unit);
		}

	}

	void OnDisable ()
	{
		upgradeButton.onClick.RemoveAllListeners ();
		selectWeaponButton.onClick.RemoveAllListeners ();
		selectArmourButton.onClick.RemoveAllListeners ();

		selectWeaponButton.gameObject.SetActive (true);
		selectArmourButton.gameObject.SetActive (true);
	}

}
