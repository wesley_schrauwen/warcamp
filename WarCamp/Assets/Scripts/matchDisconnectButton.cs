﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class matchDisconnectButton : MonoBehaviour
{

	void Start ()
	{
		gameObject.GetComponent<Button> ().onClick.AddListener (() => {

			GameManager.getInstance ().disconnectFromMatch ();

		});
	}

	void OnDestroy ()
	{
		gameObject.GetComponent<Button> ().onClick.RemoveAllListeners ();
	}
}
