﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class equipmentListItemPrefab : MonoBehaviour
{

	public Text label;
	public GameObject selector;

	private Button listButton = null;
	private unitEquipmentManager parentReference;
	private bool isWeapon = false;
	private GameObject item;

	public void setupLabel (GameObject item, unitEquipmentManager reference, bool isWeapon)
	{

		if (listButton == null) {
			listButton = gameObject.GetComponent<Button> ();
		}
			
		this.item = item;
		label.text = item.GetComponent<equipment> ().equipmentName;
		parentReference = reference;
		this.isWeapon = isWeapon;

		listButton.onClick.AddListener (() => {

			if (this.isWeapon) {
				parentReference.setWeaponRequest (this.item, this);
			} else {
				parentReference.setArmourRequest (this.item, this);
			}

		});

	}

	public void setAsSelected ()
	{
		selector.SetActive (true);
		label.color = standardColors.selected;
		label.fontStyle = FontStyle.Bold;
	}

	public void setAsUnselected ()
	{
		selector.SetActive (false);
		label.color = standardColors.unselected;
		label.fontStyle = FontStyle.Normal;
	}

	void OnDestroy ()
	{
		listButton.onClick.RemoveAllListeners ();
	}

}
