﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class mapCanvasManager : MonoBehaviour
{

	public GameObject[] mapBuildSites;
	public GameObject[] physicalBuildSites;

	[Header ("Prefabs")]
	public GameObject towerPrefab;
	public GameObject trapPrefab;
	public GameObject fortPrefab;
	public GameObject goldMinePrefab;
	public GameObject lumberMillPrefab;

	void Start ()
	{
		
		foreach (GameObject site in mapBuildSites) {

			site.GetComponent<RawImage> ().color = Color.white;

		}

	}

	public void assignBuildSite (int peerID, int site)
	{
	
		mapBuildSites [site].GetComponent<RawImage> ().color = peerID == GameManager.getInstance ().getClientPeerID () ? standardColors.teal : standardColors.red;
		physicalBuildSites [site].transform.GetChild (1).gameObject.GetComponent<buildSitePrefab> ().assignOwner (peerID);

	}

	public void setHasStructure (int site, bool hasStructure)
	{
		physicalBuildSites [site].transform.GetChild (1).gameObject.GetComponent<buildSitePrefab> ().setHasStructure (hasStructure);
	}

	public void removeSiteIcon(int buildSite){

		if (mapBuildSites[buildSite].transform.childCount == 0){
			return;
		}

		GameObject child = mapBuildSites [buildSite].transform.GetChild (0).gameObject;
		Destroy (child);

	}

	public void setSiteIcon (int peerID, int site, string structureCode)
	{

		Color32 iconColor = peerID == GameManager.getInstance ().getClientPeerID () ? standardColors.teal : standardColors.red;

		switch (structureCode) {

		case "tower":
			
			GameObject tower = Instantiate (towerPrefab);
			tower.transform.SetParent (mapBuildSites [site].transform, false);
			tower.GetComponent<buildSiteIcon> ().setIconColor (iconColor);

			break;
		case "fort":

			GameObject fort = Instantiate (fortPrefab);
			fort.transform.SetParent (mapBuildSites [site].transform, false);
			fort.GetComponent<buildSiteIcon> ().setIconColor (iconColor);

			break;
		case "explosive_trap":

			GameObject trap = Instantiate (trapPrefab);
			trap.transform.SetParent (mapBuildSites [site].transform, false);
			trap.GetComponent<buildSiteIcon> ().setIconColor (iconColor);

			break;
		case "gold_mine":

			print ("setting gold icon");
			GameObject goldMine = Instantiate (goldMinePrefab);
			goldMine.transform.SetParent (mapBuildSites [site].transform, false);
			goldMine.GetComponent<buildSiteIcon> ().setIconColor (iconColor);

			break;
		case "lumber_mill":

			GameObject lumberMill = Instantiate (lumberMillPrefab);
			lumberMill.transform.SetParent (mapBuildSites [site].transform, false);
			lumberMill.GetComponent<buildSiteIcon> ().setIconColor (iconColor);

			break;

		default:
			break;

		}

	}

}
