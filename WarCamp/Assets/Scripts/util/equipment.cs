﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[System.Serializable]
public class upgradeable
{
	public bool isUpgrading;
	public ulong upgradeComplete;
	public ulong upgradeBegin;
	public int upgradeGoldCost;
	public int upgradeLumberCost;
	public ulong upgradeTime;
	public int upgradeGemCost;
}

[System.Serializable]
public class equipmentStats
{
	public int rangeCH = 0;
	public int goldCostCH = 0;
	public int lumberCostCH = 0;
	public int damageCH = 0;
	public int attackSpeedCH = 0;
	public int moveSpeedCH = 0;
	public int hpCH = 0;
}

public class equipment : MonoBehaviour
{
	public string equipmentName;
	public int level;
	public int maxLevel;
	public Texture2D card;
	[TextArea (minLines: 0, maxLines: 6)]
	public string itemDescription;
	public equipmentStats stats;
	public upgradeable upgrade;
	public equipmentType type;
	private Animator attackAnimation;

	void Start ()
	{
		//attackAnimation = GetComponent<Animator>();
	}

	public void fire ()
	{
        
	}

	public void setupEquipment (int level, int maxLevel, bool isUpgrading, ulong upgradeComplete, ulong upgradeBegin)
	{
		this.level = level;
		this.maxLevel = maxLevel;
		this.upgrade.isUpgrading = isUpgrading;
		this.upgrade.upgradeComplete = upgradeComplete;
		this.upgrade.upgradeBegin = upgradeBegin;
		this.upgrade.upgradeTime = upgradeComplete > upgradeBegin ? upgradeComplete - upgradeBegin : 0;

		setStats (level, stats, type);
		upgradeCost (level < maxLevel ? ++level : 0, upgrade, type);

	}

	public void setStats (int level, equipmentStats stats, equipmentType type)
	{

		switch (type) {
		case equipmentType.SWORD:

			stats.goldCostCH = 10 * level;
			stats.lumberCostCH = 10 * level;
			stats.damageCH = 10 * level;
			stats.attackSpeedCH = 5 * level;

			break; 
		case equipmentType.AXE:

			stats.goldCostCH = 10 * level;
			stats.lumberCostCH = 10 * level;
			stats.damageCH = 10 * level;
			stats.attackSpeedCH = 5 * level;

			break; 

		case equipmentType.SPEAR:

			stats.goldCostCH = 10 * level;
			stats.lumberCostCH = 10 * level;
			stats.damageCH = 10 * level;
			stats.attackSpeedCH = 5 * level;

			break; 
		case equipmentType.SHORT_BOW:

			stats.goldCostCH = 10 * level;
			stats.lumberCostCH = 10 * level;
			stats.damageCH = 10 * level;
			stats.attackSpeedCH = 5 * level;
			break; 
		case equipmentType.LONG_BOW:

			stats.goldCostCH = 10 * level;
			stats.lumberCostCH = 10 * level;
			stats.damageCH = 10 * level;
			stats.attackSpeedCH = 5 * level;

			break; 
		case equipmentType.CROSS_BOW:
			stats.goldCostCH = 10 * level;
			stats.lumberCostCH = 10 * level;
			stats.damageCH = 10 * level;
			stats.attackSpeedCH = 5 * level;

			break; 
		case equipmentType.FIRE_BOLT:
			stats.goldCostCH = 10 * level;
			stats.lumberCostCH = 10 * level;
			stats.damageCH = 10 * level;
			stats.attackSpeedCH = 5 * level;

			break; 
		case equipmentType.ICE_SHARD:
			stats.goldCostCH = 10 * level;
			stats.lumberCostCH = 10 * level;
			stats.damageCH = 10 * level;
			stats.attackSpeedCH = 5 * level;

			break; 
		case equipmentType.CHAIN_LIGHTNING:
			stats.goldCostCH = 10 * level;
			stats.lumberCostCH = 10 * level;
			stats.damageCH = 10 * level;
			stats.attackSpeedCH = 5 * level;

			break; 
		case equipmentType.SCIMITAR:
			stats.goldCostCH = 10 * level;
			stats.lumberCostCH = 10 * level;
			stats.damageCH = 10 * level;
			stats.attackSpeedCH = 5 * level;

			break; 
		case equipmentType.LANCE:
			stats.goldCostCH = 10 * level;
			stats.lumberCostCH = 10 * level;
			stats.damageCH = 10 * level;
			stats.attackSpeedCH = 5 * level;

			break; 
		case equipmentType.MACE:
			stats.goldCostCH = 10 * level;
			stats.lumberCostCH = 10 * level;
			stats.damageCH = 10 * level;
			stats.attackSpeedCH = 5 * level;
			break; 
		case equipmentType.REGULAR_SHOT:
			stats.goldCostCH = 10 * level;
			stats.lumberCostCH = 10 * level;
			stats.damageCH = 10 * level;
			stats.attackSpeedCH = 5 * level;

			break; 
		case equipmentType.MULTI_SHOT:
			stats.goldCostCH = 10 * level;
			stats.lumberCostCH = 10 * level;
			stats.damageCH = 10 * level;
			stats.attackSpeedCH = 5 * level;

			break; 
		case equipmentType.FLAME_SHOT:
			stats.goldCostCH = 10 * level;
			stats.lumberCostCH = 10 * level;
			stats.damageCH = 10 * level;
			stats.attackSpeedCH = 5 * level;

			break; 
		case equipmentType.LARGE_HEAL:
			stats.goldCostCH = 10 * level;
			stats.lumberCostCH = 10 * level;
			stats.damageCH = 10 * level;
			stats.attackSpeedCH = 5 * level;

			break; 
		case equipmentType.CHAIN_HEAL:
			stats.goldCostCH = 10 * level;
			stats.lumberCostCH = 10 * level;
			stats.damageCH = 10 * level;
			stats.attackSpeedCH = 5 * level;

			break; 
		case equipmentType.BURST_HEAL:
			stats.goldCostCH = 10 * level;
			stats.lumberCostCH = 10 * level;
			stats.damageCH = 10 * level;
			stats.attackSpeedCH = 5 * level;
			break; 
		case equipmentType.LIFE_STEAL:
			stats.goldCostCH = 10 * level;
			stats.lumberCostCH = 10 * level;
			stats.damageCH = 10 * level;
			stats.attackSpeedCH = 5 * level;

			break; 
		case equipmentType.CONVERT:
			stats.goldCostCH = 10 * level;
			stats.lumberCostCH = 10 * level;
			stats.damageCH = 10 * level;
			stats.attackSpeedCH = 5 * level;

			break; 
		case equipmentType.HEALTH_AURA:
			stats.goldCostCH = 10 * level;
			stats.lumberCostCH = 10 * level;
			stats.damageCH = 10 * level;
			stats.attackSpeedCH = 5 * level;

			break; 
		case equipmentType.SPEED_AURA:
			stats.goldCostCH = 10 * level;
			stats.lumberCostCH = 10 * level;
			stats.damageCH = 10 * level;
			stats.attackSpeedCH = 5 * level;

			break; 
		case equipmentType.DAMAGE_AURA:
			stats.goldCostCH = 10 * level;
			stats.lumberCostCH = 10 * level;
			stats.damageCH = 10 * level;
			stats.attackSpeedCH = 5 * level;

			break; 
		case equipmentType.THORNS_AURA:
			stats.goldCostCH = 10 * level;
			stats.lumberCostCH = 10 * level;
			stats.damageCH = 10 * level;
			stats.attackSpeedCH = 5 * level;
			break; 
		case equipmentType.LIGHT:
			stats.goldCostCH = 10 * level;
			stats.lumberCostCH = 10 * level;
			stats.damageCH = 10 * level;
			stats.attackSpeedCH = 5 * level;

			break; 
		case equipmentType.MEDIUM:
			stats.goldCostCH = 10 * level;
			stats.lumberCostCH = 10 * level;
			stats.damageCH = 10 * level;
			stats.attackSpeedCH = 5 * level;

			break;  
		case equipmentType.HEAVY:
			stats.goldCostCH = 10 * level;
			stats.lumberCostCH = 10 * level;
			stats.damageCH = 10 * level;
			stats.attackSpeedCH = 5 * level;

			break;
		case equipmentType.FORT_BASIC_ATTACK:
			stats.goldCostCH = 10 * level;
			stats.lumberCostCH = 10 * level;
			stats.damageCH = 10 * level;
			stats.attackSpeedCH = 5 * level;

			break;
		case equipmentType.TOWER_BASIC_ATTACK:
			stats.goldCostCH = 10 * level;
			stats.lumberCostCH = 10 * level;
			stats.damageCH = 10 * level;
			stats.attackSpeedCH = 5 * level;

			break;
		case equipmentType.TRAP_BASIC_ATTACK:
			stats.goldCostCH = 10 * level;
			stats.lumberCostCH = 10 * level;
			stats.damageCH = 10 * level;
			stats.attackSpeedCH = 5 * level;

			break;
		case equipmentType.FORTIFIED:
			stats.goldCostCH = 10 * level;
			stats.lumberCostCH = 10 * level;
			stats.damageCH = 10 * level;
			stats.attackSpeedCH = 5 * level;
			break;
		}

	}

	public Texture2D getCard ()
	{
		return card;
	}

	public equipmentType getType ()
	{
		return type;
	}

    public string getItemGroup() {

        switch (type) {
            case equipmentType.LIGHT:
                return "armour";
            case equipmentType.MEDIUM:
                return "armour";
            case equipmentType.HEAVY:
                return "armour";
            case equipmentType.FORTIFIED:
                return "armour";
            default:
                return "weapons";
        }

    }

	public void upgradeCost (int level, upgradeable stats, equipmentType type)
	{

		switch (type) {
		case equipmentType.SWORD:

			stats.upgradeGoldCost = 1000 * level;
			stats.upgradeLumberCost = 1000 * level;


			break; 
		case equipmentType.AXE:

			stats.upgradeGoldCost = 1000 * level;
			stats.upgradeLumberCost = 1000 * level;


			break; 

		case equipmentType.SPEAR:

			stats.upgradeGoldCost = 1000 * level;
			stats.upgradeLumberCost = 1000 * level;


			break; 
		case equipmentType.SHORT_BOW:

			stats.upgradeGoldCost = 1000 * level;
			stats.upgradeLumberCost = 1000 * level;

			break; 
		case equipmentType.LONG_BOW:

			stats.upgradeGoldCost = 1000 * level;
			stats.upgradeLumberCost = 1000 * level;

			break; 
		case equipmentType.CROSS_BOW:
			stats.upgradeGoldCost = 1000 * level;
			stats.upgradeLumberCost = 1000 * level;

			break; 
		case equipmentType.FIRE_BOLT:
			stats.upgradeGoldCost = 1000 * level;
			stats.upgradeLumberCost = 1000 * level;

			break; 
		case equipmentType.ICE_SHARD:
			stats.upgradeGoldCost = 1000 * level;
			stats.upgradeLumberCost = 1000 * level;

			break; 
		case equipmentType.CHAIN_LIGHTNING:
			stats.upgradeGoldCost = 1000 * level;
			stats.upgradeLumberCost = 1000 * level;

			break; 
		case equipmentType.SCIMITAR:
			stats.upgradeGoldCost = 1000 * level;
			stats.upgradeLumberCost = 1000 * level;

			break; 
		case equipmentType.LANCE:
			stats.upgradeGoldCost = 1000 * level;
			stats.upgradeLumberCost = 1000 * level;

			break; 
		case equipmentType.MACE:
			stats.upgradeGoldCost = 1000 * level;
			stats.upgradeLumberCost = 1000 * level;

			break; 
		case equipmentType.REGULAR_SHOT:
			stats.upgradeGoldCost = 1000 * level;
			stats.upgradeLumberCost = 1000 * level;

			break; 
		case equipmentType.MULTI_SHOT:
			stats.upgradeGoldCost = 1000 * level;
			stats.upgradeLumberCost = 1000 * level;

			break; 
		case equipmentType.FLAME_SHOT:
			stats.upgradeGoldCost = 1000 * level;
			stats.upgradeLumberCost = 1000 * level;

			break; 
		case equipmentType.LARGE_HEAL:
			stats.upgradeGoldCost = 1000 * level;
			stats.upgradeLumberCost = 1000 * level;

			break; 
		case equipmentType.CHAIN_HEAL:
			stats.upgradeGoldCost = 1000 * level;
			stats.upgradeLumberCost = 1000 * level;

			break; 
		case equipmentType.BURST_HEAL:
			stats.upgradeGoldCost = 1000 * level;
			stats.upgradeLumberCost = 1000 * level;

			break; 
		case equipmentType.LIFE_STEAL:
			stats.upgradeGoldCost = 1000 * level;
			stats.upgradeLumberCost = 1000 * level;

			break; 
		case equipmentType.CONVERT:
			stats.upgradeGoldCost = 1000 * level;
			stats.upgradeLumberCost = 1000 * level;

			break; 
		case equipmentType.HEALTH_AURA:
			stats.upgradeGoldCost = 1000 * level;
			stats.upgradeLumberCost = 1000 * level;

			break; 
		case equipmentType.SPEED_AURA:
			stats.upgradeGoldCost = 1000 * level;
			stats.upgradeLumberCost = 1000 * level;

			break; 
		case equipmentType.DAMAGE_AURA:
			stats.upgradeGoldCost = 1000 * level;
			stats.upgradeLumberCost = 1000 * level;

			break; 
		case equipmentType.THORNS_AURA:
			stats.upgradeGoldCost = 1000 * level;
			stats.upgradeLumberCost = 1000 * level;

			break; 
		case equipmentType.LIGHT:
			stats.upgradeGoldCost = 1000 * level;
			stats.upgradeLumberCost = 1000 * level;

			break; 
		case equipmentType.MEDIUM:
			stats.upgradeGoldCost = 1000 * level;
			stats.upgradeLumberCost = 1000 * level;

			break;  
		case equipmentType.HEAVY:
			stats.upgradeGoldCost = 1000 * level;
			stats.upgradeLumberCost = 1000 * level;
			break;
		case equipmentType.FORT_BASIC_ATTACK:
			stats.upgradeGoldCost = 1000 * level;
			stats.upgradeLumberCost = 1000 * level;
			break;
		case equipmentType.TOWER_BASIC_ATTACK:
			stats.upgradeGoldCost = 1000 * level;
			stats.upgradeLumberCost = 1000 * level;
			break;
		case equipmentType.TRAP_BASIC_ATTACK:
			stats.upgradeGoldCost = 1000 * level;
			stats.upgradeLumberCost = 1000 * level;
			break;
		case equipmentType.FORTIFIED:
			stats.upgradeGoldCost = 1000 * level;
			stats.upgradeLumberCost = 1000 * level;
			break;
		}

		stats.upgradeGemCost = Mathf.CeilToInt (stats.upgradeTime / 300f);

	}

	//   public string getArmourEnumAsString() {
	//       switch (type) {
	//           case equipmentType.CLOTH:
	//               return "cloth";
	//           case equipmentType.LEATHER:
	//               return "leather";
	//           case equipmentType.CHAIN:
	//               return "chain";
	//           case equipmentType.PLATE:
	//               return "plate";
	//           default:
	//               return "invalid type";
	//       }
	//   }
	//   public string getWeaponAsString() {
	//       switch (type) {
	//           case equipmentType.SWORD:
	//               return "sword";
	//           case equipmentType.AXE:
	//               return "axe";
	//           case equipmentType.SPEAR:
	//               return "spear";
	//           case equipmentType.SHORT_BOW:
	//               return "short_bow";
	//           case equipmentType.LONG_BOW:
	//               return "long_bow";
	//           case equipmentType.CROSS_BOW:
	//               return "cross_bow";
	//           case equipmentType.FIRE_BOLT:
	//               return "fire_bolt";
	//           case equipmentType.ICE_SHARD:
	//               return "ice_shard";
	//           case equipmentType.CHAIN_LIGHTNING:
	//               return "chain_lightning";
	//           case equipmentType.SCIMITAR:
	//               return "scimitar";
	//           case equipmentType.LANCE:
	//               return "lance";
	//           case equipmentType.MACE:
	//               return "mace";
	//           case equipmentType.REGULAR_SHOT:
	//               return "regular_shot";
	//           case equipmentType.MULTI_SHOT:
	//               return "multi_shot";
	//           case equipmentType.FIRE_SHOT:
	//               return "fire_shot";
	//           case equipmentType.LARGE_HEAL:
	//               return "large_heal";
	//           case equipmentType.CHAIN_HEAL:
	//               return "chain_heal";
	//           case equipmentType.BURST_HEAL:
	//               return "burst_heal";
	//           case equipmentType.LIFE_STEAL:
	//               return "life_steal";
	//           case equipmentType.CONVERT:
	//               return "convert";
	//           case equipmentType.HEALTH_AURA:
	//               return "health_aura";
	//           case equipmentType.SPEED_AURA:
	//               return "speed_aura";
	//           case equipmentType.DAMAGE_AURA:
	//               return "damage_aura";
	//           case equipmentType.THORNS_AURA:
	//               return "thorns_aura";
	//           default:
	//               return "invalid type";
	//       }
	//   }

	///// <summary>
	///// Looks up the string value for the corresponding equipment type enum.
	///// </summary>
	///// <returns>The enum as string.</returns>
	//public string getEnumAsString ()
	//{
	//	switch (type) {
	//	case equipmentType.SWORD:
	//		return "sword";
	//	case equipmentType.AXE:
	//		return "axe";
	//	case equipmentType.SPEAR:
	//		return "spear";
	//	case equipmentType.SHORT_BOW:
	//		return "short_bow";
	//	case equipmentType.LONG_BOW:
	//		return "long_bow";
	//	case equipmentType.CROSS_BOW:
	//		return "cross_bow";
	//	case equipmentType.FIRE_BOLT:
	//		return "fire_bolt";
	//	case equipmentType.ICE_SHARD:
	//		return "ice_shard";
	//	case equipmentType.CHAIN_LIGHTNING:
	//		return "chain_lightning";
	//	case equipmentType.SCIMITAR:
	//		return "scimitar";
	//	case equipmentType.LANCE:
	//		return "lance";
	//	case equipmentType.MACE:
	//		return "mace";
	//	case equipmentType.REGULAR_SHOT:
	//		return "regular_shot";
	//	case equipmentType.MULTI_SHOT:
	//		return "multi_shot";
	//	case equipmentType.FIRE_SHOT:
	//		return "fire_shot";
	//	case equipmentType.LARGE_HEAL:
	//		return "large_heal";
	//	case equipmentType.CHAIN_HEAL:
	//		return "chain_heal";
	//	case equipmentType.BURST_HEAL:
	//		return "burst_heal";
	//	case equipmentType.LIFE_STEAL:
	//		return "life_steal";
	//	case equipmentType.CONVERT:
	//		return "convert";
	//	case equipmentType.HEALTH_AURA:
	//		return "health_aura";
	//	case equipmentType.SPEED_AURA:
	//		return "speed_aura";
	//	case equipmentType.DAMAGE_AURA:
	//		return "damage_aura";
	//	case equipmentType.THORNS_AURA:
	//		return "thorns_aura";
	//	case equipmentType.CLOTH:
	//		return "cloth";
	//	case equipmentType.LEATHER:
	//		return "leather";
	//	case equipmentType.CHAIN:
	//		return "chain";
	//	case equipmentType.PLATE:
	//		return "plate";
	//	default:
	//		return "";
	//	}
	//}
}

public enum equipmentType
{
	SWORD,
	AXE,
	SPEAR,
	SHORT_BOW,
	LONG_BOW,
	CROSS_BOW,
	FIRE_BOLT,
	ICE_SHARD,
	CHAIN_LIGHTNING,
	SCIMITAR,
	LANCE,
	MACE,
	REGULAR_SHOT,
	MULTI_SHOT,
	FLAME_SHOT,
	LARGE_HEAL,
	CHAIN_HEAL,
	BURST_HEAL,
	LIFE_STEAL,
	CONVERT,
	HEALTH_AURA,
	SPEED_AURA,
	DAMAGE_AURA,
	THORNS_AURA,
	LIGHT,
	MEDIUM,
	HEAVY,
	FORT_BASIC_ATTACK,
	TOWER_BASIC_ATTACK,
	TRAP_BASIC_ATTACK,
	FORTIFIED
}