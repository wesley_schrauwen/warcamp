﻿using UnityEngine.UI;
using UnityEngine;

public class _baseUnit : MonoBehaviour
{

	public string unitName;
	public int level;
	public int guid;
	public Texture2D card;
	public int maxLevel;
	public string description;

	public void setGUID(int guid){
		this.guid = guid;
	}

	public int getGUID(){
		return this.guid;
	}

}
