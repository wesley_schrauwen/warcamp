﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

//[System.Serializable]
public class creepStatistics
{
	public int hp;
	public int moveSpeed;
	public int range;
	public int damage;
	public int attackSpeed;
	public int goldCost;
	public int lumberCost;
}

//[System.Serializable]
public class unitAnimations
{
	public Animation death;
	public Animation walking;
	public Animation bored;
}

public class unit : _baseUnit
{

	private GameObject selectedWeapon = null;
	private GameObject[] availableWeapons;
	private GameObject selectedArmour = null;
	private GameObject[] availableArmour;
	public creepStatistics creepStats = new creepStatistics ();
	public unitAnimations animations;
	public upgradeable upgrade;

	public creepType type;
	public bool isBuildable;

	private float moveToX;
	//	private float moveToY;
	private float moveToZ;
	private bool mayMove = false;

	void Update ()
	{
		if (mayMove) {

			var xAdjustment = (moveToX - gameObject.transform.position.x) * Time.deltaTime;
			var zAdjustment = (moveToZ - gameObject.transform.position.z) * Time.deltaTime;

			gameObject.transform.position = new Vector3 (
				gameObject.transform.position.x + xAdjustment, 
				25, 
				gameObject.transform.position.z + zAdjustment);

		}
	}

	private void fire ()
	{
		var weapon = selectedWeapon.GetComponent<equipment> ();
		weapon.fire ();
	}


	public void updateUnit (string[] unitPacket, matchCoordinator invokerReference)
	{

		moveToX = invokerReference.getUnitX (unitPacket);
//		moveToY = invokerReference.getUnitY (unitPacket);
		moveToZ = invokerReference.getUnitZ (unitPacket);

		if (!mayMove) {
			mayMove = true;
		}

	}

	#region unit setup

	/// <summary>
	/// Sets up this particular instance of a unit
	/// </summary>
	/// <param name="level">The current level of the unit</param>
	/// <param name="isUpgrading">If set to <c>true</c> is upgrading.</param>
	/// <param name="upgradeComplete">Upgrade completion date</param>
	/// <param name="upgradeBegin">Upgrade begin date.</param>
	/// <param name="selectedWeapon">Selected weapon.</param>
	/// <param name="selectedArmour">Selected armour.</param>
	/// <param name="availableWeapons">Available weapons.</param>
	/// <param name="availableArmour">Available armour.</param>
	public void setupUnit (int level, int maxLevel, bool isUpgrading, ulong upgradeComplete, ulong upgradeBegin, 
	                       string selectedWeapon, string selectedArmour, List<string> availableWeapons, List<string> availableArmour)
	{

		this.level = level;
		this.maxLevel = maxLevel;

		this.upgrade.isUpgrading = isUpgrading;
		this.upgrade.upgradeComplete = upgradeComplete;
		this.upgrade.upgradeBegin = upgradeBegin;

		setupBaseStats (level, creepStats, type);
		setupUpgradeCosts (level < maxLevel ? ++level : 0, upgrade, type);

		if (availableArmour != null && availableArmour.Count > 0) {	
			this.availableArmour = profileManager.getInstance ().getArmourForUnit (availableArmour, selectedArmour).ToArray ();
			this.selectedArmour = this.availableArmour [0];	

			applyEquipmentStats (this.selectedArmour);
		}
			
		if (availableWeapons != null && availableWeapons.Count > 0) {
			this.availableWeapons = profileManager.getInstance ().getWeaponForUnit (availableWeapons, selectedWeapon).ToArray ();
			this.selectedWeapon = this.availableWeapons [0];

			applyEquipmentStats (this.selectedWeapon);
		}
			
		Debug.Log ("unit setup: \t\t" + unitName);
		Debug.Log ("unit level is: " + level);
//		Debug.Log ("current weapon is: " + this.selectedWeapon.GetComponent<equipment> ().equipmentName);
//		Debug.Log ("current armour is: " + this.selectedArmour.GetComponent<equipment> ().equipmentName);
		if (this.availableWeapons != null) {
			Debug.Log ("available weapons:");
			foreach (GameObject weapon in this.availableWeapons) {
				Debug.Log (weapon.GetComponent<equipment> ().equipmentName);
			}
		}

		if (this.availableArmour != null) {
			Debug.Log ("available armour:");
			foreach (GameObject armour in this.availableArmour) {
				Debug.Log (armour.GetComponent<equipment> ().equipmentName);
			}
		}

	}

	public void swapOutSelectedArmour (GameObject item)
	{
		removeEquipmentStats (selectedArmour);
		applyEquipmentStats (item);
	}

	public void swapOutSelectedWeapon (GameObject item)
	{
		removeEquipmentStats (selectedWeapon);
		applyEquipmentStats (item);
	}

	public void applyEquipmentStats (equipment item, creepStatistics creepStats)
	{

		creepStats.goldCost += item.stats.goldCostCH;
		creepStats.lumberCost += item.stats.lumberCostCH;
		creepStats.hp += item.stats.hpCH;
		creepStats.damage += item.stats.damageCH;
		creepStats.attackSpeed += item.stats.attackSpeedCH;
		creepStats.moveSpeed += item.stats.moveSpeedCH;
		creepStats.range += item.stats.rangeCH;

	}

	public void applyEquipmentStats (GameObject item)
	{
		applyEquipmentStats (item.GetComponent<equipment> (), this.creepStats);
	}

	public void applyEquipmentStats (GameObject item, creepStatistics creepStats)
	{
		applyEquipmentStats (item.GetComponent<equipment> (), creepStats);
	}

	public void removeEquipmentStats (equipment item, creepStatistics creepStats)
	{

		creepStats.goldCost -= item.stats.goldCostCH;
		creepStats.lumberCost -= item.stats.lumberCostCH;
		creepStats.hp -= item.stats.hpCH;
		creepStats.damage -= item.stats.damageCH;
		creepStats.attackSpeed -= item.stats.attackSpeedCH;
		creepStats.moveSpeed -= item.stats.moveSpeedCH;
		creepStats.range -= item.stats.rangeCH;
	}

	public void removeEquipmentStats (GameObject item)
	{
		removeEquipmentStats (item.GetComponent<equipment> (), this.creepStats);
	}

	public void setupBaseStats (int level, creepStatistics creepStats, creepType type)
	{

		switch (type) {
		case creepType.WARRIOR:
			creepStats.moveSpeed = 20;
			creepStats.goldCost = 200 + 15 * level;
			creepStats.lumberCost = 300 + 15 * level;
			creepStats.range = 1;
			creepStats.attackSpeed = 5 + 10 * level;
			creepStats.damage = 20 + 10 * level;
			creepStats.hp = 120 + 30 * level;
			break;
		case creepType.MONK:
			creepStats.moveSpeed = 20;
			creepStats.goldCost = 60 + 15 * level;
			creepStats.lumberCost = 40 + 15 * level;
			creepStats.range = 1;
			creepStats.attackSpeed = 5 + 10 * level;
			creepStats.damage = 20 + 10 * level;
			creepStats.hp = 60 + 30 * level;
			break;
		case creepType.HEALER:
			creepStats.moveSpeed = 20;
			creepStats.goldCost = 60 + 15 * level;
			creepStats.lumberCost = 40 + 15 * level;
			creepStats.range = 10;
			creepStats.attackSpeed = 5 + 10 * level;
			creepStats.damage = 20 + 10 * level;
			creepStats.hp = 95 + 30 * level;
			break;
		case creepType.ARCHER:
			creepStats.moveSpeed = 20;
			creepStats.goldCost = 60 + 15 * level;
			creepStats.lumberCost = 40 + 15 * level;
			creepStats.range = 1;
			creepStats.attackSpeed = 5 + 10 * level;
			creepStats.damage = 20 + 10 * level;
			creepStats.hp = 80 + 30 * level;
			break;
		case creepType.CAVALRY:
			creepStats.moveSpeed = 20;
			creepStats.goldCost = 60 + 15 * level;
			creepStats.lumberCost = 40 + 15 * level;
			creepStats.range = 1;
			creepStats.attackSpeed = 5 + 10 * level;
			creepStats.damage = 20 + 10 * level;
			creepStats.hp = 80 + 30 * level;
			break;
		case creepType.CATAPULT:
			creepStats.moveSpeed = 20;
			creepStats.goldCost = 60 + 15 * level;
			creepStats.lumberCost = 40 + 15 * level;
			creepStats.range = 1;
			creepStats.attackSpeed = 5 + 10 * level;
			creepStats.damage = 20 + 10 * level;
			creepStats.hp = 80 + 30 * level;
			break;
		case creepType.ZOMBIE:
			creepStats.moveSpeed = 20;
			creepStats.goldCost = 60 + 15 * level;
			creepStats.lumberCost = 40 + 15 * level;
			creepStats.range = 1;
			creepStats.attackSpeed = 5 + 10 * level;
			creepStats.damage = 20 + 10 * level;
			creepStats.hp = 80 + 30 * level;
			break;
		case creepType.WIZARD:
			creepStats.moveSpeed = 20;
			creepStats.goldCost = 60 + 15 * level;
			creepStats.lumberCost = 40 + 15 * level;
			creepStats.range = 1;
			creepStats.attackSpeed = 5 + 10 * level;
			creepStats.damage = 20 + 10 * level;
			creepStats.hp = 80 + 30 * level;
			break;
		case creepType.COMMAND_CENTRE:
			creepStats.moveSpeed = 0;
			creepStats.goldCost = 60 + 15 * level;
			creepStats.lumberCost = 40 + 15 * level;
			creepStats.range = 1;
			creepStats.attackSpeed = 5 + 10 * level;
			creepStats.damage = 20 + 10 * level;
			creepStats.hp = 80 + 30 * level;
			break;
		case creepType.TOWER:
			creepStats.moveSpeed = 0;
			creepStats.goldCost = 60 + 15 * level;
			creepStats.lumberCost = 40 + 15 * level;
			creepStats.range = 1;
			creepStats.attackSpeed = 5 + 10 * level;
			creepStats.damage = 20 + 10 * level;
			creepStats.hp = 80 + 30 * level;
			break;
		case creepType.FORT:
			creepStats.moveSpeed = 0;
			creepStats.goldCost = 60 + 15 * level;
			creepStats.lumberCost = 40 + 15 * level;
			creepStats.range = 1;
			creepStats.attackSpeed = 5 + 10 * level;
			creepStats.damage = 20 + 10 * level;
			creepStats.hp = 80 + 30 * level;
			break;
		case creepType.TRAP:
			creepStats.moveSpeed = 0;
			creepStats.goldCost = 60 + 15 * level;
			creepStats.lumberCost = 40 + 15 * level;
			creepStats.range = 1;
			creepStats.attackSpeed = 5 + 10 * level;
			creepStats.damage = 20 + 10 * level;
			creepStats.hp = 80 + 30 * level;
			break;
		case creepType.ROAD:
			creepStats.moveSpeed = 0;
			creepStats.goldCost = 60 + 15 * level;
			creepStats.lumberCost = 40 + 15 * level;
			creepStats.range = 1;
			creepStats.attackSpeed = 5 + 10 * level;
			creepStats.damage = 20 + 10 * level;
			creepStats.hp = 80 + 30 * level;
			break;
		case creepType.GOLD_MINE:
			creepStats.moveSpeed = 0;
			creepStats.goldCost = 60 + 15 * level;
			creepStats.lumberCost = 40 + 15 * level;
			creepStats.range = 1;
			creepStats.attackSpeed = 5 + 10 * level;
			creepStats.damage = 20 + 10 * level;
			creepStats.hp = 80 + 30 * level;
			break;
		case creepType.LUMBER_MILL:
			creepStats.moveSpeed = 0;
			creepStats.goldCost = 60 + 15 * level;
			creepStats.lumberCost = 40 + 15 * level;
			creepStats.range = 1;
			creepStats.attackSpeed = 5 + 10 * level;
			creepStats.damage = 20 + 10 * level;
			creepStats.hp = 80 + 30 * level;
			break;
		}
	}

	public void setupUpgradeCosts (int level, upgradeable stats, creepType type)
	{

		switch (type) {
		case creepType.WARRIOR:
			stats.upgradeGoldCost = 1000 * level;
			stats.upgradeLumberCost = 1000 * level;
			break;
		case creepType.MONK:
			stats.upgradeGoldCost = 1000 * level;
			stats.upgradeLumberCost = 1000 * level;
			break;
		case creepType.HEALER:
			stats.upgradeGoldCost = 1000 * level;
			stats.upgradeLumberCost = 1000 * level;
			break;
		case creepType.ARCHER:
			stats.upgradeGoldCost = 1000 * level;
			stats.upgradeLumberCost = 1000 * level;
			break;
		case creepType.CAVALRY:
			stats.upgradeGoldCost = 1000 * level;
			stats.upgradeLumberCost = 1000 * level;
			break;
		case creepType.CATAPULT:
			stats.upgradeGoldCost = 1000 * level;
			stats.upgradeLumberCost = 1000 * level;
			break;
		case creepType.ZOMBIE:
			stats.upgradeGoldCost = 1000 * level;
			stats.upgradeLumberCost = 1000 * level;
			break;
		case creepType.WIZARD:
			stats.upgradeGoldCost = 1000 * level;
			stats.upgradeLumberCost = 1000 * level;
			break;
		case creepType.COMMAND_CENTRE:
			stats.upgradeGoldCost = 1000 * level;
			stats.upgradeLumberCost = 1000 * level;
			break;
		case creepType.TOWER:
			stats.upgradeGoldCost = 1000 * level;
			stats.upgradeLumberCost = 1000 * level;
			break;
		case creepType.FORT:
			stats.upgradeGoldCost = 1000 * level;
			stats.upgradeLumberCost = 1000 * level;
			break;
		case creepType.TRAP:
			stats.upgradeGoldCost = 1000 * level;
			stats.upgradeLumberCost = 1000 * level;
			break;
		case creepType.ROAD:
			stats.upgradeGoldCost = 1000 * level;
			stats.upgradeLumberCost = 1000 * level;
			break;
		case creepType.GOLD_MINE:
			stats.upgradeGoldCost = 1000 * level;
			stats.upgradeLumberCost = 1000 * level;
			break;
		case creepType.LUMBER_MILL:
			stats.upgradeGoldCost = 1000 * level;
			stats.upgradeLumberCost = 1000 * level;
			break;
		}

		stats.upgradeGemCost = Mathf.CeilToInt (stats.upgradeTime / 3000);

	}

	/// <summary>
	/// Must be called to enable unit movement
	/// </summary>
	public void enableUnitMovement ()
	{
		mayMove = true;
	}

	#endregion

	#region getters & setters

	public creepType getUnitType ()
	{
		return type;
	}

	public GameObject[] getAvailableArmour ()
	{
		return availableArmour;
	}

	public GameObject[] getAvailableWeapons ()
	{
		return availableWeapons;
	}

	public GameObject getSelectedArmour ()
	{
		return selectedArmour;
	}

	public GameObject getSelectedWeapon ()
	{
		return selectedWeapon;
	}

	public int getGoldCost ()
	{
		return creepStats.goldCost;
	}

	public int getLumberCost ()
	{
		return creepStats.lumberCost;
	}

	public bool getIsBuildable ()
	{
		return this.isBuildable;
	}

	public string getUnitName ()
	{
		return unitName;
	}

    public string getItemGroup() {
        switch (type) {
            case creepType.ARCHER:
                return "units";
            case creepType.CATAPULT:
                return "units";
            case creepType.CAVALRY:
                return "units";
            case creepType.HEALER:
                return "units";
            case creepType.MONK:
                return "units";
            case creepType.WARRIOR:
                return "units";
            case creepType.WIZARD:
                return "units";
            case creepType.ZOMBIE:
                return "units";
            default:
                return "defenses";
        }
    }

	public int getUnitLevel ()
	{
		return level;
	}

	public string getUnitDescription ()
	{
		return description;
	}

	public Texture2D getCard ()
	{
		return card;
	}

	public void setUnitToType (int peerID, string type)
	{

		foreach (GameObject item in profileManager.getInstance().getPeerProfile(peerID).getStructures()) {

			if (item.GetComponent<unit> ().getUnitName ().Equals (type)) {

				Destroy (gameObject.transform.GetChild (0).gameObject);

				GameObject newModel = Instantiate (item.transform.GetChild (0).gameObject);
				newModel.transform.SetParent (gameObject.transform, false);

				base.unitName = type;
				base.level = item.GetComponent<unit> ().level;


			}

		}

	}

	public void setEquipment (string itemType, string item)
	{
		if (itemType.Equals ("weapon")) {
			setSelectedWeapon (item);
		} else {
			setSelectedArmour (item);
		}
	}

	public void setEquipment (bool isWeapon, string item)
	{
		if (isWeapon) {
			setSelectedWeapon (item);
		} else {
			setSelectedArmour (item);
		}
	}

	private void setSelectedArmour (string item)
	{
		var newArmour = profileManager.getInstance ().getArmourForUnit (item);
		swapOutSelectedArmour (newArmour);
		this.selectedArmour = newArmour;
	}

	private void setSelectedWeapon (string item)
	{
		var newWeapon = profileManager.getInstance ().getWeaponForUnit (item);
		swapOutSelectedWeapon (newWeapon);
		this.selectedWeapon = newWeapon;
	}

	#endregion

}

public enum creepType
{
	ARCHER,
	CATAPULT,
	CAVALRY,
	CONSTRUCTION_SITE,
	WARRIOR,
	MONK,
	HEALER,
	ZOMBIE,
	WIZARD,
	COMMAND_CENTRE,
	TOWER,
	FORT,
	ROAD,
	TRAP,
	GOLD_MINE,
	LUMBER_MILL
}
