﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class exitApplication : MonoBehaviour
{

	void Start ()
	{
		
		gameObject.GetComponent<Button> ().onClick.AddListener (() => {
		
			Application.Quit ();

		});

	}


	void Destroy ()
	{
		gameObject.GetComponent<Button> ().onClick.RemoveAllListeners ();
	}
}
