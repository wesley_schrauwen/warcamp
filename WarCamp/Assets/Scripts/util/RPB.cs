﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class RPB : MonoBehaviour
{

	public Transform LoadingBar;
	public Transform TextIndicator;
	public Transform TextLoading;
	[SerializeField] private float currentAmount;
	[SerializeField] private float speed;

	void Update ()
	{
	
		if (currentAmount < 100) {
			currentAmount += speed * Time.deltaTime;
			TextIndicator.GetComponent<Text> ().text = ((int)currentAmount).ToString () + "%";
			TextLoading.gameObject.SetActive (true);
		}

		LoadingBar.GetComponent<Image> ().fillAmount = currentAmount / 100;

	}
}
