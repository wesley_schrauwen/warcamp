﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

/// <summary>
/// Rescales the X and Width of a UI elements Rect Transform at run time.
/// Requires the UI be designed in 1920 x 1080 and using a reference screen size of 1000 x 800.
/// Scales everything using a normalised value of 1422.22f.
/// </summary>
public class rescaleAtRuntime : MonoBehaviour
{
	[Tooltip ("The number of parent GameObjects that have the 'rescaleAtRuntime' script.")]
	public float adjustments = 0;
	[Tooltip ("Should this element maintain its current ratio on adjustment?")]
	public bool maintainRatio = false;

	public bool logEvents = false;
	// Use this for initialization
	void Start ()
	{

		if (adjustments >= 0) {
		
			var parent = GameObject.Find ("Canvas").GetComponent<RectTransform> ().rect;
			var currentTransform = gameObject.GetComponent< RectTransform > ();

			var widthRatio = parent.width / 1422.22f; // 1422.22f the value from the parent canvas reference ratio of 1000 and screen resolution 1920.
			var heightRatio = currentTransform.rect.height / currentTransform.rect.width;

			var width = currentTransform.rect.width * widthRatio;
			var height = currentTransform.rect.height;

			if (maintainRatio) {
				height = width * heightRatio;
			}		

			var xAdjustment = currentTransform.position.x - currentTransform.position.x * widthRatio;

			if (logEvents)
				Debug.Log (gameObject.GetComponent<RectTransform> ().sizeDelta);

			gameObject.GetComponent<RectTransform> ().sizeDelta = new Vector2 (width, height);

			if (logEvents)
				Debug.Log (gameObject.GetComponent<RectTransform> ().sizeDelta);

			if (adjustments != 1) {
				
				if (adjustments == 0) {

					gameObject.GetComponent<RectTransform> ().position = new Vector3 (currentTransform.position.x - xAdjustment, 
						currentTransform.position.y, 
						currentTransform.position.z);	

				} else {

					gameObject.GetComponent<RectTransform> ().position = new Vector3 (currentTransform.position.x + (xAdjustment * adjustments - 1), 
						currentTransform.position.y,
						currentTransform.position.z);

				}

			}

		}

	}
	
}
