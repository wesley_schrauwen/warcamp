﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using UnityEngine;

/// <summary>
/// Collection of validation methods.
/// </summary>
public static class isValid
{

	/// <summary>
	/// Validates the text as a valid email address.
	/// </summary>
	/// <param name="emailText">The Email Address.</param>
	public static bool email (string emailAddress)
	{

		if (emailAddress != null) {
			return Regex.IsMatch (emailAddress, @"^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@"
			+ @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\."
			+ @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
			+ @"([a-zA-Z]+[\w-]+\.)+[a-zA-Z]{2,4})$");
		} else {
			return false;
		}
				
	}

}

public static class standardColors
{

	/// <summary>
	/// The most widely used text unselected color in the game.
	/// Typically used where a off white / beige type color is needed.
	/// RGBA: 192, 186, 172, 128.
	/// </summary>
	public static Color32 unselected = new Color32 (198, 186, 172, 128);
	/// <summary>
	/// The most widely used text selector color in the game.
	/// RGBA: 255, 255, 255, 192.
	/// </summary>
	public static Color32 selected = new Color32 (255, 255, 255, 255);
	/// <summary>
	/// The most widely used text color in the game.
	/// RGBA: 0, 0, 0, 192.
	/// </summary>
	public static Color32 text = new Color32 (0, 0, 0, 192);
	/// <summary>
	/// The red used to color icons.
	/// Normally used to indicate a negative value.
	/// RGBA: 255, 0, 0, 255
	/// </summary>
	public static Color32 red = new Color32 (255, 0, 0, 255);
	/// <summary>
	/// The green used to color icons.
	/// /// Normally used to indicate a positive value.
	/// RGBA: 255, 0, 0, 255
	/// </summary>
	public static Color32 green = new Color32 (0, 255, 0, 255);
	/// <summary>
	/// The yellow used to color icons.
	/// /// Normally used to indicate a no change of value.
	/// RGBA: 255, 0, 0, 255
	/// </summary>
	public static Color32 yellow = new Color32 (255, 185, 0, 255);
	/// <summary>
	/// The teal used to color local player build sites
	/// </summary>
	public static Color32 teal = new Color32 (0, 255, 255, 255);


	/// <summary>
	/// Sets the alpha of the passed in color.
	/// </summary>
	/// <returns>The alpha.</returns>
	/// <param name="existing">Existing Color.</param>
	/// <param name="alpha">The new alpha value [0-1].</param>
	public static Color32 setAlpha (Color32 existing, float alpha)
	{
		return new Color32 (existing.r, existing.g, existing.b, (byte)(alpha * 255));
	}

}

public static class Date
{
	public static System.Int64 now = (System.Int64)((System.DateTime.UtcNow.Subtract (new System.DateTime (1970, 1, 1, 0, 0, 0, System.DateTimeKind.Utc))).TotalMilliseconds);
}

public static class resize
{

	/// <summary>
	/// Resizes the scroll view vertically.
	/// </summary>
	/// <param name="scrollView">Scroll view.</param>
	/// <param name="prefab">The item to size for.</param>
	/// <param name="count">The number of items in the scroll view.</param>
	/// <param name="offset">Extra padding.</param>
	public static void scrollView_Vertical (GameObject scrollView, GameObject prefab, int count, float offset = 0)
	{
	
		scrollView.GetComponent<RectTransform> ().sizeDelta = new Vector2 (
			scrollView.GetComponent<RectTransform> ().sizeDelta.x,
			(prefab.GetComponent<RectTransform> ().sizeDelta.y + offset) * count
		);

	}

	/// <summary>
	/// Resizes the scroll view horizontally.
	/// </summary>
	/// <param name="scrollView">Scroll view.</param>
	/// <param name="prefab">The item to size for.</param>
	/// <param name="count">The number of items in the scroll view.</param>
	/// <param name="offset">Extra padding.</param>
	public static void scrollView_Horizontal (GameObject scrollView, GameObject prefab, int count, float offset = 0)
	{

		scrollView.GetComponent<RectTransform> ().sizeDelta = new Vector2 (
			(prefab.GetComponent<RectTransform> ().sizeDelta.x + offset) * count,
			scrollView.GetComponent<RectTransform> ().sizeDelta.y
		);

	}

}
	
