﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class openWebsite : MonoBehaviour
{

	void Start ()
	{
		
		gameObject.GetComponent<Button> ().onClick.AddListener (() => {

			Application.OpenURL ("http://www.stackle.co.za");

		});

	}

	void Destroy ()
	{
		gameObject.GetComponent<Button> ().onClick.RemoveAllListeners ();
	}
}
