﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class gameOverCanvas : MonoBehaviour
{

	public Text didWinText;
	public Button finishButton;
	public Text goldWinnings;
	public Text lumberWinnings;
	public RawImage didWinImage;
	public Texture2D victoryImage;
	public Texture2D defeatImage;

	void Start ()
	{
		
		finishButton.onClick.AddListener (() => {

			GameManager.getInstance ().finishMatch ();

		});

	}

	public void gameOver (int winnerID, int goldGained, int lumberGained)
	{

		didWinText.text = winnerID == GameManager.getInstance ().getClientPeerID () ?
			"VICTORY" : 
			"DEFEAT";

		didWinImage.texture = winnerID == GameManager.getInstance ().getClientPeerID () ?
			victoryImage :
			defeatImage;

		goldWinnings.text = goldGained.ToString ();
		lumberWinnings.text = lumberGained.ToString ();

	}

	void OnDestroy ()
	{
		finishButton.onClick.RemoveAllListeners ();
	}

}
