﻿using UnityEngine;
using System.Collections;

public class PanelSlider : MonoBehaviour {

    public void EnableBoolAnimator(Animator Slider)
    {
        if (Slider.GetBool("IsDisplayed") == true)
        {
            Slider.SetBool("IsDisplayed", false);
        }
        else
        {
            Slider.SetBool("IsDisplayed", true);
        }
    }
}
