﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class matchCanvasManager : MonoBehaviour
{
	[Header ("Main Scene")]
	public Text timer;
	public Text goldMain;
	public Text lumberMain;

	[Header ("Unit Draft")]
	public GameObject unitDraftCanvas;
	public Text goldDraft;
	public Text lumberDraft;

	[Header ("Canvas")]
	public GameObject mapCanvas;
	public GameObject localDraftManager;
	public GameObject peerDraftManager;
	public GameObject notificationManager;
	public GameObject unitEquipmentManager;
	public GameObject constructionCanvas;
	public GameObject cancelConstructionCanvas;
	public GameObject gameOverCanvas;

	private GameObject currentCanvas;
	private Stack<GameObject> canvasStack = new Stack<GameObject> ();
	private matchCoordinator matchInstance;

	private int matchGold;
	private int matchLumber;

	private int timeToSpawn;

	void Start ()
	{
		matchInstance = GameObject.Find ("MatchCoordinator").GetComponent<matchCoordinator> ();
		currentCanvas = gameObject;
		mapCanvas.SetActive (true);
		unitDraftCanvas.GetComponent<unitDraftManager> ().setupCanvas ();
	}

	IEnumerator timerCoroutine (int timeToSpawn)
	{

		while (timeToSpawn > 0) {

			timeToSpawn -= 1;

			timer.text = (timeToSpawn).ToString ();

			yield return new WaitForSeconds (1f);

		}
			
	}

	#region navigation

	public void switchToCanvas (GameObject canvas)
	{

		if (currentCanvas != null &&
		    currentCanvas.name.Equals (canvas.name) &&
		    canvasStack.Count > 0) {

			popCanvas ();

		} else {

			pushCanvas (canvas);

		}

	}

	public void pushCanvas (GameObject canvas)
	{

		if (currentCanvas != null && currentCanvas.name.Equals (canvas.name)) { // prevent the same canvas from being pushed onto itself
			return;
		}

		canvasStack.Push (currentCanvas);
		currentCanvas = canvas;
		canvas.SetActive (true);

	}

	/// <summary>
	/// Pops the current canvas and displays the previous one in the stack.
	/// </summary>
	public void popCanvas ()
	{

		currentCanvas.SetActive (false);

		if (canvasStack.Count != 0) {
			currentCanvas = (GameObject)canvasStack.Pop ();
			currentCanvas.SetActive (true);
		} else {
			currentCanvas = gameObject;
		}			

	}

	public void switchToEquipmentManager (unit _unit)
	{
		pushCanvas (unitEquipmentManager);
		unitEquipmentManager.GetComponent<unitEquipmentManager> ().setupCanvas (_unit);
	}

	public void switchToConstructionCanvas (int siteReference)
	{
		pushCanvas (constructionCanvas);
		constructionCanvas.GetComponent<constructionCanvasManager> ().setupCanvas (siteReference);
	}

	public void switchToCancelConstructionCanvas(int buildSite){
		pushCanvas (cancelConstructionCanvas);
		cancelConstructionCanvas.GetComponent<cancelStructureCanvas> ().cancelStructure (buildSite);
	}

	#endregion

	#region local and peer draft managers

	public void increaseLocalDraft (unit _unit)
	{

		localDraftManager.GetComponent<localPlayerDraftManager> ().increaseUnitCount (_unit);
		unitDraftCanvas.GetComponent<unitDraftManager> ().increaseUnitCount (_unit);

	}

	public void gameOver (int winnerID, int gold, int lumber)
	{
		gameOverCanvas.SetActive (true);
		gameOverCanvas.GetComponent<gameOverCanvas> ().gameOver (winnerID, gold, lumber);
	}

	public void decreaseLocalDraft (unit _unit)
	{

		localDraftManager.GetComponent<localPlayerDraftManager> ().decreaseUnitCount (_unit);
		unitDraftCanvas.GetComponent<unitDraftManager> ().decreaseUnitCount (_unit);

	}

	public void setUnknownPeerDraft (int count)
	{
		peerDraftManager.GetComponent<peerDraftManager> ().setUnknownUnitCount (count);
	}

	public void updatePeerDraft (unit _unit, int amount)
	{
		peerDraftManager.GetComponent<peerDraftManager> ().setPeerDraft (_unit, amount);
	}

	public void resetDrafts ()
	{
		peerDraftManager.GetComponent<peerDraftManager> ().resetPeerDraft ();
		localDraftManager.GetComponent<localPlayerDraftManager> ().reset ();
		unitDraftCanvas.GetComponent<unitDraftManager> ().resetUnitCount ();
	}

	public void lockInDraft ()
	{
		matchInstance.requestDraftLockIn ();
	}

	public void reloadUnitDraftManager (unit _unit)
	{
		
		unitDraftCanvas.GetComponent<unitDraftManager> ().reloadUnit (_unit);
	}

	public void displayLockIn (int gold, int lumber)
	{

		notificationManager.GetComponent<notificationManager> ().displayLockIn (gold, lumber);

	}

	public void setResources (int gold, int lumber)
	{
		goldMain.text = gold.ToString ();
		lumberMain.text = lumber.ToString ();
	}

	public void displayResourceNotification (int gold, int lumber)
	{
		notificationManager.GetComponent<notificationManager> ().displayResourceNotification (gold, lumber);
	}

	public void draftUnit (unit _unit)
	{
		matchInstance.requestUnitDraft (_unit);
	}

	public void draftUnit (creepType type)
	{
		matchInstance.requestUnitDraft (type);
	}

	#endregion

	public void assignBuildSite (int peerID, int site)
	{

		mapCanvas.GetComponent<mapCanvasManager> ().assignBuildSite (peerID, site);

	}

	public void setSiteIcon (int peerID, int buildSite, string structureCode)
	{
		mapCanvas.GetComponent<mapCanvasManager> ().setSiteIcon (peerID, buildSite, structureCode);
	}

	public void setHasStructure (int site, bool hasStructure)
	{
		mapCanvas.GetComponent<mapCanvasManager> ().setHasStructure (site, hasStructure);
	}

	public void removeSiteIcon(int buildSite){
		mapCanvas.GetComponent<mapCanvasManager> ().removeSiteIcon (buildSite);
	}

	public void startSpawnTimer (int _spawnTimer)
	{		
//		Debug.Log ("beginning spawn timer...");
		var time = _spawnTimer * 0.001f;
		timer.text = time.ToString ();
//		print ("timer: " + time);
		StartCoroutine (timerCoroutine ((int)time));
	}

}
