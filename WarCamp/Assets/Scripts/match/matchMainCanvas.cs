﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
 using System;
//using System.Diagnostics;

public class matchMainCanvas : MonoBehaviour {
    
    public Text goldText;
    public Text lumberText;
    public Text spawnText;
    // public DateTime endTime;
    //public TimeSpan span;
    private int gold;
    private int lumber;
    private float spawnTime = 0;
    //public Button cancelButton;
    //public Button editDraftButton;
    DateTime timeStarted = DateTime.Now;
    bool done = false;
  
    private matchCanvasManager matchCanvasRef;

    TimeSpan elapsed;
    // Use this for initialization


    void Start () {
        matchCanvasRef = GameObject.FindGameObjectWithTag("MainCanvas").GetComponent<matchCanvasManager>();
      



        //editDraftButton.onClick.AddListener(matchCanvasRef.activateUnitCanvas);
        //cancelButton.onClick.AddListener(matchCanvasRef.activateExitCanvas);
    }
	
	// Update is called once per frame
	void Update () {
      
      
        displaySpawnTime();
    }
  /*  private IEnumerator Countdown(int time)
    {
        while (time > 0)
        {
            spawnTime = time--;
            spawnText.text =  spawnTime.ToString();
            yield return new WaitForSeconds(1);
        }

     
    }*/

    void GameStart()
    {

    }
    public void setGold(int myGold)
    {
        this.gold = myGold;
        goldText.text = myGold.ToString();
    }



    public void setLumber(int myLumber)
    {
        this.lumber = myLumber;
        lumberText.text = myLumber.ToString();

    }
    public void setSpawnTime(float mySpawnTime)
    {
        this.spawnTime = mySpawnTime;
   //     displaySpawnTime();
      //  StartCoroutine("Countdown", mySpawnTime);
      // this.mySpawnTime = mySpawnTime;

    }

    public void displaySpawnTime()
    {
        elapsed = DateTime.Now.Subtract(timeStarted);
        if ((int)elapsed.Seconds < spawnTime &&!done)
        {
            
           // Debug.Log("Waitting..." + (20 - elapsed.Seconds) + "  " + timeStarted);

            spawnText.text = (spawnTime - elapsed.Seconds).ToString();
        }
        else
        {
            done = true;
            timeStarted = DateTime.Now;
            spawnText.text = "Ready!!";
         GameStart();
        }
    }

    public int getLumber()
    {
        return lumber;
    }
    public int getGold()
    {
        return gold;
    }



}
