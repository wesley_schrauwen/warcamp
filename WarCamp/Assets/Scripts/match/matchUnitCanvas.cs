﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class matchUnitCanvas : MonoBehaviour
{
	public Text goldText;
	public Text lumberText;
  
	public Button cancelButton;
	public Button lockButton;
	private matchCanvasManager matchCanvasRef;
	public matchMainCanvas matchMainCanvasRef;
	// Use this for initialization


	// Update is called once per frame
	void Update ()
	{
	
	}

	public void setResources ()
	{
		goldText.text = matchMainCanvasRef.getGold ().ToString (); 
		lumberText.text = matchMainCanvasRef.getLumber ().ToString ();
	}
}
