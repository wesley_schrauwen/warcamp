﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.IO;
using System.Collections.Generic;

public class draftEditing : MonoBehaviour
{

    // Use this for initialization
    private string AssetPath = "/resources/cards/units/Images";
    

    public GameObject parentPanel;
    public GameObject prefabPanel;
    public RawImage prefab;
    public Text prefabText;

    private Texture2D[] textList;
    private string[] files;
    private int pathLength;

    void Start()
    {
        // This is to set the path and clear old links
        string path = (Application.dataPath + "" + AssetPath);
        pathLength = path.Length + 1;
        //files = new string[10];
       
        // This is to get the files and start the load prosess
        files = Directory.GetFiles(path, "*.jpg");
        StartCoroutine(LoadImages());
    }

    private IEnumerator LoadImages()
    {
        textList = new Texture2D[files.Length];
        int dummy = 0;

        // This gets a textures for every file found at that path
        foreach (string file in files)
        {
            string pathTemp = @"file://" + file;
            //This loads the textures into physical memory to manipulate "use"
            WWW www = new WWW(pathTemp);
            //This allows the textures to load before being manipulated to prevent null errors
            yield return www;
            //Changes texture size and format and loads textures to array
            Texture2D texTmp = new Texture2D(1024, 1024, TextureFormat.DXT1, false);
            www.LoadImageIntoTexture(texTmp);
            //Indexes array of textures
            textList[dummy] = texTmp;
            //Creates Images to add the textures to
            var ImageClone = Instantiate(prefab);
            //Says where the image should be created
            ImageClone.transform.SetParent(parentPanel.transform);
            ImageClone.transform.localScale = new Vector3(1F, 1F, 1F);
            //Adding the indevidual textures to the individually created images
            ImageClone.texture = textList[dummy];
            
            dummy++;
        }
        //This is to load the namese of the images
        foreach (string file in files)
        {
            var cloneNames = GameObject.FindGameObjectsWithTag("Clones");
            for (var i = 0; i < cloneNames.Length; i++)
               // prefabText.text = file.Substring(pathLength, (file.Length - (pathLength + 4)));
                cloneNames[i].GetComponentInChildren<Text>().text = files[i].Substring(pathLength, (files[i].Length - (pathLength + 4)));
        }


    }
}