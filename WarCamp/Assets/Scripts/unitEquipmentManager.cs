﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class unitEquipmentManager : MonoBehaviour
{

	public Text unitName;
	public GameObject equipButton;

	[Header ("Unit Stats")]
	public Text gold;
	public Text lumber;
	public Text HPText;
	public Text damageText;
	public Text rangeText;
	public Text moveSpeedText;
	public Text attackSpeedText;
	public Text blurb;
	public RawImage rank;

	[Header ("Canvas")]
	public GameObject scrollView;
	public GameObject itemPrefab;
	public GameObject seperatorPrefab;

	private unit RTUnit;
	private GameObject selectedWeapon;
	private GameObject selectedArmour;

	private equipmentListItemPrefab selectedWeaponListItem;
	private equipmentListItemPrefab selectedArmourListItem;

	private matchCoordinator matchInstance;
	private matchCanvasManager sceneManager;

	private List<GameObject> scrollViewItems = new List<GameObject> ();

	void Start ()
	{
		sceneManager = GameObject.Find ("Canvas").GetComponent<matchCanvasManager> ();
		matchInstance = GameObject.Find ("MatchCoordinator").GetComponent<matchCoordinator> ();
	}

	public void setupCanvas (unit RTUnit)
	{

		if (this.RTUnit != null) {
			cleanUp ();
			this.RTUnit = null;
		}

		this.RTUnit = RTUnit;

		unitName.text = (RTUnit.getUnitName ().ToUpper () + " - level " + RTUnit.getUnitLevel () + "").ToString ();
		equipButton.SetActive (false);

		equipButton.GetComponent<Button> ().onClick.AddListener (() => {

			sceneManager.switchToCanvas (gameObject); // exit the current canvas
			setNewEquipment (RTUnit, selectedWeapon, selectedArmour);

		});

		setupUnitStats (RTUnit.creepStats);
		setupScrollView (RTUnit);

	}

	private void setupUnitStats (creepStatistics RTUnit)
	{
		gold.text = RTUnit.goldCost.ToString ();
		lumber.text = RTUnit.lumberCost.ToString ();

		HPText.text = RTUnit.hp.ToString ();
		damageText.text = RTUnit.damage.ToString ();
		rangeText.text = RTUnit.range.ToString ();
		moveSpeedText.text = RTUnit.moveSpeed.ToString ();
		attackSpeedText.text = RTUnit.moveSpeed.ToString ();
	}

	private void setupScrollView (unit RTUnit)
	{

		this.selectedArmour = RTUnit.getSelectedArmour ();
		this.selectedWeapon = RTUnit.getSelectedWeapon ();

		var offset = 0;

		foreach (GameObject armour in RTUnit.getAvailableArmour()) {

			if (armour.GetComponent<equipment> ().level != 0) {
				offset++;
			}

		}

		foreach (GameObject weapon in RTUnit.getAvailableWeapons()) {

			if (weapon.GetComponent<equipment> ().level != 0) {
				offset++;
			}

		}

		if (RTUnit.getAvailableArmour () != null) {
			setupItemList (RTUnit.getAvailableArmour (), this.selectedArmour, 0, false);	
			placeSeperator (offset);
		}

		if (RTUnit.getAvailableWeapons () != null) {
			setupItemList (RTUnit.getAvailableWeapons (), this.selectedWeapon, offset, true);	
		}

		resizeScrollView (offset);

	}

	private void resizeScrollView (int items)
	{

		float requiredHeight = items * itemPrefab.GetComponent<RectTransform> ().sizeDelta.y;

		if (requiredHeight > scrollView.GetComponent<RectTransform> ().sizeDelta.y) {

			scrollView.GetComponent<RectTransform> ().sizeDelta = new Vector2 (scrollView.GetComponent<RectTransform> ().sizeDelta.x, requiredHeight);

		}

	}

	private void placeSeperator (float offset)
	{
		GameObject seperator = Instantiate (seperatorPrefab);
		scrollViewItems.Add (seperator);
		seperator.GetComponent<RectTransform> ().position = new Vector3 (0, itemPrefab.GetComponent<RectTransform> ().sizeDelta.y * -offset, 0);
		seperator.transform.SetParent (scrollView.transform, false);
	}

	private void setupItemList (GameObject[] items, GameObject selectedItem, float offset, bool isWeapon)
	{
		
		for (int i = 0; i < items.Length; i++) {

			if (items [i].GetComponent<equipment> ().level == 0) {
				return;
			}

			GameObject listItem = Instantiate (itemPrefab);
			scrollViewItems.Add (listItem);
			listItem.GetComponent<equipmentListItemPrefab> ().setupLabel (items [i], this, isWeapon);

			if (items [i].GetComponent<equipment> ().equipmentName.Equals (selectedItem.GetComponent<equipment> ().equipmentName)) {

				listItem.GetComponent<equipmentListItemPrefab> ().setAsSelected ();

				if (isWeapon) {
					selectedWeaponListItem = listItem.GetComponent<equipmentListItemPrefab> ();
				} else {
					selectedArmourListItem = listItem.GetComponent<equipmentListItemPrefab> ();
				}



			} else {
				listItem.GetComponent<equipmentListItemPrefab> ().setAsUnselected ();
			}

			listItem.transform.localPosition = new Vector3 (0, itemPrefab.GetComponent<RectTransform> ().sizeDelta.y * -i + -offset * itemPrefab.GetComponent<RectTransform> ().sizeDelta.y, 0);

			listItem.transform.SetParent (scrollView.transform, false);

		}
	}

	public void setArmourRequest (GameObject item, equipmentListItemPrefab reference)
	{
	
		if (matchInstance.getDraftIsLocked ()) {
			return;
		}

		equipButton.SetActive (true);
			
		selectedArmourListItem.setAsUnselected ();
		selectedArmourListItem = reference;
		selectedArmourListItem.setAsSelected ();
			
		creepStatistics newStats = new creepStatistics ();

		RTUnit.setupBaseStats (RTUnit.level, newStats, RTUnit.type); // set the new stats for the unit
		RTUnit.applyEquipmentStats (selectedWeapon, newStats);
		RTUnit.applyEquipmentStats (item, newStats);

		selectedArmour = item;

		setupUnitStats (newStats);

	}

	public void setWeaponRequest (GameObject item, equipmentListItemPrefab reference)
	{

		if (matchInstance.getDraftIsLocked ()) {
			return;
		}
			
		equipButton.SetActive (true);
			
		selectedWeaponListItem.setAsUnselected ();
		selectedWeaponListItem = reference;
		selectedWeaponListItem.setAsSelected ();

		creepStatistics newStats = new creepStatistics ();

		RTUnit.setupBaseStats (RTUnit.level, newStats, RTUnit.type); // set the new stats for the unit
		RTUnit.applyEquipmentStats (selectedArmour, newStats);
		RTUnit.applyEquipmentStats (item, newStats);

		selectedWeapon = item;

		setupUnitStats (newStats);

	}

	private void setNewEquipment (unit RTUnit, GameObject weapon, GameObject armour)
	{

		matchInstance.requestUnitEquipmentChange (RTUnit, weapon, armour);

	}

	private void cleanUp ()
	{
		equipButton.GetComponent<Button> ().onClick.RemoveAllListeners ();

		foreach (GameObject item in scrollViewItems) {
			Destroy (item);
		}

		this.RTUnit = null;

	}

	void OnDestroy ()
	{

		cleanUp ();

	}

}
