
﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class matchCoordinator : MonoBehaviour
{

	public int gold;
	public int lumber;

	private matchCanvasManager sceneCanvasManager;
	private List<unit> draftPool = new List<unit> ();
	private Dictionary<int, GameObject> activeUnits = new Dictionary<int, GameObject> ();
	private Transform allUnits;
	/// <summary>
	/// UnitID:BuildSiteID dictionary
	/// </summary>
	private Dictionary<int, int> structureIDS = new Dictionary<int, int> ();

	private int unitIDCounter = 0;
	private bool draftIsLocked = false;

	void Start ()
	{
		GameManager.getInstance ().setMatchCoordinator (this);
		allUnits = GameObject.Find ("allUnits").GetComponent<Transform> ();
		sceneCanvasManager = GameObject.Find ("Canvas").GetComponent<matchCanvasManager> ();
	}

	#region requests

	public void cancelStructure(int unitID){

		if (structureIDS[unitID] == null){ // check that there is a buildSite attached to this unitID
			return;
		}

		GameManager.getInstance ().cancelStructure (structureIDS[unitID]);

	}

	public void requestUnitDraft (unit _unit)
	{


		requestUnitDraft (_unit.type);

	}

	public void requestUnitDraft (creepType type)
	{
		unit unitRT = profileManager.getInstance ().getLocalPlayerRT ().getUnitOfType (type).GetComponent<unit> ();

		if (gold >= unitRT.getGoldCost () && lumber >= unitRT.getLumberCost ()) {
			gold -= unitRT.getGoldCost (); // resource cost deducted now to reduce wasteful input; server will send new resources for update.
			lumber -= unitRT.getLumberCost ();
			draftPool.Add (unitRT);
			GameManager.getInstance ().draftUnit (unitRT.getUnitName ());	
		} 
	}

	public void requestRemoveUnitDraft (unit _unit)
	{
		draftPool.Add (_unit);
		GameManager.getInstance ().removeUnitDraft (_unit.getUnitName ());
	}

	public void requestDraftLockIn ()
	{
		if (draftIsLocked) {
			return;
		}

		GameManager.getInstance ().lockInDraft ();
	}

	public void requestUnitEquipmentChange (unit _unit, GameObject weapon, GameObject armour)
	{

		GameManager.getInstance ().changeUnitEquipmentRT (_unit.getUnitName (), weapon.GetComponent<equipment> ().equipmentName, armour.GetComponent<equipment> ().equipmentName);

	}

	public void requestBuildStructure (int siteNumber, unit unitObj)
	{

		if (gold >= unitObj.getGoldCost () && lumber >= unitObj.getLumberCost ()) {

			gold -= unitObj.getGoldCost ();
			lumber -= unitObj.getLumberCost ();

			GameManager.getInstance ().buildStructure (siteNumber, unitObj.getUnitName ());	


		}

	}

	#endregion

	#region responses

	public void removeUnits (int[] unitIDS)
	{
		foreach (int id in unitIDS) {

			removeUnit (id);

		}
	}

	public void removeUnit(int id){



		if (structureIDS.ContainsKey (id)) {
			sceneCanvasManager.setHasStructure (structureIDS [id], false);
			sceneCanvasManager.removeSiteIcon (structureIDS [id]);
			structureIDS.Remove (id);
		}

		Destroy (activeUnits [id]);
	}

	public void gameOver (int winnerID, int peerGoldGain, int peerLumberGain)
	{
		sceneCanvasManager.gameOver (winnerID, peerGoldGain, peerLumberGain);
	}

	public void structureComplete (int peerID, int unitID, string structureCode)
	{
		
		activeUnits [unitID].GetComponent<unit> ().setUnitToType (peerID, structureCode);
		sceneCanvasManager.setSiteIcon (peerID, structureIDS [unitID], structureCode);

	}

	public void responseUnitEquipmentChange (string unitCode, string weaponCode, string armourCode, int goldChange, int lumberChange)
	{

		unit _unit = profileManager.getInstance ().getLocalPlayerRT ().updateUnitEquipment (unitCode, weaponCode, armourCode);
		sceneCanvasManager.displayResourceNotification (goldChange, lumberChange);
		sceneCanvasManager.reloadUnitDraftManager (_unit);

	}

	public void manageUnitPacket (string[] unitPacket)
	{

		if (unitIDCounter < getUnitID (unitPacket)) {

			var newUnit = spawnUnit (unitPacket);

			if (newUnit.GetComponent<unit> ().getIsBuildable ()) {
				structureIDS.Add (getUnitID (unitPacket), getStructureBuildSite (unitPacket));
				sceneCanvasManager.setHasStructure (getStructureBuildSite (unitPacket), true);
			}

			print ("spawning: " + getUnitCode(unitPacket));
			activeUnits.Add (getUnitID (unitPacket), newUnit);

			unitIDCounter++;

		} else {

			print ("updating unit");
			activeUnits [getUnitID (unitPacket)].GetComponent<unit> ().updateUnit (unitPacket, this);

		}

	}

	public GameObject spawnUnit (string[] unitPacket)
	{

		print ("spawning unit");

		float x = getUnitX (unitPacket);
		float y = getUnitY (unitPacket);
		float z = getUnitZ (unitPacket);
		string unitType = getUnitCode (unitPacket);
		int unitID = getUnitID (unitPacket);

		GameObject newUnit = (GameObject)Instantiate (profileManager.getInstance ().getPeerProfile (getUnitPeerID (unitPacket)).getUnitOfType (unitType));

		newUnit.GetComponent<unit> ().setGUID(unitID);

		newUnit.transform.SetParent (allUnits);

		newUnit.transform.position = new Vector3 (x, y, z);

		return newUnit;

	}

	public void updatePeerDraft (GameSparks.RT.RTPacket packet)
	{

		uint packetCount = 2;
		playerProfile peerProfile = profileManager.getInstance ().getPeerProfile ((int)packet.Data.GetInt (1));

		foreach (GameObject unit in profileManager.getInstance().getAllUnits()) {
		
			if (packet.Data.GetString (packetCount) != null) {

				unit _unit = peerProfile.getUnitOfType (packet.Data.GetString (packetCount++)).GetComponent<unit> ();
				sceneCanvasManager.updatePeerDraft (_unit, (int)packet.Data.GetInt (packetCount++));

			} else {
				break;
			}
				
		}

	}


	public void updateUnknownDraft (int count)
	{

		sceneCanvasManager.setUnknownPeerDraft (count);

	}

	public void setResources (int gold, int lumber)
	{
		sceneCanvasManager.setResources (gold, lumber);
		this.gold = gold;
		this.lumber = lumber;
	}

	public void setGold (int gold)
	{
		this.gold = gold;
	}

	public void setLumber (int lumber)
	{
		this.lumber = lumber;
	}

	public void startSpawnTimer (int spawnRate)
	{
		sceneCanvasManager.startSpawnTimer (spawnRate);
	}

	public void increaseLocalDraft (string unitCode)
	{

		foreach (unit _unit in draftPool) {

			if (_unit.getUnitName ().Equals (unitCode)) {
				draftPool.Remove (_unit);
				sceneCanvasManager.increaseLocalDraft (_unit);
				break;
			}

		}

	}

	public void decreaseLocalDraft (string unitCode)
	{

		foreach (unit _unit in draftPool) {

			if (_unit.getUnitName ().Equals (unitCode)) {

				draftPool.Remove (_unit);
				sceneCanvasManager.decreaseLocalDraft (_unit);
				break;

			}

		}

	}

	public void displayLockIn (int gold, int lumber)
	{
		sceneCanvasManager.displayLockIn (gold, lumber);
	}

	public void resetDrafts ()
	{
		draftPool.Clear ();
		sceneCanvasManager.resetDrafts ();
	}

	public void assignBuildSite (int peerID, int buildSite)
	{

		sceneCanvasManager.assignBuildSite (peerID, buildSite);

	}

	public void setHasStructure(int buildSite, bool hasStructure){

		sceneCanvasManager.setHasStructure (buildSite, hasStructure);

	}

	public void removeSiteIcon(int buildSite){
		sceneCanvasManager.removeSiteIcon (buildSite);
	}

	#endregion

	#region unitPacket get / set

	public bool getDraftIsLocked ()
	{
		return draftIsLocked;
	}

	public int getUnitPeerID (string[] unitPacket)
	{
		return int.Parse (unitPacket [0]);
	}

	public int getUnitID (string[] unitPacket)
	{
		return int.Parse (unitPacket [1]);
	}

	public float getUnitX (string[] unitPacket)
	{
		return float.Parse (unitPacket [5]);
	}

	public float getUnitY (string[] unitPacket)
	{
		return float.Parse (unitPacket [6]);
	}

	public float getUnitZ (string[] unitPacket)
	{
		return float.Parse (unitPacket [7]);
	}

	public string getUnitCode (string[] unitPacket)
	{
		return unitPacket [8];
	}

	public int getStructureBuildSite (string[] unitPacket)
	{
		return int.Parse (unitPacket [3]);
	}

	#endregion
}
