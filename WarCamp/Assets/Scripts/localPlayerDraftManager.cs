﻿using UnityEngine;
using System.Collections;

public class localPlayerDraftManager : MonoBehaviour
{

	public GameObject[] panels;

	public void increaseUnitCount (unit _unit)
	{

		switch (_unit.type) {
		case creepType.ARCHER:
			panels [0].GetComponent<localPlayerDraft> ().increaseCount ();
			break;
		case creepType.CATAPULT:
			panels [1].GetComponent<localPlayerDraft> ().increaseCount ();
			break;
		case creepType.CAVALRY:
			panels [2].GetComponent<localPlayerDraft> ().increaseCount ();
			break;
		case creepType.HEALER:
			panels [3].GetComponent<localPlayerDraft> ().increaseCount ();
			break;
		case creepType.MONK:
			panels [4].GetComponent<localPlayerDraft> ().increaseCount ();
			break;
		case creepType.WARRIOR:
			panels [5].GetComponent<localPlayerDraft> ().increaseCount ();
			break;
		case creepType.WIZARD:
			panels [6].GetComponent<localPlayerDraft> ().increaseCount ();
			break;
		case creepType.ZOMBIE:
			panels [7].GetComponent<localPlayerDraft> ().increaseCount ();
			break;
		}

	}

	public void decreaseUnitCount (unit _unit)
	{
		switch (_unit.type) {
		case creepType.ARCHER:
			panels [0].GetComponent<localPlayerDraft> ().decreaseCount ();
			break;
		case creepType.CATAPULT:
			panels [1].GetComponent<localPlayerDraft> ().decreaseCount ();
			break;
		case creepType.CAVALRY:
			panels [2].GetComponent<localPlayerDraft> ().decreaseCount ();
			break;
		case creepType.HEALER:
			panels [3].GetComponent<localPlayerDraft> ().decreaseCount ();
			break;
		case creepType.MONK:
			panels [4].GetComponent<localPlayerDraft> ().decreaseCount ();
			break;
		case creepType.WARRIOR:
			panels [5].GetComponent<localPlayerDraft> ().decreaseCount ();
			break;
		case creepType.WIZARD:
			panels [6].GetComponent<localPlayerDraft> ().decreaseCount ();
			break;
		case creepType.ZOMBIE:
			panels [7].GetComponent<localPlayerDraft> ().decreaseCount ();
			break;
		}
	}

	public void reset ()
	{
		foreach (GameObject panel in panels) {
			panel.GetComponent<localPlayerDraft> ().reset ();
		}
	}

}
