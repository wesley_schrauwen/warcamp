﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using GameSparks.Api.Responses;

public class createAccount : MonoBehaviour
{
	public InputField email;
	public InputField password;
	public InputField confirmPassword;
	public InputField displayName;

	public Text errorText;

	public Button createUser;

	private loginSceneManager loginScene;

	// Use this for initialization
	void Start ()
	{
		loginScene = GameObject.Find ("LoginMainSceneCanvas").GetComponent<loginSceneManager> ();
		createUser.onClick.AddListener (createUserAccount);
		errorText.gameObject.SetActive (false);
	}

	void createUserAccount ()
	{
		errorText.text = "";
		errorText.gameObject.SetActive (false);

		if (email.text.Equals ("") ||
		    password.text.Equals ("") ||
		    confirmPassword.text.Equals ("") ||
		    displayName.text.Equals ("")) {

			errorText.text = "Form Incomplete.";
			errorText.gameObject.SetActive (true);
			return;
		}

		if (!password.text.Equals (confirmPassword.text)) {
			errorText.text = "Passwords dont match.";
			errorText.gameObject.SetActive (true);
			return;
		}

		if (!isValid.email (email.text)) {
			errorText.text = "Invalid Email.";
			errorText.gameObject.SetActive (true);
			return;
		}

		GameManager.getInstance ().createAccount (displayName.text, email.text, password.text);	
			
	}

	void Destroy ()
	{
		createUser.onClick.RemoveAllListeners ();
	}

}
