﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class successPanelManager : MonoBehaviour
{

	public Text successText;

	public void accountCreatedText ()
	{
		successText.text = "Account Created. Please try logging in.";
	}

	public void passwordResetText ()
	{
		successText.text = "Password Reset Successful. Please check your email.";
	}

	public void timeoutError ()
	{
		successText.text = "A Timeout error occurred.";
	}

	public void unknownError ()
	{
		successText.text = "An unknown error occurred, please try again.";
	}
		
}
