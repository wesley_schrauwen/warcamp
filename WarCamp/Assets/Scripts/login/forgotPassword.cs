﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

public class forgotPassword : MonoBehaviour
{

	public GameObject errorPanel;
	public Button resetButton;
	public InputField emailAddress;

	void Start ()
	{
		errorPanel.SetActive (false);
		resetButton.onClick.AddListener (initResetPassword);
	}

	void initResetPassword ()
	{

		if (isValid.email (emailAddress.text)) {

			GameManager.getInstance ().resetPassword (emailAddress.text);

		} else {

			errorPanel.SetActive (true);

		}
			
	}

	void Destroy ()
	{
		resetButton.onClick.RemoveAllListeners ();
	}

}
