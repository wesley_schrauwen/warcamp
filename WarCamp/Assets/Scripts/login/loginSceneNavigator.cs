﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class loginSceneNavigator : MonoBehaviour
{

	public GameObject navigateToCanvas;

	private loginSceneManager sceneManagerReference;
	// Use this for initialization
	void Start ()
	{

		sceneManagerReference = GameObject.Find ("LoginMainSceneCanvas").GetComponent<loginSceneManager> ();

		gameObject.GetComponent<Button> ().onClick.AddListener (() => {

			sceneManagerReference.toggleActivePanel (navigateToCanvas);

		});

	}

	void Destroy ()
	{
		gameObject.GetComponent<Button> ().onClick.RemoveAllListeners ();
	}
}
