﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class loginPanel : MonoBehaviour
{
	public InputField email;
	public InputField password;
	public Button loginButton;


	private loginSceneManager loginSceneCanvas;

	// Use this for initialization
	void Start ()
	{
		loginSceneCanvas = GameObject.Find ("LoginMainSceneCanvas").GetComponent<loginSceneManager> ();

		loginButton.onClick.AddListener (() => {
			GameManager.getInstance ().attemptAuth (password.text, email.text);
		});

    }
	
	// Update is called once per frame
	void Update ()
	{
	
	}
		
}
