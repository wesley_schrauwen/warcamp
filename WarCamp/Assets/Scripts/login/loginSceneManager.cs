﻿using UnityEngine;
using System.Collections;

public class loginSceneManager : MonoBehaviour
{

	public GameObject loginPanel;
	public GameObject createAccountPanel;
	public GameObject resetPasswordPanel;
	public GameObject successPanel;

	private GameObject currentPanel;

	void Start ()
	{
		
		loginPanel.SetActive (true);
		createAccountPanel.SetActive (false);
		resetPasswordPanel.SetActive (false);
		successPanel.SetActive (false);

		currentPanel = loginPanel;
	}

	public void toggleActivePanel (GameObject panel)
	{
		currentPanel.SetActive (false);
		currentPanel = panel;
		currentPanel.SetActive (true);
	}

	public void returnToLoginPanel ()
	{
		currentPanel.SetActive (false);
		currentPanel = loginPanel;
		currentPanel.SetActive (true);
	}

	public void displaySuccessPanel ()
	{
		successPanel.SetActive (true);
	}

	public void displayCreateAccountError ()
	{
		
	}

}
