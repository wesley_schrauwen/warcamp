﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class lockInButton : MonoBehaviour
{

	private matchCanvasManager sceneManager;


	void Start ()
	{
		sceneManager = GameObject.Find ("Canvas").GetComponent<matchCanvasManager> ();

		gameObject.GetComponent<Button> ().onClick.AddListener (() => {

			sceneManager.lockInDraft ();

		});

	}

	void onDestroy ()
	{
		gameObject.GetComponent<Button> ().onClick.RemoveAllListeners ();
	}

}
