﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class upgradeManager : MonoBehaviour
{

	[Header ("Details Page")]
	public Text itemName;
	public Text upgradedGoldCost;
	public Text upgradedLumberCost;
	public Text upgradedHP;
	public Text upgradedDamage;
	public Text upgradedAttackSpeed;
	public Text upgradedMoveSpeed;
	public Text upgradedRange;

	[Header ("Upgrade Page")]
	public Text goldUpgradeCost;
	public Text lumberUpgradeCost;
	public Text upgradeTimeText;
	public Text upgradeText;
	public Button buyButton;
	public Button upgradeButton;

	[Header ("Is Upgrading")]
	public GameObject goldPanel;
	public GameObject lumberPanel;
	public GameObject upgradeTickerPanel;
	public GameObject gemPanel;

	private equipment equipmentItem;
	private unit unitItem;
	private bool isDisplayingUnit = false;
	private capitalCanvasManager sceneManager;

	void Start ()
	{
		sceneManager = GameObject.Find ("Canvas").GetComponent<capitalCanvasManager> ();
	}

	public void displayUpgrade (equipment item)
	{

		this.equipmentItem = item;
		isDisplayingUnit = false;
		equipmentStats upgradedStats = new equipmentStats ();

		int upgradedLevel = item.level < item.maxLevel ? item.level + 1 : item.maxLevel;

		item.setStats (upgradedLevel, upgradedStats, item.getType ());

		displayUpgradePage (item.upgrade, item.level, item.maxLevel);

		itemName.text = item.equipmentName.ToUpper () + " LEVEL " + upgradedLevel.ToString ();
		upgradedGoldCost.text = item.stats.goldCostCH + " + " + (upgradedStats.goldCostCH - item.stats.goldCostCH);
		upgradedLumberCost.text = item.stats.lumberCostCH + " + " + (upgradedStats.lumberCostCH - item.stats.lumberCostCH);
		upgradedHP.text = item.stats.hpCH + " + " + (upgradedStats.hpCH - item.stats.hpCH);
		upgradedDamage.text = item.stats.damageCH + " + " + (upgradedStats.damageCH - item.stats.damageCH);
		upgradedAttackSpeed.text = item.stats.attackSpeedCH + " + " + (upgradedStats.attackSpeedCH - item.stats.attackSpeedCH);
		upgradedMoveSpeed.text = item.stats.moveSpeedCH + " + " + (upgradedStats.moveSpeedCH - item.stats.moveSpeedCH);
		upgradedRange.text = item.stats.rangeCH + " + " + (upgradedStats.rangeCH - item.stats.rangeCH);

		setupButtons (item.getItemGroup (), item.equipmentName);

	}

	public void displayUpgrade (unit item)
	{
		this.unitItem = item;
		isDisplayingUnit = true;
		creepStatistics upgradedStats = new creepStatistics ();

		int upgradedLevel = item.level < item.maxLevel ? item.level + 1 : item.maxLevel;

		item.setupBaseStats (upgradedLevel, upgradedStats, item.type);

		displayUpgradePage (item.upgrade, item.level, item.maxLevel);

		itemName.text = item.getUnitName ().ToUpper () + " LEVEL " + upgradedLevel.ToString ();
		upgradedGoldCost.text = item.creepStats.goldCost + " + " + (upgradedStats.goldCost - item.creepStats.goldCost);
		upgradedLumberCost.text = item.creepStats.lumberCost + " + " + (upgradedStats.lumberCost - item.creepStats.lumberCost);
		upgradedHP.text = item.creepStats.hp + " + " + (upgradedStats.hp - item.creepStats.hp);
		upgradedDamage.text = item.creepStats.damage + " + " + (upgradedStats.damage - item.creepStats.damage);
		upgradedAttackSpeed.text = item.creepStats.attackSpeed + " + " + (upgradedStats.attackSpeed - item.creepStats.attackSpeed);
		upgradedMoveSpeed.text = item.creepStats.moveSpeed + " + " + (upgradedStats.moveSpeed - item.creepStats.moveSpeed);
		upgradedRange.text = item.creepStats.range + " + " + (upgradedStats.range - item.creepStats.range);

		setupButtons (item.getItemGroup (), item.getUnitName ());

	}

	public void reloadUI ()
	{
		if (isDisplayingUnit)
			displayUpgrade (unitItem);
		else
			displayUpgrade (equipmentItem);
	}

	private void setupButtons (string groupType, string itemName)
	{

		upgradeButton.onClick.AddListener (() => {

			int playerGold = profileManager.getInstance ().getLocalPlayer ().getGold ();
			int playerLumber = profileManager.getInstance ().getLocalPlayer ().getLumber ();

			if (isDisplayingUnit) {

				if (playerGold > unitItem.upgrade.upgradeGoldCost && playerLumber > unitItem.upgrade.upgradeLumberCost) {

					sceneManager.requestUpgrade (groupType, itemName);

				} else {
					
					displayErrorInsufficientResources ();

				}

			} else {

				if (playerGold > equipmentItem.upgrade.upgradeGoldCost && playerLumber > equipmentItem.upgrade.upgradeLumberCost) {

					sceneManager.requestUpgrade (groupType, itemName);

				} else {
					
					displayErrorInsufficientResources ();

				}

			}



		});

		buyButton.onClick.AddListener (() => {

			//TODO:
			/*
             * this will need to call some function that will buy the item using gems or pass the user to a screen to buy gems.
             * 
             */

		});
	}

	public void displayErrorInsufficientResources ()
	{
		sceneManager.displayInsufficientResources ();
	}

	public void displayUpgradePage (upgradeable upgrade, int level, int maxLevel)
	{

		if (upgrade.isUpgrading && upgrade.upgradeComplete < (ulong)Date.now) {

			goldPanel.SetActive (false);
			lumberPanel.SetActive (false);
			upgradeTickerPanel.SetActive (true);
			upgradeButton.gameObject.SetActive (false);
			buyButton.gameObject.SetActive (true);
			gemPanel.SetActive (true);
			this.upgradeTickerPanel.GetComponent<upgradeTickerPanel> ().startTicking (upgrade.upgradeBegin, upgrade.upgradeComplete);
			upgradeTimeText.text = upgrade.upgradeGemCost.ToString () + " gems.";
			upgradeText.text = "UPGRADING...";

		} else if (level == maxLevel) {

			goldPanel.SetActive (false);
			lumberPanel.SetActive (false);
			upgradeTickerPanel.SetActive (false);
			buyButton.gameObject.SetActive (false);
			upgradeButton.gameObject.SetActive (false);
			gemPanel.SetActive (false);
			upgradeText.text = "MAX LEVEL";

		} else {

			upgradeText.text = "UPGRADE";
			goldPanel.SetActive (true);
			lumberPanel.SetActive (true);
			upgradeTickerPanel.SetActive (false);
			upgradeButton.gameObject.SetActive (true);
			gemPanel.SetActive (true);
			buyButton.gameObject.SetActive (true);
			goldUpgradeCost.text = upgrade.upgradeGoldCost.ToString ();
			lumberUpgradeCost.text = upgrade.upgradeLumberCost.ToString ();
			upgradeTimeText.text = upgrade.upgradeTime.ToString () + " or " + upgrade.upgradeGemCost.ToString () + " gems.";

		}

		
	}

	void OnDisable ()
	{
		upgradeButton.onClick.RemoveAllListeners ();
	}

}
