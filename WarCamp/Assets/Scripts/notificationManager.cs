﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class notificationManager : MonoBehaviour
{

	[Header ("Canvas")]
	public GameObject unitLockIn;
	public GameObject notificationView;

	[Header ("Prefab")]
	public GameObject goldGainPrefab;
	public GameObject lumberGainPrefab;

	[Header ("Timer")]
	[Tooltip ("Time to Destruction in Seconds")]
	public float notificationTimer = 1.5f;

	private Queue<GameObject> resourceQueue = new Queue <GameObject> ();

	private GameObject[] displayArray;
	private int spaceForDisplay;
	private bool adjustPositions;

	void Start ()
	{
		displayArray = new GameObject[Mathf.FloorToInt (notificationView.GetComponent<RectTransform> ().sizeDelta.y / goldGainPrefab.GetComponent<RectTransform> ().sizeDelta.y)];
		spaceForDisplay = displayArray.Length;
		adjustPositions = false;

		goldGainPrefab.GetComponent<resourceNotification> ().setTimeToExpire (notificationTimer);
		lumberGainPrefab.GetComponent<resourceNotification> ().setTimeToExpire (notificationTimer);

	}

	void Update ()
	{

		// add in new items if any exist
		if (resourceQueue.Count > 0 && spaceForDisplay > 0) {

//			print (displayArray.Length - spaceForDisplay);
//			print (displayArray.Length);
//			print (resourceQueue.Count);

			for (int i = displayArray.Length - spaceForDisplay; i < displayArray.Length && i < resourceQueue.Count + i; i++) {

				GameObject notification = resourceQueue.Dequeue ();

				print ("i: " + i);

				notification.GetComponent<RectTransform> ().localPosition = new Vector3 (0, notification.GetComponent<RectTransform> ().sizeDelta.y * i, 0);

				print ("position: " + notification.GetComponent<RectTransform> ().position);

				notification.SetActive (true);

				displayArray [i] = notification;

				spaceForDisplay--;

			}
		}

		for (int i = 0; i < displayArray.Length; i++) {

			if (displayArray [i] == null) {
				break;
			} 

			displayArray [i].GetComponent<resourceNotification> ().decreaseTimeToExpire (Time.deltaTime);

			if (displayArray [i].GetComponent<resourceNotification> ().getTimeToExpire () <= 0) {
				Destroy (displayArray [i]);
				displayArray [i] = null;
				spaceForDisplay++;
				adjustPositions = true;
			}
		}

		if (adjustPositions) {

			adjustPositions = false;

			for (int i = 0; i < displayArray.Length; i++) {

				if (displayArray [i] == null) {
					break;
				}

				displayArray [i].transform.localPosition = new Vector3 (0, displayArray [i].GetComponent<RectTransform> ().sizeDelta.y * i, 0);
				
			}
		}

	}

	public void displayLockIn (int gold, int lumber)
	{
		displayLockIn ();
		displayGoldGain (gold);
		displayLumberGain (lumber);
	}

	public void displayResourceNotification (int gold, int lumber)
	{
		displayGoldGain (gold);
		displayLumberGain (lumber);
	}

	public void displayLockIn ()
	{
		unitLockIn.GetComponent<unitLockedInNotification> ().displayLockIn ();
	}

	public void displayGoldGain (int gold)
	{
		if (gold != 0)
			displayGain (goldGainPrefab, gold);

	}

	public void displayLumberGain (int lumber)
	{

		if (lumber != 0)
			displayGain (lumberGainPrefab, lumber);

	}

	public void displayGain (GameObject prefab, int resource)
	{

		GameObject obj = Instantiate (prefab);

		obj.GetComponent<resourceNotification> ().setGain (resource);

		obj.transform.SetParent (notificationView.transform, false);

		obj.SetActive (false);

		resourceQueue.Enqueue (obj);

	}
}
