﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class selectItemManager : MonoBehaviour
{

	public Text itemDescription;
	public Text goldText;
	public Text lumberText;
	public Text hpText;
	public Text damageText;
	public Text moveSpeedText;
	public Text attackSpeedText;
	public Text rangeText;

	public Button equipButton;

	public GameObject scrollView;

	public GameObject prefab;

	private GameObject[] scrollViewList;

	private equipmentListButton currentlySelectedButton = null;
	private equipment currentItem = null;
	private string itemType;
	private unit _unit;

	public void setupCanvas (unit _unit, string itemType)
	{

		if (currentlySelectedButton != null) {
			cleanUp ();
		}

		this.itemType = itemType;
		this._unit = _unit;

		GameObject[] availableItems;
		GameObject selectedItem;

		if (itemType.Equals ("armour")) {
			availableItems = _unit.getAvailableArmour ();
			selectedItem = _unit.getSelectedArmour ();
		} else {
			availableItems = _unit.getAvailableWeapons ();
			selectedItem = _unit.getSelectedWeapon ();
		}

		scrollViewList = new GameObject[availableItems.Length];
	
		itemDescription.text = selectedItem.GetComponent<equipment> ().itemDescription;

		for (int i = 0; i < availableItems.Length; i++) {
			
			var item = (GameObject)Instantiate (prefab);


			item.transform.position = new Vector3 (item.transform.position.x, -item.GetComponent<RectTransform> ().sizeDelta.y * i, 0);
			item.transform.SetParent (scrollView.transform, false);

			item.GetComponent<equipmentListButton> ().setupObject (this, availableItems [i].GetComponent<equipment> ());

			if (availableItems [i].name.Equals (selectedItem.name)) {

				displayItem (selectedItem.GetComponent<equipment> (), item.GetComponent<equipmentListButton> ());

			}

			scrollViewList [i] = item;

		}

		equipButton.onClick.AddListener (() => {
			
			GameManager.getInstance ().changeUnitEquipmentRequest (itemType, currentItem.equipmentName, _unit);

		});

	}

	public void displayItem (equipment selectedItem, equipmentListButton buttonReference)
	{

		if (currentlySelectedButton != null) {
			currentlySelectedButton.isNotSelected ();
		}

		currentItem = selectedItem;
		currentlySelectedButton = buttonReference;
		currentlySelectedButton.isSelected ();

		itemDescription.text = selectedItem.itemDescription;
		goldText.text = selectedItem.stats.goldCostCH.ToString ();
		lumberText.text = selectedItem.stats.lumberCostCH.ToString ();
		hpText.text = selectedItem.stats.hpCH.ToString ();
		damageText.text = selectedItem.stats.damageCH.ToString ();
		moveSpeedText.text = selectedItem.stats.moveSpeedCH.ToString ();
		attackSpeedText.text = selectedItem.stats.attackSpeedCH.ToString ();
		rangeText.text = selectedItem.stats.rangeCH.ToString ();

	}

	public void cleanUp ()
	{
		foreach (GameObject item in scrollViewList) {
			Destroy (item);
		}

		scrollViewList = null;
		currentlySelectedButton = null;
		currentItem = null;
		_unit = null;
		equipButton.onClick.RemoveAllListeners ();
	}


	void OnDisable ()
	{
		cleanUp ();
	}
}
