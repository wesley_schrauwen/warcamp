﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class peerDraft : MonoBehaviour
{

	public Text unitCount;
	public RawImage card;
	public GameObject prefab = null;

	private int _unitCount;

	void Awake ()
	{
		unitCount.text = "0";
		_unitCount = 0;
	}

	void Start ()
	{
		
		if (prefab != null) {
			card.texture = prefab.GetComponent<unit> ().getCard ();	
		}

		gameObject.SetActive (false);

	}

	public void setUnitCount (int count)
	{

//		print ("display canvas with count: " + count);

		gameObject.SetActive (count > 0);

		_unitCount = count;
		unitCount.text = count.ToString ();

	}

	public void decreaseUnitCount (int decrement)
	{
		_unitCount -= decrement;
		setUnitCount (_unitCount);
	}

	public void reset ()
	{
		_unitCount = 0;
		unitCount.text = "0";
		gameObject.SetActive (false);
	}

}
