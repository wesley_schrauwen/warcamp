﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class peerDraftManager : MonoBehaviour
{

	public GameObject[] panels;

	private matchCoordinator MatchCoordinator;

	void Start ()
	{
		MatchCoordinator = GameObject.Find ("MatchCoordinator").GetComponent<matchCoordinator> ();
	}

	public void setUnknownUnitCount (int count)
	{

		panels [0].GetComponent<peerDraft> ().setUnitCount (count);

	}

	public void setPeerDraft (unit _unit, int amount)
	{

		panels [0].GetComponent<peerDraft> ().decreaseUnitCount (amount);

		switch (_unit.type) {

		case creepType.ARCHER:
			panels [1].GetComponent<peerDraft> ().setUnitCount (amount);
			break;
		case creepType.CATAPULT:
			panels [2].GetComponent<peerDraft> ().setUnitCount (amount);
			break;
		case creepType.CAVALRY:
			panels [3].GetComponent<peerDraft> ().setUnitCount (amount);
			break;
		case creepType.HEALER:
			panels [4].GetComponent<peerDraft> ().setUnitCount (amount);
			break;
		case creepType.MONK:
			panels [5].GetComponent<peerDraft> ().setUnitCount (amount);
			break;
		case creepType.WARRIOR:
			panels [6].GetComponent<peerDraft> ().setUnitCount (amount);
			break;
		case creepType.WIZARD:
			panels [7].GetComponent<peerDraft> ().setUnitCount (amount);
			break;
		case creepType.ZOMBIE:
			panels [8].GetComponent<peerDraft> ().setUnitCount (amount);
			break;

		}
	}

	public void resetPeerDraft ()
	{
		foreach (GameObject panel in panels) {
			panel.GetComponent<peerDraft> ().reset ();
		}
	}

}
