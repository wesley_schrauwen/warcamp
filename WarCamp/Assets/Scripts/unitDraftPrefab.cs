﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class unitDraftPrefab : MonoBehaviour
{

	public Button addUnit;
	public Button removeUnit;

	public RawImage rank;
	public Text unitName;
	public RawImage weapon;
	public RawImage armour;
	public Text gold;
	public Text lumber;
	public Text unitCount;
	public RawImage card;
	public Button containerButton;

	private unit _unit;

	private unitDraftManager draftManager;
	private matchCanvasManager sceneManager;
	private int _unitCount;

	void Start ()
	{
		draftManager = GameObject.Find ("unitDraftCanvas").GetComponent<unitDraftManager> ();
		sceneManager = GameObject.Find ("Canvas").GetComponent<matchCanvasManager> ();
	}

	public void setupPrefab (unit _unit)
	{
		
		this._unit = _unit;
		setPrefabStats (_unit);

		addUnit.onClick.AddListener (() => {

			draftManager.draftUnit (this._unit);

		});

		removeUnit.onClick.AddListener (() => {

			if (_unitCount > 0) {
				draftManager.removeUnitDraft (this._unit);	
			}
				
		});

		containerButton.onClick.AddListener (() => {

			sceneManager.switchToEquipmentManager (this._unit);

		});

	}

	public void setPrefabStats (unit _unit)
	{
		card.texture = _unit.getCard ();
		gold.text = _unit.getGoldCost ().ToString ();
		lumber.text = _unit.getLumberCost ().ToString ();
		unitName.text = _unit.getUnitName () + " (" + _unit.getUnitLevel () + ")";
		unitCount.text = "0";
	}

	public void reloadPrefab (unit _unit)
	{
		setPrefabStats (_unit);
		this._unit = _unit;
	}

	public int getCount ()
	{
		return _unitCount;
	}

	public void increaseCount ()
	{		
		unitCount.text = (++_unitCount).ToString ();
	}

	public void decreaseCount ()
	{
		unitCount.text = (--_unitCount).ToString ();
	}

	public void resetCount ()
	{
		unitCount.text = "0";
	}

	public creepType getUnitType ()
	{
		return _unit.type;
	}

	void OnDestroy ()
	{
	
		addUnit.onClick.RemoveAllListeners ();
		removeUnit.onClick.RemoveAllListeners ();
		containerButton.onClick.RemoveAllListeners ();

	}

	public creepType getCreepType ()
	{
		return _unit.type;
	}
}
