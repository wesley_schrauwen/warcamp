﻿using UnityEngine;
using System.Collections;

public class moveCamera : MonoBehaviour
{

	[Tooltip ("The camera's move speed")]
	public float moveSpeed = 10f;

	private int peerID = 1;

	private float maxRight;
	private float maxLeft;
	private float maxDown;
	private float maxUp;
	private float maxZoomIn;
	private float maxZoomOut;

	private float vertical = 0;
	private float horizontal = 0;
	private float zoom = 0;

	private float peer1PositiveAdjustment = 1;
	private float peer1NegativeAdjustment = -1;

	void Awake ()
	{
		
		maxRight = 430f;
		maxLeft = 90f;
		maxZoomIn = 80f;
		maxZoomOut = 300f;

	}

	void Start ()
	{

		peerID = GameManager.getInstance ().getClientPeerID ();

		if (peerID == 1) {

			maxDown = -80f;
			maxUp = 280f;
			transform.position = new Vector3 (290, 305, -80);
			transform.eulerAngles = new Vector3 (55, 0, 0);

			peer1PositiveAdjustment = 1;
			peer1NegativeAdjustment = -1;

		} else if (peerID == 2) {
			
			maxDown = 220f;
			maxUp = 580f;

			transform.position = new Vector3 (220, 305, 560);
			transform.eulerAngles = new Vector3 (55, 180, 0);

			peer1PositiveAdjustment = -1;
			peer1NegativeAdjustment = 1;

		}
	}

	void Update ()
	{

		if (Input.GetKey (KeyCode.A) || Input.GetKey (KeyCode.D)) {

			horizontal = Input.GetKey (KeyCode.A) ? peer1NegativeAdjustment : peer1PositiveAdjustment;

		}

		if (Input.GetKey (KeyCode.W) || Input.GetKey (KeyCode.S)) {
			vertical = Input.GetKey (KeyCode.W) ? peer1PositiveAdjustment : peer1NegativeAdjustment;
		}

		if (Input.GetKey (KeyCode.O) || Input.GetKey (KeyCode.I)) {
			zoom = Input.GetKey (KeyCode.O) ? 1 : -1;
		}

		if (vertical != 0 || horizontal != 0 || zoom != 0) {

			float x = horizontal * moveSpeed * Time.deltaTime + transform.position.x;
			float z = vertical * moveSpeed * Time.deltaTime + transform.position.z;
			float y = zoom * moveSpeed * Time.deltaTime + transform.position.y;

			transform.position = new Vector3 (
				Mathf.Clamp (x, maxLeft, maxRight),
				Mathf.Clamp (y, maxZoomIn, maxZoomOut),
				Mathf.Clamp (z, maxDown, maxUp)
			);

			vertical = 0;
			horizontal = 0;
			zoom = 0;

		}
			
	}
}
