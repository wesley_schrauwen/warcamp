﻿using UnityEngine;
using System.Collections;

public class weaponCanvasSideMenu : MonoBehaviour
{

	public GameObject scrollView;
	public GameObject listPrefab;

	private weaponCanvasManager parentReference;

	public void setupCanvas (weaponCanvasManager reference, GameObject[] units)
	{

		this.parentReference = reference;

		for (int i = 0; i < units.Length; i++) {

			GameObject item = Instantiate (listPrefab);

			item.GetComponent<equipmentListButton> ().setupObject (this, units [i].GetComponent<unit> ());

			item.transform.SetParent (scrollView.transform, false);

			item.GetComponent<RectTransform> ().localPosition = new Vector3 (item.GetComponent<RectTransform> ().localPosition.x, item.GetComponent<RectTransform> ().sizeDelta.y * -i, 0);

		}

		resize.scrollView_Vertical (scrollView, listPrefab, units.Length);

	}

	public void unitSelected (unit item)
	{

		parentReference.unitSelected (item);

	}

}
