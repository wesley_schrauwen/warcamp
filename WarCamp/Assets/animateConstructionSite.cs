﻿using UnityEngine;
using System.Collections;

public class animateConstructionSite : MonoBehaviour {

	public int animationSpeed = 1;

	void Start(){
		StartCoroutine ("hammer");
	}

	IEnumerator hammer(){

		for(;;){

			int zRotation = gameObject.transform.localRotation.z == 0 ? 45 : 0;

			gameObject.transform.eulerAngles = new Vector3(0, 0, zRotation);

			yield return new WaitForSeconds (animationSpeed);

		}

	}

	void onDestroy(){

		StopCoroutine ("hammer");

	}

}
