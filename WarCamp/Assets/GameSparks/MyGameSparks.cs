using System;
using System.Collections.Generic;
using GameSparks.Core;
using GameSparks.Api.Requests;
using GameSparks.Api.Responses;

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY!!
//THIS FILE IS AUTO GENERATED, DO NOT MODIFY!!
//THIS FILE IS AUTO GENERATED, DO NOT MODIFY!!

namespace GameSparks.Api.Requests{
	public class LogEventRequest_changeEquipment : GSTypedRequest<LogEventRequest_changeEquipment, LogEventResponse>
	{
	
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogEventResponse (response);
		}
		
		public LogEventRequest_changeEquipment() : base("LogEventRequest"){
			request.AddString("eventKey", "changeEquipment");
		}
		
		public LogEventRequest_changeEquipment Set_item( string value )
		{
			request.AddString("item", value);
			return this;
		}
		
		public LogEventRequest_changeEquipment Set_type( string value )
		{
			request.AddString("type", value);
			return this;
		}
		
		public LogEventRequest_changeEquipment Set_unit( string value )
		{
			request.AddString("unit", value);
			return this;
		}
	}
	
	public class LogChallengeEventRequest_changeEquipment : GSTypedRequest<LogChallengeEventRequest_changeEquipment, LogChallengeEventResponse>
	{
		public LogChallengeEventRequest_changeEquipment() : base("LogChallengeEventRequest"){
			request.AddString("eventKey", "changeEquipment");
		}
		
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogChallengeEventResponse (response);
		}
		
		/// <summary>
		/// The challenge ID instance to target
		/// </summary>
		public LogChallengeEventRequest_changeEquipment SetChallengeInstanceId( String challengeInstanceId )
		{
			request.AddString("challengeInstanceId", challengeInstanceId);
			return this;
		}
		public LogChallengeEventRequest_changeEquipment Set_item( string value )
		{
			request.AddString("item", value);
			return this;
		}
		public LogChallengeEventRequest_changeEquipment Set_type( string value )
		{
			request.AddString("type", value);
			return this;
		}
		public LogChallengeEventRequest_changeEquipment Set_unit( string value )
		{
			request.AddString("unit", value);
			return this;
		}
	}
	
	public class LogEventRequest_matchDisconnect : GSTypedRequest<LogEventRequest_matchDisconnect, LogEventResponse>
	{
	
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogEventResponse (response);
		}
		
		public LogEventRequest_matchDisconnect() : base("LogEventRequest"){
			request.AddString("eventKey", "matchDisconnect");
		}
		
		public LogEventRequest_matchDisconnect Set_matchID( string value )
		{
			request.AddString("matchID", value);
			return this;
		}
	}
	
	public class LogChallengeEventRequest_matchDisconnect : GSTypedRequest<LogChallengeEventRequest_matchDisconnect, LogChallengeEventResponse>
	{
		public LogChallengeEventRequest_matchDisconnect() : base("LogChallengeEventRequest"){
			request.AddString("eventKey", "matchDisconnect");
		}
		
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogChallengeEventResponse (response);
		}
		
		/// <summary>
		/// The challenge ID instance to target
		/// </summary>
		public LogChallengeEventRequest_matchDisconnect SetChallengeInstanceId( String challengeInstanceId )
		{
			request.AddString("challengeInstanceId", challengeInstanceId);
			return this;
		}
		public LogChallengeEventRequest_matchDisconnect Set_matchID( string value )
		{
			request.AddString("matchID", value);
			return this;
		}
	}
	
	public class LogEventRequest_getMatch : GSTypedRequest<LogEventRequest_getMatch, LogEventResponse>
	{
	
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogEventResponse (response);
		}
		
		public LogEventRequest_getMatch() : base("LogEventRequest"){
			request.AddString("eventKey", "getMatch");
		}
	}
	
	public class LogChallengeEventRequest_getMatch : GSTypedRequest<LogChallengeEventRequest_getMatch, LogChallengeEventResponse>
	{
		public LogChallengeEventRequest_getMatch() : base("LogChallengeEventRequest"){
			request.AddString("eventKey", "getMatch");
		}
		
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogChallengeEventResponse (response);
		}
		
		/// <summary>
		/// The challenge ID instance to target
		/// </summary>
		public LogChallengeEventRequest_getMatch SetChallengeInstanceId( String challengeInstanceId )
		{
			request.AddString("challengeInstanceId", challengeInstanceId);
			return this;
		}
	}
	
	public class LogEventRequest_getInventory : GSTypedRequest<LogEventRequest_getInventory, LogEventResponse>
	{
	
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogEventResponse (response);
		}
		
		public LogEventRequest_getInventory() : base("LogEventRequest"){
			request.AddString("eventKey", "getInventory");
		}
		
		public LogEventRequest_getInventory Set_playerID( string value )
		{
			request.AddString("playerID", value);
			return this;
		}
	}
	
	public class LogChallengeEventRequest_getInventory : GSTypedRequest<LogChallengeEventRequest_getInventory, LogChallengeEventResponse>
	{
		public LogChallengeEventRequest_getInventory() : base("LogChallengeEventRequest"){
			request.AddString("eventKey", "getInventory");
		}
		
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogChallengeEventResponse (response);
		}
		
		/// <summary>
		/// The challenge ID instance to target
		/// </summary>
		public LogChallengeEventRequest_getInventory SetChallengeInstanceId( String challengeInstanceId )
		{
			request.AddString("challengeInstanceId", challengeInstanceId);
			return this;
		}
		public LogChallengeEventRequest_getInventory Set_playerID( string value )
		{
			request.AddString("playerID", value);
			return this;
		}
	}
	
	public class LogEventRequest_getProfile : GSTypedRequest<LogEventRequest_getProfile, LogEventResponse>
	{
	
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogEventResponse (response);
		}
		
		public LogEventRequest_getProfile() : base("LogEventRequest"){
			request.AddString("eventKey", "getProfile");
		}
	}
	
	public class LogChallengeEventRequest_getProfile : GSTypedRequest<LogChallengeEventRequest_getProfile, LogChallengeEventResponse>
	{
		public LogChallengeEventRequest_getProfile() : base("LogChallengeEventRequest"){
			request.AddString("eventKey", "getProfile");
		}
		
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogChallengeEventResponse (response);
		}
		
		/// <summary>
		/// The challenge ID instance to target
		/// </summary>
		public LogChallengeEventRequest_getProfile SetChallengeInstanceId( String challengeInstanceId )
		{
			request.AddString("challengeInstanceId", challengeInstanceId);
			return this;
		}
	}
	
	public class LogEventRequest_resetPassword : GSTypedRequest<LogEventRequest_resetPassword, LogEventResponse>
	{
	
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogEventResponse (response);
		}
		
		public LogEventRequest_resetPassword() : base("LogEventRequest"){
			request.AddString("eventKey", "resetPassword");
		}
	}
	
	public class LogChallengeEventRequest_resetPassword : GSTypedRequest<LogChallengeEventRequest_resetPassword, LogChallengeEventResponse>
	{
		public LogChallengeEventRequest_resetPassword() : base("LogChallengeEventRequest"){
			request.AddString("eventKey", "resetPassword");
		}
		
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogChallengeEventResponse (response);
		}
		
		/// <summary>
		/// The challenge ID instance to target
		/// </summary>
		public LogChallengeEventRequest_resetPassword SetChallengeInstanceId( String challengeInstanceId )
		{
			request.AddString("challengeInstanceId", challengeInstanceId);
			return this;
		}
	}
	
	public class LogEventRequest_upgrade : GSTypedRequest<LogEventRequest_upgrade, LogEventResponse>
	{
	
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogEventResponse (response);
		}
		
		public LogEventRequest_upgrade() : base("LogEventRequest"){
			request.AddString("eventKey", "upgrade");
		}
		
		public LogEventRequest_upgrade Set_type( string value )
		{
			request.AddString("type", value);
			return this;
		}
		
		public LogEventRequest_upgrade Set_item( string value )
		{
			request.AddString("item", value);
			return this;
		}
	}
	
	public class LogChallengeEventRequest_upgrade : GSTypedRequest<LogChallengeEventRequest_upgrade, LogChallengeEventResponse>
	{
		public LogChallengeEventRequest_upgrade() : base("LogChallengeEventRequest"){
			request.AddString("eventKey", "upgrade");
		}
		
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogChallengeEventResponse (response);
		}
		
		/// <summary>
		/// The challenge ID instance to target
		/// </summary>
		public LogChallengeEventRequest_upgrade SetChallengeInstanceId( String challengeInstanceId )
		{
			request.AddString("challengeInstanceId", challengeInstanceId);
			return this;
		}
		public LogChallengeEventRequest_upgrade Set_type( string value )
		{
			request.AddString("type", value);
			return this;
		}
		public LogChallengeEventRequest_upgrade Set_item( string value )
		{
			request.AddString("item", value);
			return this;
		}
	}
	
}
	

namespace GameSparks.Api.Messages {


}
